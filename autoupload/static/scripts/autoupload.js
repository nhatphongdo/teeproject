(function ($) {
    "use strict";

    $('[sidebar_toggle]').on('click', function () {
        if ($('.right-sidebar').hasClass('slideInRight')) {
            $('.right-sidebar').addClass('slideInLeft');
            $('.right-sidebar').removeClass('slideInRight');
            $('.main-header.sidebar-header').addClass('slideOutRightSlideBar');
            $('.main-header.sidebar-header').removeClass('slideInRightSlideBar');
            $('.login-box').addClass('slideOutRightLoginBox');
            $('.login-box').removeClass('slideInRightLoginBox');
            $('#main-header').animate({'padding-right': 120}, 700, "linear");
        } else {
            $('.right-sidebar').addClass('slideInRight');
            $('.right-sidebar').removeClass('slideInLeft');
            $('.main-header.sidebar-header').addClass('slideInRightSlideBar');
            $('.main-header.sidebar-header').removeClass('slideOutRightSlideBar');
            $('.login-box').addClass('slideInRightLoginBox');
            $('.login-box').removeClass('slideOutRightLoginBox');
            $('#main-header').animate({'padding-right': '16.666%'}, 700, "linear");
        }
    });

    $(document).ready(function () {
        fix_css();
        mark_active($(window).scrollTop());
        $(window).resize(function () {
            fix_css();
            applySKScroll();
        });

        $('#right-bar a[href*="#"]:not([href="#"]), a[role="try-it"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                var step = 100;
                if (target.attr('id') == "learn") {
                    step = 25;
                }
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 100
                    }, 1000);
                    $('#right-bar li').removeClass('active');
                    $(this).parent('li').addClass('active');
                    return false;
                }
            }
        });

        var lastScrollTop = 0;
        $(window).scroll(function (event) {
            var st = $(this).scrollTop();
            //console.log('st = ' + st);

            mark_active(st);

            if (st == 0) {
                $('.logo-tee-insight img').addClass('zoomOutLogo');
                $('.logo-tee-insight img').removeClass('zoomInLogo');
                $('.main-header').removeClass('scroll');
            }

            if (st > lastScrollTop) {
                $('.logo-tee-insight img').addClass('zoomInLogo');
                $('.logo-tee-insight img').removeClass('zoomOutLogo');
                $('.main-header').addClass('scroll');
            }

            lastScrollTop = st;
        });
    });

    function mark_active(st) {
        $('#right-bar a').each(function () {
            var hash = $(this).attr('href');
            if (hash.length > 0 && $(hash).offset()) {
                //console.log(hash, $(hash).offset().top);
                if (st >= $(hash).offset().top - 0.75 * $(window).height()) {
                    $(hash + ' .delay_ani').addClass('animated').removeClass('delay_ani').addClass('ani');
                    $(hash + ' .delay_load').addClass('animated fadeIn').removeClass('delay_load').addClass('loaded');
                    $(hash + ' .delay_rotate').addClass('animated rotateIn').removeClass('delay_rotate').addClass('rotated');
                    $(hash + ' .delay_zoom').addClass('animated zoomIn').removeClass('delay_zoom').addClass('zoomed');
                }
                if (st > $(hash).offset().top - 0.75 * $(window).height()) {
                    $('#right-bar li').removeClass('active');
                    $(this).parent().addClass('active');
                }
            }
        });
    }

    function fix_css() {
        $('.main-header').css('opacity', 1);
        $('.winheight').height($(window).height());
    }

    function applySKScroll() {
        if ($(window).width() > 992) {
            var s = skrollr.init();
        }
    }

    applySKScroll();
})(jQuery);

function subscribe_type_local(type, event) {
    localStorage.setItem('_subscribe_type', type);
    ga('send', 'event', {
        eventCategory: 'Subscribe Button',
        eventAction: 'click',
        eventLabel: type,
        transport: 'beacon'
    });
}