(function ($) {
    "use strict";
    $('[data-toggle="tooltip"]').tooltip({html: true});

    var currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'CRC': '₡', // Costa Rican Colón
        'GBP': '£', // British Pound Sterling
        'ILS': '₪', // Israeli New Sheqel
        'INR': '₹', // Indian Rupee
        'JPY': '¥', // Japanese Yen
        'KRW': '₩', // South Korean Won
        'NGN': '₦', // Nigerian Naira
        'PHP': '₱', // Philippine Peso
        'PLN': 'zł', // Polish Zloty
        'PYG': '₲', // Paraguayan Guarani
        'THB': '฿', // Thai Baht
        'UAH': '₴', // Ukrainian Hryvnia
        'VND': '₫' // Vietnamese Dong
    };

    var currency_name = 'USD';
    var USD_VND_currency_rate = 22280.28; //Last updated on September 11, 2016 17:00:03 UTC
    USD_VND_currency_rate = 22200.00; //default 2016

    //if (currency_symbols[currency_name] !== undefined) {
    //    console.log(currency_symbols[currency_name]);
    //}

    var theLanguage = $('html').attr('lang');

    var packages = {
        trial: {
            id: 'trial',
            name: 'Trial',
            price: 0,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        },
        lite: {
            id: 'lite',
            name: 'Lite',
            price: 19,
            platforms: ['sunfrog', 'teespring']
        },
        bundle: {
            id: 'bundle',
            name: 'Bundle',
            price3: 49,
            price4: 59,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        }
    };
    var chosen = {
        name: '',
        pack: packages.trial,
        total: 0
    };

    $('#payment_modal').on({
        'show.bs.modal': function (event) {
            var button = $(event.relatedTarget);
            var front_price_display = chosen.total;
            if (chosen.pack.id != packages.trial.id) {
                $('.not-free').show();
                if (typeof chosen.total == 'number' && chosen.total >= 0) {
                    front_price_display = intcomma(chosen.total.toFixed(2));
                }
                $('#usd_sum').text(front_price_display);
                if (typeof chosen.total == 'number' && chosen.total >= 0) {
                    var sum = USD_VND_currency_rate * chosen.total;
                    front_price_display = intcomma(sum.toFixed(0));
                }
                $('#vnd_sum').text(front_price_display);
            }
        },
        'shown.bs.modal': function () {
            $('.modal-backdrop.fade.in').css('opacity', 0.8);
            var margin_top = ($(window).height() - $('#payment_modal .modal-content').height()) / 2;
            if ((margin_top - 20) <= 0){
                margin_top = 20;
            }
            $('#payment_modal .modal-dialog').animate({'margin-top': margin_top}, 200, "linear");

            localStorage.removeItem('chosen_name');
            localStorage.removeItem('chosen_pack');
            localStorage.removeItem('chosen_total');
        },
        'hide.bs.modal': function (event) {
            window.location.href = $('.logo').attr('href');
        }
    });


    $(document).ready(function () {
        theLanguage = $('html').attr('lang');

        fix_css();
        $(window).resize(function () {
            fix_css();
        });

        if (localStorage.getItem('chosen_name')
            && localStorage.getItem('chosen_pack')
            && localStorage.getItem('chosen_total')) {
            chosen.name = localStorage.getItem('chosen_name');
            chosen.pack = packages[localStorage.getItem('chosen_pack')];
            chosen.total = parseInt(localStorage.getItem('chosen_total'));

            $('#payment_modal').modal('show');
        }
        else{
            window.location.href = $('.logo').attr('href');
        }
    });

    function fix_css() {
        $('.winheight').height($(window).height());
    }

    function intcomma(str) {
        if (typeof str == 'string' || typeof str == 'number') {
            return String(str).replace(/(\d)(?=(\d\d\d)+(?!\d)\.?)/g, "$1,");
        }
        return str;
    }

})(jQuery);