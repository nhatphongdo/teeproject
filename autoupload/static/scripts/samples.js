(function ($) {
    "use strict";
    $('[data-toggle="tooltip"]').tooltip({html: true});

    var currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'CRC': '₡', // Costa Rican Colón
        'GBP': '£', // British Pound Sterling
        'ILS': '₪', // Israeli New Sheqel
        'INR': '₹', // Indian Rupee
        'JPY': '¥', // Japanese Yen
        'KRW': '₩', // South Korean Won
        'NGN': '₦', // Nigerian Naira
        'PHP': '₱', // Philippine Peso
        'PLN': 'zł', // Polish Zloty
        'PYG': '₲', // Paraguayan Guarani
        'THB': '฿', // Thai Baht
        'UAH': '₴', // Ukrainian Hryvnia
        'VND': '₫' // Vietnamese Dong
    };

    var currency_name = 'USD';
    var USD_VND_currency_rate = 22280.28; //Last updated on September 11, 2016 17:00:03 UTC
    USD_VND_currency_rate = 22200.00; //default 2016

    //if (currency_symbols[currency_name] !== undefined) {
    //    console.log(currency_symbols[currency_name]);
    //}

    var theLanguage = $('html').attr('lang');

    var packages = {
        trial: {
            id: 'trial',
            name: 'Trial',
            price: 0,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        },
        lite: {
            id: 'lite',
            name: 'Lite',
            price: 19,
            platforms: ['sunfrog', 'teespring']
        },
        bundle: {
            id: 'bundle',
            name: 'Bundle',
            price3: 49,
            price4: 59,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        }
    };
    var chosen = {
        name: '',
        email: '',
        emails_file: '',
        tel: '',
        pack: packages.trial,
        platforms: packages.trial.platforms,
        licenses: 1,
        discount: false,
        total: 0
    };

    var currency = currency_name;

    $(document).ready(function () {
        theLanguage = $('html').attr('lang');
        chosen.pack = packages[$('input[name="pack"]:checked').val().toLowerCase()];
        var platforms = [];
        $('input:checkbox[name="platform"]:checked').each(function () {
            platforms.push($(this).val());
        });
        chosen.platforms = platforms;
        chosen.total = total_price();
        if (chosen.pack.name == 'Trial') {
            $('#subscribe').addClass('disabled');
        }

        fix_css();
        $(window).resize(function () {
            fix_css();
        });

        // test
        //console.log(chosen.pack, chosen.platforms, chosen.total);
    });

    $('input[name="pack"]').on('change', function () {
        chosen.pack = packages[this.value.toLowerCase()];
        chosen.platforms = packages[this.value.toLowerCase()].platforms;
        chosen.total = total_price();

        $('input[name="platform"]:checked').parent().removeClass('active');
        $('input[name="platform"]:checked').prop('checked', false);
        $.each(packages[this.value.toLowerCase()].platforms, function (key, value) {
            $('#platform_' + value).parent().addClass('active');
            $('#platform_' + value).prop('checked', true);
        });
        if (this.value == 'Trial') {
            $('#subscribe').addClass('disabled');
            $('.plan-btn-radio.disabled').removeClass('disabled');
        }
        else {
            $('#subscribe').removeClass('disabled');
            $('.plan-btn-radio').addClass('disabled').removeClass('active');
            $('input[name="plan"][value="Monthly"]').prop('checked', true);
            $('input[name="plan"]:checked').parent().addClass('active');
            var plan = $('input[name="plan"]:checked').val();
            var cycle_number = $('input[name="cycle_number"]:checked').val();
            chosen.total = total_price();
            change_plan(plan, cycle_number)
        }
        // test
        console.log(chosen.pack, chosen.platforms, chosen.total);
    });

    $('.platform-btn-check').click(function (event) {
        var this_child = $(this).children('input[type="checkbox"]');
        var name = this_child.attr('name'); //platform

        if ($('input[name=' + name + ']:checked').length <= 1) {
            if (this_child.is(":checked")) {
                return false;
            }
        }
        switch (chosen.pack.id) {
            case packages.lite.id:
                if ($('input[name=' + name + ']:checked').length >= 2) {
                    if (!this_child.is(":checked")) {
                        return false;
                    }
                }
                break;
            case packages.bundle.id:
                if ($('input[name=' + name + ']:checked').length <= 3) {
                    if (this_child.is(":checked")) {
                        return false;
                    }
                }
                break;
            default :
                break
        }
    });

    $('input[name="platform"]').on('change', function (event) {
        var platforms = [];
        $('input:checkbox[name="platform"]:checked').each(function () {
            platforms.push($(this).val());
        });
        chosen.platforms = platforms;
        chosen.total = total_price();
        // test
        console.log(chosen.pack, chosen.platforms, chosen.total);
    });

    $('input[name="plan"]').on('change', function () {
        var cycle_number = $('input[name="cycle_number"]:checked').val();
        change_plan(this.value, cycle_number)
    });

    $('input[name="cycle_number"]').on('change', function () {
        var plan = $('input[name="plan"]:checked').val();
        change_plan(plan, this.value)
    });

    $('.btn').on('click', function (event) {
        if ($(this).hasClass('disabled')) {
            return false;
        }
    });

    function change_plan(plan, cycle_number) {
        var cycle_period_display;
        if (plan === 'Daily') {
            cycle_period_display = 'Day';
        } else if (plan === 'Weekly') {
            cycle_period_display = 'Week';
        } else if (plan === 'Monthly') {
            cycle_period_display = 'Month';
        } else if (plan === 'Yearly') {
            cycle_period_display = 'Year';
        }
        var suf_currency = currency + '/' + cycle_number + ' ' + cycle_period_display;
        $('.suf_currency').text(suf_currency);
        total_price();
    }

    function total_price() {
        var price = chosen.pack.price * chosen.platforms.length;
        var total = price;
        if (chosen.pack.id == packages.trial.id) {
            var plan = $('input[name="plan"]:checked').val();
            switch (plan) {
                case 'Daily':
                    price = 5;
                    break;
                case 'Weekly':
                    price = 30;
                    break;
                case 'Yearly':
                    price = 1400;
                    break;
                default :
                    break;
            }
            //return 'Free';
        }
        else if (chosen.pack.id == packages.bundle.id) {
            if (chosen.platforms.length == 3) {
                price = packages.bundle.price3;
            }
            else if (chosen.platforms.length == 4) {
                price = packages.bundle.price4;
            }
            else if (chosen.platforms.length > 0) {
                //return '(Lite)';
            }
            else {
                //return '(Trial)';
            }
        }
        else if (chosen.pack.id == packages.lite.id) {
            if (chosen.platforms.length > 2) {
                //return '(Bundle)';
            }
            else if (chosen.platforms.length > 0) {
                price = chosen.pack.price * chosen.platforms.length;
            }
            else {
                //return '(Trial)';
            }
        }
        total = price;

        total = parseFloat(Math.round(total * 100) / 100).toFixed(2);
        chosen.total = total;
        $('#paper_cost').text(chosen.total);
        $('input[name="total"]').val(chosen.total);
        return chosen.total;
    }

    function fix_css() {
        $('.winheight').height($(window).height());
    }

    function intcomma(str) {
        if (typeof str == 'string' || typeof str == 'number') {
            return String(str).replace(/(\d)(?=(\d\d\d)+(?!\d)\.?)/g, "$1,");
        }
        return str;
    }

    // using jQuery
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            //if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            //    xhr.setRequestHeader("X-CSRFToken", csrftoken);
            //}
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

})(jQuery);