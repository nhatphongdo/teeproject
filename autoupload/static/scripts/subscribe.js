(function ($) {
    "use strict";
    $('[data-toggle="tooltip"]').tooltip({html: true});

    var currency_symbols = {
        'USD': '$', // US Dollar
        'EUR': '€', // Euro
        'CRC': '₡', // Costa Rican Colón
        'GBP': '£', // British Pound Sterling
        'ILS': '₪', // Israeli New Sheqel
        'INR': '₹', // Indian Rupee
        'JPY': '¥', // Japanese Yen
        'KRW': '₩', // South Korean Won
        'NGN': '₦', // Nigerian Naira
        'PHP': '₱', // Philippine Peso
        'PLN': 'zł', // Polish Zloty
        'PYG': '₲', // Paraguayan Guarani
        'THB': '฿', // Thai Baht
        'UAH': '₴', // Ukrainian Hryvnia
        'VND': '₫' // Vietnamese Dong
    };

    var currency_name = 'USD';
    var USD_VND_currency_rate = 22280.28; //Last updated on September 11, 2016 17:00:03 UTC
    USD_VND_currency_rate = 22200.00; //default 2016

    //if (currency_symbols[currency_name] !== undefined) {
    //    console.log(currency_symbols[currency_name]);
    //}

    var theLanguage = $('html').attr('lang');

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var packages = {
        trial: {
            id: 'trial',
            name: 'Trial',
            price: 0,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        },
        lite: {
            id: 'lite',
            name: 'Lite',
            price: 19,
            platforms: ['sunfrog', 'teespring']
        },
        bundle: {
            id: 'bundle',
            name: 'Bundle',
            price3: 49,
            price4: 59,
            platforms: ['sunfrog', 'teespring', 'teechip', 'viralstyle']
        }
    };
    var chosen = {
        name: '',
        email: '',
        emails_file: '',
        tel: '',
        pack: packages.trial,
        platforms: packages.trial.platforms,
        licenses: 1,
        discount: false,
        total: 0
    };

    // iCheck
    if (typeof $.fn.iCheck != 'undefined') {
        $('input[type="checkbox"], input[type="radio"]').iCheck('destroy').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%'
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifChecked', function () {
            $(this).prop('checked', true);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifUnchecked', function () {
            $(this).prop('checked', false);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifDisabled', function () {
            $(this).prop('disabled', true);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifEnabled', function () {
            $(this).prop('disabled', false);
        });
    }
    else {
        console.log('iCheck is not found!');
    }

    $('#payment_modal').on({
        'show.bs.modal': function (event) {
            var button = $(event.relatedTarget);
            var front_price_display = chosen.total;
            if (chosen.pack.id != packages.trial.id) {
                $('.not-free').show();
                if (typeof chosen.total == 'number' && chosen.total >= 0) {
                    front_price_display = intcomma(chosen.total.toFixed(2));
                }
                $('#usd_sum').text(front_price_display);
                if (typeof chosen.total == 'number' && chosen.total >= 0) {
                    var sum = USD_VND_currency_rate * chosen.total;
                    front_price_display = intcomma(sum.toFixed(0));
                }
                $('#vnd_sum').text(front_price_display);
            }
        },
        'shown.bs.modal': function () {
            $('.modal-backdrop.fade.in').css('opacity', 0.8);
            var margin_top = ($(window).height() - $('#payment_modal .modal-content').height()) / 2;
            if ((margin_top - 20) <= 0) {
                margin_top = 20;
            }
            $('#payment_modal .modal-dialog').animate({'margin-top': margin_top}, 200, "linear");
        },
        'hide.bs.modal': function (event) {
            clear_form();
        }
    });

    $('#change_pack').on({
        'click': function () {
            $('[name="type_opts"]').iCheck('enable');
            email_trial_off(chosen.email);
        },
        'mouseenter mouseleave': function () {
            //$(this).toggleClass('fa-spin');
        }
    });

    $('#subscribe_submit').on('click', function (event) {
        //ga('send', 'event', 'Submit Subscription', 'click', 'Subscribe', {transport: 'beacon'});
        $(this).prop('disabled', true);
        if (chosen.name == '') {
            $('#id_name').focus();
            if (theLanguage == 'vi') {
                toastr.warning('Chúng tôi cần biết tên bạn!', '(*)bắt buộc!');
            }
            else {
                toastr.warning('Your name are required!', '(*)required!');
            }
        }
        else if (chosen.email == '' && chosen.licenses == 1) {
            $('#id_email').focus();
            if (theLanguage == 'vi') {
                toastr.warning('Chúng tôi cần email của bạn!', '(*)bắt buộc!');
            }
            else {
                toastr.warning('Your email are required!', '(*)required!');
            }
        }
        //else if (chosen.tel == '') {
        //    $('#id_phone').focus();
        //}
        else if (chosen.licenses > 1 && $('#id_emails_file').val() == '') {
            $('#id_emails_file').click();
            if (theLanguage == 'vi') {
                toastr.warning('Chúng tôi cần danh sách các email của bạn!', '(*)bắt buộc!');
            }
            else {
                toastr.warning('Your emails are required!', '(*)required!');
            }
        }
        else if (chosen.platforms.length) {
            var cond_trial = chosen.pack.id == packages.trial.id
                && chosen.platforms.length >= 1 && chosen.platforms.length <= 4;
            var cond_lite = chosen.pack.id == packages.lite.id
                && chosen.platforms.length >= 1 && chosen.platforms.length <= 2;
            var cond_bundle = chosen.pack.id == packages.bundle.id
                && chosen.platforms.length >= 3 && chosen.platforms.length <= 4;
            if (cond_trial || cond_lite || cond_bundle) {
                chosen.emails_file = $('#id_emails_file')[0].files[0];
                var form_data = new FormData($('#subscribe-form')[0]);
                form_data.append('_name', chosen.name);
                form_data.append('_email', chosen.email);
                form_data.append('_emails_file', chosen.emails_file);
                form_data.append('_tel', chosen.tel);
                form_data.append('_pack_id', chosen.pack.id);
                form_data.append('_pack_name', chosen.pack.name);
                form_data.append('_platforms', chosen.platforms);
                form_data.append('_licenses', chosen.licenses);
                form_data.append('_discount', chosen.discount);
                form_data.append('_total', chosen.total);
                form_data.append('_fbid', $('#id_fbid').val());
                $.ajax({
                    url: '/api/subscribe-autoupload/',
                    data: form_data,
                    method: 'POST',
                    crossDomain: false,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if (result['error'] == 0) {
                            localStorage.setItem('chosen_name', chosen.name);
                            localStorage.setItem('chosen_pack', chosen.pack.id);
                            localStorage.setItem('chosen_total', chosen.total);
                            window.location.href = window.location.href + 'thanks/';
                            //$('#payment_modal').modal('show');
                        }
                        else {
                            var message = result.message[result.message.length - 1];
                            if (theLanguage == 'vi') {
                                switch (message) {
                                    case 'Sorry, your email address has been denied!':
                                        message = 'Xin lỗi! Email của bạn có vấn đề. Hãy nhập email cá nhân của bạn!';
                                        break;
                                    case 'Email is already used.':
                                        message = 'Email này đã được sử dụng!';
                                        break;
                                    case 'Can not save file!':
                                        message = 'Không lưu được file! Hãy thử lại! Cám ơn!';
                                        break;
                                    case 'File is not valid!':
                                        message = 'File quá nặng!';
                                        break;
                                    default :
                                        break;
                                }
                                toastr.error(message, 'Hãy thử lại!');
                            }
                            else {
                                toastr.error(message, 'Please try again!');
                            }
                            clear_form();
                            $('#id_name').focus();
                        }
                    },
                    error: function () {
                        $('#id_name').focus();
                        if (theLanguage == 'vi') {
                            toastr.error('Vui lòng kiểm tra lại các thông tin!');
                        }
                        else {
                            toastr.error('Please check your fields!');
                        }
                    }
                });
            }
        }
    });

    $('#trial').on({
        'ifChecked': function () {
            chosen.pack = packages.trial;
            chosen.platforms = packages.trial.platforms;

            $('.platforms-show').removeClass('platforms-show').addClass('platforms-hide');
            $('#trial_platforms').removeClass('platforms-hide').addClass('platforms-show');

            $('.type-opt').removeClass('active');
            $(this).parents('.type-opt').addClass('active');

            $('[name="platform_opts"]').iCheck('uncheck');
            $.each(packages.trial.platforms, function (i, val) {
                $('.platforms-show #' + val).iCheck('check');
            });

            $('[name="type_opts"]').iCheck('disable');
            $('#trial[name="type_opts"]').iCheck('enable');

            $('.info-input').prop('disabled', true);
            $('#subscribe_submit').prop('disabled', true);

            $('#sum_price').text(total_price());

            // Catalyst Program Enrollment
            catalystProgramRowShow();
        },
        'ifUnchecked': function () {
            // Catalyst Program Enrollment
            catalystProgramRowHide();
        }
    });

    $('#lite').on({
        'ifChecked': function () {
            chosen.pack = packages.lite;
            chosen.platforms = packages.lite.platforms;

            $('.platforms-show').removeClass('platforms-show').addClass('platforms-hide');
            $('#lite_platforms').removeClass('platforms-hide').addClass('platforms-show');

            $('.type-opt').removeClass('active');
            $(this).parents('.type-opt').addClass('active');

            $('[name="platform_opts"]').iCheck('uncheck');
            $.each(packages.lite.platforms, function (i, val) {
                $('.platforms-show #' + val).iCheck('check');
            });

            $('[name="type_opts"]').iCheck('disable');
            $('#lite[name="type_opts"]').iCheck('enable');

            $('.info-input').prop('disabled', true);
            $('#subscribe_submit').prop('disabled', true);

            $('#sum_price').text(total_price());
        },
        'ifUnchecked': function () {
        }
    });

    $('#bundle').on({
        'ifChecked': function () {
            chosen.pack = packages.bundle;
            chosen.platforms = packages.bundle.platforms;

            $('.platforms-show').removeClass('platforms-show').addClass('platforms-hide');
            $('#bundle_platforms').removeClass('platforms-hide').addClass('platforms-show');

            $('.type-opt').removeClass('active');
            $(this).parents('.type-opt').addClass('active');

            $('[name="platform_opts"]').iCheck('uncheck');
            $.each(packages.bundle.platforms, function (i, val) {
                $('.platforms-show #' + val).iCheck('check');
            });

            $('[name="type_opts"]').iCheck('disable');
            $('#bundle[name="type_opts"]').iCheck('enable');

            $('.info-input').prop('disabled', true);
            $('#subscribe_submit').prop('disabled', true);

            $('#sum_price').text(total_price());
        },
        'ifUnchecked': function () {
        }
    });

    $('#trial_platforms input:checkbox[name="platform_opts"]').on({
        'ifClicked': function () {
        },
        'ifChecked': function () {
            var platforms = [];
            $('#trial_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            chosen.platforms = platforms;
            $('#sum_price').text(total_price());

            if (chosen.platforms.length > 1) {
                $('#trial_platforms input:checkbox[name="platform_opts"]').iCheck('enable');
            }
        },
        'ifUnchecked': function () {
            var platforms = [];
            $('#trial_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            chosen.platforms = platforms;
            $('#sum_price').text(total_price());

            if (chosen.platforms.length == 1) {
                $('#trial_platforms input:checkbox[name="platform_opts"]:checked').iCheck('disable');
            }
        }
    });

    $('#lite_platforms input:checkbox[name="platform_opts"]').on({
        'ifClicked': function () {
        },
        'ifChecked': function () {
            var platforms = [];
            $('#lite_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            chosen.platforms = platforms;
            $('#sum_price').text(total_price());

            if (chosen.platforms.length == 2) {
                $('#lite_platforms input:checkbox[name="platform_opts"]').iCheck('disable');
                $('#lite_platforms input:checkbox[name="platform_opts"]:checked').iCheck('enable');
            }
        },
        'ifUnchecked': function () {
            var platforms = [];
            $('#lite_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            if (chosen.pack.id != packages.trial.id) {
                chosen.platforms = platforms;
            }
            $('#sum_price').text(total_price());

            if (chosen.platforms.length == 1) {
                $('#lite_platforms input:checkbox[name="platform_opts"]').iCheck('enable');
                $('#lite_platforms input:checkbox[name="platform_opts"]:checked').iCheck('disable');
            }
        }
    });

    $('#bundle_platforms input:checkbox[name="platform_opts"]').on({
        'ifClicked': function () {
        },
        'ifChecked': function () {
            var platforms = [];
            $('#bundle_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            chosen.platforms = platforms;
            $('#sum_price').text(total_price());

            if (chosen.platforms.length >= 3) {
                $('#bundle_platforms input:checkbox[name="platform_opts"]').iCheck('enable');
            }
        },
        'ifUnchecked': function () {
            var platforms = [];
            $('#bundle_platforms input:checkbox[name="platform_opts"]:checked').each(function () {
                platforms.push($(this).val());
            });
            if (chosen.pack.id != packages.trial.id) {
                chosen.platforms = platforms;
            }
            $('#sum_price').text(total_price());

            if (chosen.platforms.length <= 3) {
                $('#bundle_platforms input:checkbox[name="platform_opts"]:checked').iCheck('disable');
            }
        }
    });

    $(window).click(function (e) {
        var subject = $(".platforms-show");
        var cond_trial = chosen.pack.id == packages.trial.id
            && chosen.platforms.length >= 1 && chosen.platforms.length <= 4;
        var cond_lite = chosen.pack.id == packages.lite.id
            && chosen.platforms.length >= 1 && chosen.platforms.length <= 2;
        var cond_bundle = chosen.pack.id == packages.bundle.id
            && chosen.platforms.length >= 3 && chosen.platforms.length <= 4;
        if (e.target.id != subject.attr('id')
            && !subject.has(e.target).length
            && e.target.className != 'n-platforms'
            && e.target.getAttribute('data-type') != 'none-click'
            && !($(e.target).parents('[data-type="none-click"]').length)
            || e.target.nodeName == 'INPUT') {
            if (cond_trial || cond_lite || cond_bundle) {
                var check = $('.platforms-show').hasClass('platforms-show');
                $('.platforms-show').removeClass('platforms-show').addClass('platforms-hide');

                $('.n-platforms').text('');
                $('.type-opt.active > .n-platforms').text('[ ' + chosen.platforms.length + ' platform(s) ]');

                $('.info-input').prop('disabled', false);
                if (packages.trial.id == chosen.pack.id) {
                    $('#discount').iCheck('uncheck').prop('disabled', true);
                    $('#id_licenses').val(1).prop('disabled', true);
                    $('#email_single').show();
                    $('#email_multi').hide();
                    $('#id_emails_file').val('');
                    chosen.licenses = parseInt($('#id_licenses').val());
                    $('#licenses_numb').text(chosen.licenses);
                    $('#sum_price').text(total_price());
                }

                $('.type-opt.active > .n-platforms').unbind('click').on('click', function (event) {
                    $(this).text('');
                    $('.type-opt.active > .platform-opts').removeClass('platforms-hide').addClass('platforms-show');

                    $('.info-input').prop('disabled', true);
                    $('#subscribe_submit').prop('disabled', true);
                });

                if (e.target.nodeName == 'INPUT') {
                    $(e.target).focus();
                    return false;
                }

                if (chosen.name != ''
                    && (
                        (chosen.email != '' && chosen.licenses == 1)
                        || (chosen.licenses > 1 && $('#id_emails_file').val() != '')
                        )) {
                    if (e.target.id != 'subscribe_submit') {
                        $('#subscribe_submit').prop('disabled', false);
                    }
                    if (chosen.tel == '') {
                        $('#id_phone').focus();
                    }
                }
                else {
                    $('#subscribe_submit').prop('disabled', true);
                    if (chosen.name == '') {
                        $('#id_name').focus();
                    }
                    else if (chosen.email == '' && chosen.licenses == 1) {
                        $('#id_email').focus();
                    }
                    else if (chosen.licenses > 1 && $('#id_emails_file').val() == '') {
                        $('#id_emails_file').click();
                        if (theLanguage == 'vi') {
                            toastr.warning('Chúng tôi cần danh sách các email của bạn!', '(*)bắt buộc!');
                        }
                        else {
                            toastr.warning('Your emails are required!', '(*)required!');
                        }
                    }
                    else if (chosen.tel == '') {
                        $('#id_phone').focus();
                    }
                }
            }
            else {
                if (chosen.pack.id == packages.trial.id
                    && (chosen.platforms.length < 1 || chosen.platforms.length > 4)) {
                    if (theLanguage == 'vi') {
                        toastr.error('Vui lòng chọn ít nhất 1 platform cho gói Trial!');
                    }
                    else {
                        toastr.error('Please choose at least one platform for Trial package!');
                    }
                }
                else if (chosen.pack.id == packages.lite.id
                    && (chosen.platforms.length < 1 || chosen.platforms.length > 2)) {
                    if (theLanguage == 'vi') {
                        toastr.error('Vui lòng chọn 1 hoặc 2 platform cho gói Lite!');
                    }
                    else {
                        toastr.error('Please choose at least one or two platforms for Lite package!');
                    }
                }
                else if (chosen.pack.id == packages.bundle.id
                    && (chosen.platforms.length < 3 || chosen.platforms.length > 4)) {
                    if (theLanguage == 'vi') {
                        toastr.error('Vui lòng chọn ít nhất 3 platform cho gói Bundle!');
                    }
                    else {
                        toastr.error('Please choose at least three or four platforms for Bundle package!');
                    }
                }
            }
        }
    });

    var old_licenses = chosen.licenses;
    $('#id_licenses').change(function () {
        if ($('#id_licenses').val() > 0 && $('#id_licenses').val() !== '') {
            if ($('#id_licenses').val() == 1) {
                $('#email_single').show();
                $('#email_multi').hide();
                $('#id_emails_file').val('');
                chosen.emails_file = '';
            }
            else {
                $('#email_single').hide();
                $('#email_multi').show();
                $('#id_email').val('');
                chosen.email = '';
            }
            chosen.licenses = parseInt($(this).val());
            $('#licenses_numb').text(chosen.licenses);
            $('#sum_price').text(total_price());
            if (chosen.licenses != old_licenses) {
                toastr.clear();
                if (chosen.licenses > 10) {
                    if (theLanguage == 'vi') {
                        toastr.success('Bạn sẽ được giảm giá 15%!');
                    }
                    else {
                        toastr.success('You will receive 15% discount!');
                    }
                }
                else if (chosen.licenses > 5) {
                    if (theLanguage == 'vi') {
                        toastr.success('Bạn sẽ được giảm giá 12%!');
                    }
                    else {
                        toastr.success('You will receive 12% discount!');
                    }
                }
                else if (chosen.licenses >= 3) {
                    if (theLanguage == 'vi') {
                        toastr.success('Bạn sẽ được giảm giá 10%!');
                    }
                    else {
                        toastr.success('You will receive 10% discount!');
                    }
                }
            }
        }
        else {
            $('#id_licenses').val(1);
            //error
        }
    });

    $('#discount').on({
        'ifChecked': function () {
            $(this).parents('div.checkbox').removeClass('text-opaque');
            chosen.discount = true;
            $('#sum_price').text(total_price());
        },
        'ifUnchecked': function () {
            $(this).parents('div.checkbox').addClass('text-opaque');
            chosen.discount = false;
            $('#sum_price').text(total_price());
        }
    });

    $('#id_name').change(function () {
        if ($(this).val() !== '') {
            chosen.name = $(this).val();
        }
        else {
            chosen.name = '';
            if (theLanguage == 'vi') {
                $(this).focus().prop('placeholder', "Hãy nhập họ tên của bạn!");
            }
            else {
                $(this).focus().prop('placeholder', "What's your name?");
            }
        }
    });

    $('#id_email').change(function () {
        if ($(this).val() !== '' && validateEmail($(this).val())) {
            chosen.email = $(this).val();
            email_trial_off(chosen.email);
        }
        else {
            chosen.email = '';
            if (theLanguage == 'vi') {
                $(this).focus().prop('placeholder', "Hãy nhập email của bạn!");
            }
            else {
                $(this).focus().prop('placeholder', "What's your email?");
            }
        }
    });

    $('#id_phone').change(function () {
        if ($(this).val() !== '') {
            chosen.tel = $(this).val();
        }
        else {
            chosen.tel = '';
            if (theLanguage == 'vi') {
                $(this).focus().prop('placeholder', "Hãy nhập số điện thoại của bạn!");
            }
            else {
                $(this).focus().prop('placeholder', "What's your phone number?");
            }
        }
    });

    $(document).ready(function () {
        theLanguage = $('html').attr('lang');

        fix_css();
        $(window).resize(function () {
            fix_css();
        });

        if (localStorage.getItem('_subscribe_type')) {
            chosen.pack = packages[localStorage.getItem('_subscribe_type')];
            localStorage.removeItem('_subscribe_type');
        }
        //$('#id_name').focus();
        $('#' + chosen.pack.id).iCheck('check').parents('.type-opt').addClass('active').iCheck('update');
        // Catalyst Program Enrollment
        if (chosen.pack.id == packages.trial.id) {
            catalystProgramRowShow();
        }
        else {
            catalystProgramRowHide();
        }
        $('#id_licenses').val(chosen.licenses);
        $('#sum_price').text(total_price());
    });

    function clear_form() {
        document.getElementById("subscribe-form").reset();
        $('#trial').iCheck('check');
    }

    function email_trial_off(email) {
        if ($('#id_email').val() !== '' && validateEmail($('#id_email').val())) {
            $.ajax({
                url: '/api/check-email/',
                data: {email: email, csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()},
                dataType: 'json',
                method: 'POST',
                crossDomain: false,
                success: function (result) {
                    if (result['error'] == 0) {
                        if (result['message'] != 'trial') {
                            $('#trial').iCheck('uncheck').iCheck('disable');
                            if (chosen.pack.id == packages.trial.id) {
                                $('#lite').iCheck('check');
                                if (theLanguage == 'vi') {
                                    toastr.warning('Cám ơn bạn đã dùng sản phẩm của chúng tôi! Vui lòng chọn gói trả phí [Lite] hoặc [Bundle] để tiếp tục ..');
                                }
                                else {
                                    toastr.warning('Thanks for your trial or use! Please choose our paid package [Lite] or [Bundle]..');
                                }
                            }
                        }
                        else {

                        }
                    }
                    else {
                        var message = result.message[result.message.length - 1];
                        if (theLanguage == 'vi') {
                            switch (message) {
                                case 'Sorry, your email address has been denied!':
                                    message = 'Xin lỗi! Email của bạn có vấn đề. Hãy nhập email cá nhân của bạn!';
                                    break;
                                case 'Need 1 email to check!':
                                    message = 'Hãy nhập email của bạn!';
                                    break;
                                default :
                                    break;
                            }
                            toastr.error(message, 'Hãy thử lại!');
                        }
                        else {
                            toastr.error(message, 'Please try again!');
                        }
                        //clear_form();
                        $('#id_email').focus();
                    }
                },
                error: function () {
                    $('#id_name').focus();
                }
            });
        }
    }

    function total_price() {
        var price = chosen.pack.price * chosen.platforms.length;
        var total = price;
        $('#currency_symb').removeClass('hidden');
        if (chosen.pack.id == packages.trial.id) {
            $('#currency_symb').addClass('hidden');
            if (theLanguage == 'vi') {
                return 'miễn phí';
            }
            return 'free';
        }
        else if (chosen.pack.id == packages.bundle.id) {
            if (chosen.platforms.length == 3) {
                price = packages.bundle.price3;
            }
            else if (chosen.platforms.length == 4) {
                price = packages.bundle.price4;
            }
            else if (chosen.platforms.length > 0) {
                //return '(Lite)';
            }
            else {
                //return '(Trial)';
            }
        }
        else if (chosen.pack.id == packages.lite.id) {
            if (chosen.platforms.length > 2) {
                //return '(Bundle)';
            }
            else if (chosen.platforms.length > 0) {
                price = chosen.pack.price * chosen.platforms.length;
            }
            else {
                //return '(Trial)';
            }
        }
        total = price;

        if (chosen.licenses) {
            total = price * chosen.licenses;
            if (chosen.licenses > 10) {
                total -= total * 0.15;
            }
            else if (chosen.licenses > 5) {
                total -= total * 0.12;
            }
            else if (chosen.licenses >= 3) {
                total -= total * 0.1;
            }
        }

        if (chosen.discount) {
            total = total * 3 * 0.85;
        }

        total = parseFloat(Math.round(total * 100) / 100).toFixed(2);
        chosen.total = total;
        return chosen.total;
    }

    function fix_css() {
        $('.winheight').height($(window).height());
    }

    function intcomma(str) {
        if (typeof str == 'string' || typeof str == 'number') {
            return String(str).replace(/(\d)(?=(\d\d\d)+(?!\d)\.?)/g, "$1,");
        }
        return str;
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
})(jQuery);

// Catalyst Program Enrollment

function catalystProgramRowHide() {
    $('#catalyst_program_btn').removeClass('visible');
}

function catalystProgramRowShow() {
    $('#catalyst_program_btn').addClass('visible');
}

function catalystProgramShow() {
    ga('send', 'event', 'Join Catalyst Program', 'click', 'Catalyst Program', {transport: 'beacon'});
    $('.cp-block').animate({
        'opacity': 1,
        'min-height': '200px',
        'height': 'auto',
        'padding': '10px 15px 15px 20px'
    }, 1000, 'linear').addClass('visible').css('height', 'auto');

    $('#id_fbid').unbind('change').change(function () {
//        if ($(this).val() !== '') {
//            document.getElementById('cp_share_btn').innerHTML = '<a href="javascript:void(0)" role="button" onclick="shareAutoupload();" title="Thanks for join Catalyst Program Enrollment, ' + $(this).val() + '!" class="btn cp-share-btn">Share our page</a>';
//        }
//        else {
//            document.getElementById('cp_share_btn').innerHTML = '';
//        }
    });
}

function catalystProgramHide() {
    $('.cp-block').animate({
        'opacity': 0,
        'min-height': '0',
        'height': '0',
        'padding': '0'
    }, 1000, 'linear', function () {
        $(this).removeClass('visible');
    });

    $('#id_fbid').unbind('change');
}

