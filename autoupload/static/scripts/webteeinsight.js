(function ($) {
    "use strict";

    $('[sidebar_toggle]').on('click', function () {
        if ($('.right-sidebar').hasClass('slideInRight')) {
            $('.right-sidebar').addClass('slideInLeft');
            $('.right-sidebar').removeClass('slideInRight');
            $('.main-header.sidebar-header').addClass('slideOutRightSlideBar');
            $('.main-header.sidebar-header').removeClass('slideInRightSlideBar');
            $('.login-box').addClass('slideOutRightLoginBox');
            $('.login-box').removeClass('slideInRightLoginBox');
        } else {
            $('.right-sidebar').addClass('slideInRight');
            $('.right-sidebar').removeClass('slideInLeft');
            $('.main-header.sidebar-header').addClass('slideInRightSlideBar');
            $('.main-header.sidebar-header').removeClass('slideOutRightSlideBar');
            $('.login-box').addClass('slideInRightLoginBox');
            $('.login-box').removeClass('slideOutRightLoginBox');
        }
    });

    $(document).ready(function () {
        fix_css();
        mark_active($(window).scrollTop());
        $(window).resize(function () {
            fix_css();
            applySKScroll();
        });

        $('#right-bar a[href*="#"]:not([href="#"]), a[role="try-it"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

                var step = 100;
                if (target.attr('id') == "learn") {
                    step = 25;
                }
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top - 100
                    }, 1000);
                    $('#right-bar li').removeClass('active');
                    $(this).parent('li').addClass('active');
                    return false;
                }
            }
        });

        var lastScrollTop = 0;
        $(window).scroll(function (event) {
            var st = $(this).scrollTop();
            //console.log('st = ' + st);

            mark_active(st);

            if (st == 0) {
                $('.logo-tee-insight img').addClass('zoomOutLogo');
                $('.logo-tee-insight img').removeClass('zoomInLogo');
                $('.main-header').removeClass('scroll');
            }

            if (st > lastScrollTop) {
                $('.logo-tee-insight img').addClass('zoomInLogo');
                $('.logo-tee-insight img').removeClass('zoomOutLogo');
                $('.main-header').addClass('scroll');
            }

            if (st > lastScrollTop) {
                slideSvg($("#Layer_1"), 500, st);
                slideSvg($("#Layer_2"), 600, st);
                slideSvg($("#Layer_3"), 500, st);
                slideSvg($("#Layer_4"), 500, st);
                slideSvg($("#Layer_5"), 500, st);
                slideSvg($("#Layer_6"), 500, st);
            }

            //if (st + $(window).height() / 2 > $('.detail-testimonial-faq').offset().top && !$('.detail-testimonial').hasClass('fadeIn')) {
            //    $('.detail-testimonial').addClass('fadeIn');
            //    $('.detail-faq').addClass('fadeIn');
            //}

            lastScrollTop = st;
        });
    });

    function slideSvg(svgObj, stepView, currentScroll) {
        if (svgObj && svgObj.parent().parent().offset()) {
            var objTop = Number(svgObj.parent().parent().offset().top);

            var h = svgObj.parent().parent().height();
            var perComp = (currentScroll - objTop + stepView) / h * 100;

            if (currentScroll > objTop - stepView) {
                //console.log('objtop: ' + objTop + ', h=' + h + ', percom=' + perComp);
                setDashOffset(svgObj, perComp);
            }
        }
    }

    function setDashOffset(objParent, percentComp) {
        var totalLen = 0;
        objParent.children('.dashed').each(function () {
            var line = $(this).get(0);
            totalLen += getLength($(this).get(0));
        });
        //console.log('len= '+totalLen);
        objParent.children('.path').each(function () {
            var currentPercent = (getLength($(this).get(0)) / totalLen) * 100;
            //console.log('curentper='+currentPercent);
            if ($(window).width() > 992) {
                if (percentComp > 0) {
                    if (percentComp <= currentPercent) {
                        var unitPercent = percentComp * 100 / currentPercent;
                        $(this).css('stroke-dashoffset', 1000 - (unitPercent * 1000 / 100));
                        percentComp = 0;
                    } else {
                        percentComp -= currentPercent;
                        $(this).css('stroke-dashoffset', 0);
                    }
                }
            } else {
                $(this).css('stroke-dashoffset', 0);
            }
        });
    }

    function getLength(obj) {
        var len = 0;
        var x1 = obj.x1 != undefined ? obj.x1.baseVal.value : 0;
        var x2 = obj.x2 != undefined ? obj.x2.baseVal.value : 0;
        var y1 = obj.y1 != undefined ? obj.y1.baseVal.value : 0;
        var y2 = obj.y2 != undefined ? obj.y2.baseVal.value : 0;
        if (Math.abs(x2 - x1) <= 4) {
            len = Math.abs(y2 - y1);
        } else {
            len = Math.abs(x2 - x1);
        }
        return len;
    }

    function dist(x1, x2, y1, y2) {
        return Math.sqrt((x2 -= x1) * x2 + (y2 -= y1) * y2);
    }

    function mark_active(st) {
        $('#right-bar a').each(function () {
            var hash = $(this).attr('href');
            if (hash.length > 0 && $(hash).offset()) {
                if (st > $(hash).offset().top - 400) {
                    $('#right-bar li').removeClass('active');
                    $(this).parent().addClass('active');
                }
            }
        });
    }

    function fix_css() {
        $('.wrapper-content.slide-content, .sidebar-opened .right-sidebar').height($(window).height());
        $('[mid-content]').css('margin-top', $(window).height() * 0.25);
    }

    function applySKScroll() {
        if ($(window).width() > 992) {
            var s = skrollr.init();
        }
    }

    applySKScroll();
})(jQuery);
