from django.conf.urls import patterns, url
from autoupload import views


urlpatterns = patterns(
    '',
    # samples page
    url(r'^samples/$',
        views.samples, name='samples'),

    # web homepage page
    url(r'^web/$',
        views.webteeinsight, name='webteeinsight'),

    # autoupload pages
    # default (vi)
    url(r'^autoupload/$',
        views.autoupload_vi, name='autoupload'),
    url(r'^autoupload/subscribe/$',
        views.subscribe_vi, name='autoupload_subscribe'),
    url(r'^autoupload/subscribe/thanks/$',
        views.subscribe_tks_vi, name='autoupload_subscribe_thanks'),
    # en
    url(r'^autoupload-en/$',
        views.autoupload, name='autoupload_en'),
    url(r'^autoupload-en/subscribe/$',
        views.subscribe, name='autoupload_subscribe_en'),
    url(r'^autoupload-en/subscribe/thanks/$',
        views.subscribe_tks, name='autoupload_subscribe_thanks_en'),
    # vi
    url(r'^autoupload-vi/$',
        views.autoupload_vi, name='autoupload_vi'),
    url(r'^autoupload-vi/subscribe/$',
        views.subscribe_vi, name='autoupload_subscribe_vi'),
    url(r'^autoupload-vi/subscribe/thanks/$',
        views.subscribe_tks_vi, name='autoupload_subscribe_thanks_vi'),

    # api subscribe for autoupload page
    url(r'^api/subscribe-autoupload/$',
        views.APISubscribeAutoUpload.as_view(), name='api_subscribe_autoupload'),
    url(r'^api/check-email/$',
        views.APICheckEmailSubscribeAU.as_view(), name='api_check_email_sau'),

)
