from email.mime.application import MIMEApplication
import os
from os.path import basename
import urllib.parse
import requests
import smtplib
from django.conf import settings
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from openpyxl import Workbook, load_workbook
from rest_framework.response import Response
from rest_framework.views import APIView
from teeinsight.decorators import superuser_login_required
from teeinsight.serializers import *
from teeinsight.utils import *
from datetime import datetime


# ##
# PAGEs
# ##
# samples page
@superuser_login_required
def samples(request):
    # Default Form
    currency = 'USD'
    location = 'US'
    sandbox_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'
    merchant = 'ntphat691-facilitator@gmail.com'
    cancel_url = 'http://teeinsight.com/samples/'
    return_url = 'http://teeinsight.com/samples/'

    pack = request.POST.get('pack', 'Trial')
    platforms = request.POST.get('platform', None)
    # print(platforms, type(platforms))
    if type(platforms) is str:
        platforms = dict(request.POST)['platform']
        # print(platforms, type(platforms))
    if platforms is None:
        platforms = ['Sunfrog', 'Teespring', 'Teechip', 'Viralstyle']
        # print(platforms, type(platforms))
    plan = request.POST.get('plan', 'Monthly')
    cycle_number = request.POST.get('cycle_number', '1')
    limit = request.POST.get('limit', '')
    amount = float(request.POST.get('total', '0'))
    cycle_period = 'M'
    cycle_period_display = 'Month'

    data_instance = {
        'title': 'Samples Paypal',
        'active': ['samples'],
        'default': {
            'pack': pack,
            'platforms': platforms,
            'plan': plan,
            'cycle_number': cycle_number,
            'limit': limit,
            'amount': amount,
            'currency': currency,
            'cycle_period_display': cycle_period_display,
        }
    }

    # print('POST', request.POST)
    # print(plan, platforms)
    # Process
    if request.method == 'POST':
        if plan == 'Daily':
            pass
        elif plan == 'Weekly':
            cycle_period = 'W'
            cycle_period_display = 'Week'
        elif plan == 'Monthly':
            cycle_period = 'M'
            cycle_period_display = 'Month'
        elif plan == 'Yearly':
            cycle_period = 'Y'
            cycle_period_display = 'Year'

        data_instance['default'] = {
            'pack': pack,
            'platforms': platforms,
            'plan': plan,
            'cycle_number': cycle_number,
            'limit': limit,
            'amount': amount,
            'currency': currency,
            'cycle_period_display': cycle_period_display,
        }

        item_number = name_generator(size=16, chars=string.ascii_uppercase + string.digits)
        data_instance['process'] = {
            'sandbox_url': sandbox_url,
            'merchant': merchant,
            'location': location,
            'item_name': pack + ' - ' + plan + ' - ' + item_number,
            'platforms': ', '.join(platforms),
            'item_number': item_number,
            'limit': limit,
            'amount': amount,
            'cycle_number': cycle_number,
            'cycle_period': cycle_period,
            'currency': currency,
            'cancel_url': cancel_url,
            'return_url': return_url,
        }

    # PDT Listener
    tx = request.GET.get('tx', None)  # Paypal transaction ID
    data_instance['tx'] = tx
    if tx:
        # data_instance['st'] = request.GET.get('st', None)  # Paypal product status
        # data_instance['amt'] = request.GET.get('amt', None)  # Paypal received amount value
        # data_instance['cc'] = request.GET.get('cc', None)  # Paypal received currency type
        # data_instance['cm'] = request.GET.get('cm', None)
        # data_instance['item_number'] = request.GET.get('item_number', None)
        # data_instance['sig'] = request.GET.get('sig', None)
        # data_instance['item_number'] = request.GET.get('item_number', None)  # Product ID

        r = requests.post(
            "https://www.sandbox.paypal.com/cgi-bin/webscr",
            data={
                'cmd': '_notify-synch',
                'tx': tx,
                'at': 'teBvKoWO_uzQpNW1SoyawJTYRVcSlV3zBVycB9hcISH5Pi2_iG-TL_C00DC'
            }
        )
        tx_array = urllib.parse.unquote_plus('status=' + r.text).split('\n')
        tx_response = dict([m.groups()
                            for l in tx_array
                            for m in [re.search('([\w_]+)=(.*)', l), re.search('(Error):(.*)', l)] if m])
        data_instance['tx_response'] = tx_response
        data_instance['st'] = tx_response.get('payment_status', None)

    return render(request, 'autoupload/samples.html', data_instance)


# webteeinsight page
def webteeinsight(request):
    data_instance = {
        'title': 'Home page'
    }
    return render(request, 'webteeinsight.html', data_instance)


def autoupload(request):
    data_instance = {
        'title': 'Auto Upload Catalyst Product',
        'lang': 'en'
    }
    return render(request, 'autoupload/autoupload.html', data_instance)


def autoupload_vi(request):
    data_instance = {
        'title': 'Auto Upload Catalyst Product',
        'lang': 'vi'
    }
    return render(request, 'autoupload/autoupload-vi.html', data_instance)


def subscribe(request):
    data_instance = {
        'title': 'Subscribe',
        'lang': 'en'
    }
    return render(request, 'autoupload/subscribe.html', data_instance)


def subscribe_vi(request):
    data_instance = {
        'title': 'Đăng ký',
        'lang': 'vi'
    }
    return render(request, 'autoupload/subscribe-vi.html', data_instance)


def subscribe_tks(request):
    data_instance = {
        'title': 'Thank you!',
        'lang': 'en'
    }
    return render(request, 'autoupload/subscribe-tks.html', data_instance)


def subscribe_tks_vi(request):
    data_instance = {
        'title': 'Cảm ơn',
        'lang': 'vi'
    }
    return render(request, 'autoupload/subscribe-tks-vi.html', data_instance)


# ##
# PARTs
# ##


# ##
# APIs
# ##
class APISubscribeAutoUpload(APIView):
    @csrf_exempt
    def post(self, request):
        params = request.data
        files = request.FILES
        status = 'begin'
        msg = []
        now = timezone.now()
        api = "http://autoupload.teeinsight.com/api/users/register/"

        name = params.get('name', None)
        phone = params.get('phone', None)

        emails_file = params.get('emails_file', None)
        file = files.get('emails_file', None)
        emails_file_name = None
        emails_file_ext = None
        emails_file_path = None
        emails_file_url = None
        emails = []
        email = params.get('email', None)
        recipient = [email]

        licenses = int(params.get('licenses', 1))
        licenses_file_path = None
        licenses_file_url = None
        license_keys = []
        license_key = None

        pack_name = params.get('_pack_id', None)
        pack = 'paid' if pack_name != 'trial' else 'trial'
        is_trial_pack = False if pack_name != 'trial' else True
        is_upgrade = False
        platform_opts = params.get('_platforms', '')

        discount = params.get('_discount', False) == 'true'
        total = round(float(params.get('_total', 0.0)), 3)

        fbid = params.get('_fbid', None)

        if file:
            # only paid-pack (lite/bundle)
            # save file
            try:
                if file.size > (2 * 1024 * 1024):
                    return Response({
                        'error': 1,
                        'message': ['File is not valid!']
                    })

                emails_file_name, emails_file_ext = os.path.splitext(file.name)
                file_name = name_generator() + emails_file_ext
                path = "/".join(['autoupload'])
                local_path = "/".join([settings.MEDIA_ROOT, path])
                if not os.path.exists(local_path):
                    os.makedirs(local_path)
                emails_file_path = "/".join([local_path, file_name])
                with open(emails_file_path, 'wb+') as destination:
                    for chunk in file.chunks():
                        destination.write(chunk)
                emails_file = "/".join(['media', path, file_name])
                msg.append('Saved file!')
            except ValueError:
                return Response({
                    'error': 1,
                    'message': ['Can not save file!']
                })
            # read file
            workbook = load_workbook(emails_file_path)
            first_sheet = workbook.worksheets[0]
            a_col = first_sheet['A']
            # update values
            emails = [c.value for c in a_col][:licenses]
            email = emails[0]
            if not is_email_allowed(email):
                return Response({
                    'error': 1,
                    'message': ['Sorry, your email address has been denied!']
                })
            recipient = [emails[0]]
            # create excel licenses_key file
            licenses_file_name = name_generator() + '.xlsx'
            path = "/".join(['licenses_key'])
            local_path = "/".join([settings.MEDIA_ROOT, path])
            if not os.path.exists(local_path):
                os.makedirs(local_path)
            licenses_file_path = "/".join([local_path, licenses_file_name])
            licenses_file_url = "/".join(['media', path, licenses_file_name])
            # save workbook
            wb_licenses = Workbook()
            ws_licenses = wb_licenses.create_sheet("licenses_key", 0)
            for idx, e in enumerate(emails):
                ws_licenses.cell(row=idx + 1, column=1).value = e
                ws_licenses.cell(row=idx + 1, column=2).value = platform_opts
                if licenses >= idx + 1:
                    if not is_email_allowed(e):
                        ws_licenses.cell(row=idx + 1, column=3).value = 'Sorry, your email address has been denied!'
                        continue
                    r = requests.post(
                        api,
                        json={
                            'email': e,
                            'platforms': platform_opts
                        }
                    )
                    if r.status_code == 200:
                        api_response = r.json()
                        if api_response.get('error', 0) == 0:
                            key = api_response.get('license')
                        else:
                            key = api_response.get('message')
                    else:
                        key = 'Can not get license key!'
                    license_keys.append(key)
                    ws_licenses.cell(row=idx + 1, column=3).value = key
                else:
                    ws_licenses.cell(row=idx + 1, column=3).value = 'Out of the number of registered!'
            wb_licenses.save(licenses_file_path)
            # update values
            license_key = license_keys[0]
        else:
            # just only 1 email; trial or paid or upgrade
            if not is_email_allowed(email):
                return Response({
                    'error': 1,
                    'message': ['Sorry, your email address has been denied!']
                })
            emails = [email]
            is_upgrade = check_upgrade(email)
            r = requests.post(
                api,
                json={
                    'email': email,
                    'platforms': platform_opts
                }
            )
            if r.status_code == 200:
                api_response = r.json()
                if api_response.get('error', None) == 0:
                    key = api_response.get('license')
                else:
                    key = api_response.get('message')
                    msg.append(key)
            else:
                key = 'Can not get license key!'
                msg.append(key)
            license_keys.append(key)
            # update values
            license_key = license_keys[0]

        if is_trial_pack:
            # print('trial')
            pack = 'trial'
            if check_upgrade(email) is not None:
                return Response({
                    'error': 1,
                    'message': msg
                })
        else:
            if is_upgrade:
                pack = 'upgrade'
                # print('upgrade')
            else:
                pack = 'paid'
                # print('paid')

        msg.append(pack)
        # save data
        subscription_aup = SubscriptionAUp.objects.create(
            name=name,
            phone=phone,
            email=email,
            emails_file=emails_file,
            pack_name=pack_name,
            platforms=platform_opts,
            licenses=licenses,
            discount=discount,
            total=total,
            message=', '.join(msg),
            emails=', '.join(emails),
            license_keys=', '.join(license_keys),
            keys_file=licenses_file_url,
            facebook_id=fbid
        )
        # send mail
        if email not in EMAIL_TEST:
            if licenses_file_path is not None:
                att_files = [licenses_file_path]
            else:
                att_files = []
            if 'Email is already used.' == license_key:
                license_key = 'Please use your last license key'
                if is_trial_pack:
                    return Response({
                        'error': 1,
                        'message': ['Email is already used.']
                    })
            status, _msg = send_mail_to_customer(recipient, pack, subscription_aup, license_key, licenses_file_url, att_files)
            msg.append(_msg)
            if not is_trial_pack:
                status, _msg = send_mail_to_admin(now, pack, subscription_aup, license_key, licenses_file_url, att_files)
                msg.append(_msg)
        else:
            # pass
            msg.append('email_test')
        subscription_aup.message = ', '.join(msg)
        subscription_aup.save()
        return Response({
            'error': 0,
            'message': msg
        })


def check_upgrade(email):
    """
    :param email:
    :return: None - new email
            True - trial email
            False - paid email
    """
    sau = SubscriptionAUp.objects.filter(Q(is_deleted=False)).order_by('-created_at', '-id')
    is_contains = sau.filter(Q(email=email) | Q(emails__contains=email))
    if is_contains.exists():
        s = is_contains.first()
        return s.pack_name == 'trial'
    return None


USD_VND_currency_rate = 22200.00  # default 2016


def send_mail_to_customer(recipient, pack, sub, license_key, licenses_file_url=None, files=[]):
    host = 'smtp.sendgrid.net'
    # host = 'smtp.gmail.com'
    port = 587
    username = "iTracker"
    # username = "teeinsight@itracker.vn"
    password = "iTracker123$%^"
    # password = "itis@1416"
    sender = 'teeinsight@itracker.vn'
    receivers = recipient
    subject = '<AutoUpload@teeinsight - Your {0} Account, {1}>'.format(pack, sub.name)

    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = ', '.join(receivers)
    message['Subject'] = subject.title()

    filename_manual = "/".join([settings.MEDIA_ROOT, 'docs', "AutoUpload@teeinsight_Manual_Sept16_English.pdf"])
    with open(filename_manual, "rb") as f_m:
        part = MIMEApplication(f_m.read(), Name=basename(filename_manual))
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename_manual)
    message.attach(part)

    filename_manual = "/".join([settings.MEDIA_ROOT, 'docs', "AutoUpload@teeinsight_Manual_Sep16.pdf"])
    with open(filename_manual, "rb") as f_m:
        part = MIMEApplication(f_m.read(), Name=basename(filename_manual))
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename_manual)
    message.attach(part)

    filename_import = "/".join([settings.MEDIA_ROOT, 'docs', "Import.xlsx"])
    with open(filename_import, "rb") as f_m:
        part = MIMEApplication(f_m.read(), Name=basename(filename_import))
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename_import)
    message.attach(part)

    for filename in files:
        filename_licenses = filename
        with open(filename_licenses, "rb") as f_m:
            part = MIMEApplication(f_m.read(), Name=basename(filename_licenses))
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename_licenses)
        message.attach(part)

    if len(files) and licenses_file_url:
        text_4 = '<a href="http://teeinsight.com/{0}" target="_blank">[Please see attached excel for your registered Emails]</a>'.format(
            licenses_file_url)
        text_5 = '<a href="http://teeinsight.com/{0}" target="_blank">[Please see attached excel for your respective your license keys]</a>'.format(
            licenses_file_url)
    else:
        text_4 = text_5 = ''
    if pack == 'trial':
        trial = """
            <div>
                <a href="http://teeinsight.com/" target="_blank">
                    <img src="http://teeinsight.com/static/images/logo_mail.png" class="img-responsive" width="160">
                </a>

                <p>Dear <span style="font-size: 1.2em;">{1}</span>,</p>

                <p>Thank you for contacting us for AutoUpload@teeinsight {0} account!</p>

                <p>Please find attached
                    <a href="http://teeinsight.com/media/docs/AutoUpload@teeinsight_Manual_Sep16.pdf" target="_blank">
                        MANUAL file</a>
                    for downloading the product (*) and following its instructions.
                    Kindly read it through carefully before use.<br/>
                    (*): <i>download link is at the first line in "Về Sản Phẩm" Section.</i></p>

                <p>Here below is your account information:</p>

                <p style="padding-left: 30px;">
                    Your ID: <strong style="font-size: 1.3em;">{2}</strong><br/>
                    Your {0} license key number is: <strong style="font-size: 1.3em;">{3}</strong></p>

                <p>Also, we have the
                    <a href="http://teeinsight.com/media/docs/Import.xlsx" target="_blank">template excel</a>
                    for your importing your design configuration into the product,
                    instead of inputing it directly in the product designs board.</p>

                <p>You will have 3 days to access and experience the product.</p>

                <p>If you want to subsribe our product, please submit your subscription at
                    <a href="http://teeinsight.com/autoupload" target="_blank">http://teeinsight.com/autoupload</a></p>

                <p>Again, thank you for your interest in our services.<br/>
                    Should you have any queries, please not hesitate to reach out to us at
                    <a href="mailto:teeinsight@itracker.vn">teeinsight@itracker.vn</a> or our
                    <a href="https://www.facebook.com/teeinsight">facebook fanpage</a></p>

                <p>Cheers,<br/>
                    <strong>iTracker</strong> – <i style="color:#0070c0;">teeinsight project support team.</i></p>

                <p>Please like our <a href="https://www.facebook.com/teeinsight">facebook fanpage</a>
                    here for more immediate updates from us and we can talk, listen more from your feedbacks.
                    Thank you!</p>
            </div>
        """
        content = trial.format(pack, sub.name, sub.email, license_key)
    elif pack == 'paid':
        paid = """
            <div>
                <a href="http://teeinsight.com/" target="_blank">
                    <img src="http://teeinsight.com/static/images/logo_mail.png" class="img-responsive" width="160">
                </a>

                <p>Dear <span style="font-size: 1.2em;">{1}</span>,</p>

                <p>Thank you for contacting us for AutoUpload@teeinsight {0} account!</p>

                <p>Your total subscription amount is
                    <strong style="font-size: 1.2em;">{6} USD</strong>
                    or <strong style="font-size: 1.2em;">{7} VND</strong>.<br/>
                    Please make the payment to our Payment Gate as:</p>
                <ul>
                    <li>By Your Payoneer (PO) Account to our PO at:
                        <a href="mailto:dorichard80@gmail.com" target="_blank" style="font-weight: 600;">
                            dorichard80@gmail.com</a>
                    </li>
                    <li>Or, to our Vietcombank (VCB) Account:
                        <ul>
                            <li style="font-weight: 600;">Account Holder: Mai Chi Cong</li>
                            <li style="font-weight: 600;">Account No.: 0071000637739</li>
                            <li style="font-weight: 600;">Branch: Ho Chi Minh.</li>
                        </ul>
                    </li>
                </ul>

                <p><strong>Important Note</strong>:
                    Payment Content Syntax to recognize you when you make the payment to us for subscription:</p>

                <p>“
                    <mark style="background: #ffff00">Your_Registered_Name</mark>
                    <mark style="background: #ffff00">One_Representative_Registered_Email</mark>
                    <mark style="background: #ffff00">Selected_Platforms (ts, tc, sf, vs)</mark>
                    <mark style="background: #ffff00">No._Of_License</mark>
                    ”
                </p>

                <p><strong>Example</strong>:
                    <mark style="background: #ffff00">Cong</mark>
                    <mark style="background: #ffff00">teeinsight@itracker.vn</mark>
                    <mark style="background: #ffff00">ts, vs</mark>
                    <mark style="background: #ffff00">2</mark>
                    --- meaning my name is Cong, my email is teeinsight@itracker.vn
                    and I want to subscribe on teespring, viralstyle with 2 licenses.
                </p>

                <p>To use the product, kindly find attached
                    <a href="http://teeinsight.com/media/docs/AutoUpload@teeinsight_Manual_Sep16.pdf" target="_blank">
                        MANUAL file</a>
                    for downloading the product (*) and following its instructions.
                    Kindly read it through carefully before use.<br/>
                    (*): <i>download link is at the first line in "Về Sản Phẩm" Section.</i></p>

                <p>Here below are your account(s) information:</p>

                <p style="padding-left: 30px;">
                    Your ID: <strong style="font-size: 1.3em;">{2}</strong>
                    <a href="http://teeinsight.com/media/" target="_blank">{4}</a><br/>
                    Your {0} license key number: <strong style="font-size: 1.3em;">{3}</strong>
                    <a href="http://teeinsight.com/media/" target="_blank">{5}</a></p>

                <p>Also, we have the
                    <a href="http://teeinsight.com/media/docs/Import.xlsx" target="_blank">template excel</a>
                    for your importing your design configuration into the product,
                    instead of inputing it directly in the product designs board.</p>

                <p>Again, thank you for your interest in our services.<br/>
                    Should you have any queries, please not hesitate to reach out to us at
                    <a href="mailto:teeinsight@itracker.vn">teeinsight@itracker.vn</a> or our
                    <a href="https://www.facebook.com/teeinsight">facebook fanpage</a></p>

                <p>Cheers,<br/>
                    <strong>iTracker</strong> – <i style="color:#0070c0;">teeinsight project support team.</i></p>

                <p>Please like our <a href="https://www.facebook.com/teeinsight">facebook fanpage</a>
                    here for more immediate updates from us and we can talk, listen more from your feedbacks.
                    Thank you!</p>
            </div>
        """
        vnd = re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', str(int(round(sub.total * USD_VND_currency_rate, 0))))
        content = paid.format(pack, sub.name, sub.email, license_key, text_4, text_5, sub.total, vnd)
    elif pack == 'upgrade':
        upgrade = """
            <div>
                <a href="http://teeinsight.com/" target="_blank">
                    <img src="http://teeinsight.com/static/images/logo_mail.png" class="img-responsive" width="160">
                </a>

                <p>Dear <span style="font-size: 1.2em;">{1}</span>,</p>

                <p>Thank you for contacting us for your upgrade to Paid Account!</p>

                <p>Your total subscription amount is
                    <strong style="font-size: 1.2em;">{2} USD</strong>
                    or <strong style="font-size: 1.2em;">{3} VND</strong>.<br/>
                    Please make the payment to our Payment Gate as:</p>
                <ul>
                    <li>By Your Payoneer (PO) Account to our PO at:
                        <a href="mailto:dorichard80@gmail.com" target="_blank" style="font-weight: 600;">
                            dorichard80@gmail.com</a>
                    </li>
                    <li>Or, to our Vietcombank (VCB) Account:
                        <ul>
                            <li style="font-weight: 600;">Account Holder: Mai Chi Cong</li>
                            <li style="font-weight: 600;">Account No.: 0071000637739</li>
                            <li style="font-weight: 600;">Branch: Ho Chi Minh.</li>
                        </ul>
                    </li>
                </ul>

                <p><strong>Important Note</strong>:
                    Payment Content Syntax to recognize you when you make the payment to us for subscription:</p>

                <p>“
                    <mark style="background: #ffff00">Your_Registered_Name</mark>
                    <mark style="background: #ffff00">One_Representative_Registered_Email</mark>
                    <mark style="background: #ffff00">Selected_Platforms (ts, tc, sf, vs)</mark>
                    <mark style="background: #ffff00">No._Of_License</mark>
                    ”
                </p>

                <p><strong>Example</strong>:
                    <mark style="background: #ffff00">Cong</mark>
                    <mark style="background: #ffff00">teeinsight@itracker.vn</mark>
                    <mark style="background: #ffff00">ts, vs</mark>
                    <mark style="background: #ffff00">2</mark>
                    --- meaning my name is Cong, my email is teeinsight@itracker.vn
                    and I want to subscribe on teespring, viralstyle with 2 licenses.
                </p>

                <p>Once we receive your payment, we will activate your account immediately.
                    We consider your continous product usage as top priority in our process
                    and we appreciate if you could make the payment as soon as possible.</p>

                <p>Again, thank you for your interest in our services.<br/>
                    Should you have any queries, please not hesitate to reach out to us at
                    <a href="mailto:teeinsight@itracker.vn">teeinsight@itracker.vn</a> or our
                    <a href="https://www.facebook.com/teeinsight">facebook fanpage</a></p>

                <p>Cheers,<br/>
                    <strong>iTracker</strong> – <i style="color:#0070c0;">teeinsight project support team.</i></p>

                <p>Please like our <a href="https://www.facebook.com/teeinsight">facebook fanpage</a>
                    here for more immediate updates from us and we can talk, listen more from your feedbacks.
                    Thank you!</p>
            </div>
        """
        vnd = re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', str(int(round(sub.total * USD_VND_currency_rate, 0))))
        content = upgrade.format(pack, sub.name, sub.total, vnd)
    else:
        return None, 'no content'
    message.attach(MIMEText(content, 'html'))

    client = smtplib.SMTP(host, port)
    client.ehlo()
    # secure our email with tls encryption
    client.starttls()
    # re-identify ourselves as an encrypted connection
    client.ehlo()
    client.login(username, password)

    try:
        # client.sendmail(sender, receivers, message.as_string())
        client.send_message(message)
        error = 0
        msg = "Successfully sent email to customer"
    except smtplib.SMTPException:
        error = 1
        msg = "Unable to send email to customer"

    client.quit()

    return error, msg


def send_mail_to_admin(time, pack, sub, license_key, licenses_file_url=None, files=[]):
    host = 'smtp.sendgrid.net'
    # host = 'smtp.gmail.com'
    port = 587
    username = "iTracker"
    # username = "teeinsight@itracker.vn"
    password = "iTracker123$%^"
    # password = "itis@1416"
    sender = 'teeinsight@itracker.vn'
    receivers = [
        'phong.do@vietdev.vn',
        'teeinsight@itracker.vn',
        # 'ntphat6691@gmail.com'
    ]
    subject = 'AutoUpload@teeinsight - New Subscription ({0}) [{1}]'.format(pack, time.strftime("%B %d, %Y %X"))

    message = MIMEMultipart()
    message['From'] = sender
    message['To'] = ', '.join(receivers)
    message['Subject'] = subject.title()

    for filename in files:
        filename_licenses = filename
        with open(filename_licenses, "rb") as f_m:
            part = MIMEApplication(f_m.read(), Name=basename(filename_licenses))
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(filename_licenses)
        message.attach(part)

    body = """
        <div>
            Time: <strong>{0}</strong> <br/>
            Name: <strong>{1}</strong> <br/>
            Phone: <strong>{2}</strong> <br/>
            Email: <strong>{3}</strong> <br/>
            {4}
            Licenses: {5} <br/>
            Pack: {6} <br/>
            Platforms: <strong>{7}</strong> <br/>
            {8} <br/>
            Total: <strong>{9}</strong> USD or <strong>{10}</strong> VND
        </div>
    """
    if len(files) and licenses_file_url:
        text_att = '<a href="http://teeinsight.com/{0}" target="_blank">[Please see attached excel]</a>'.format(
            licenses_file_url)
    else:
        text_att = ''
    if license_key == 'Please use your last license key':
        key_row = 'License key: {0} <br/>'.format(text_att)
    else:
        key_row = 'License key: {0} {1} <br/>'.format(license_key, text_att)
    if sub.discount:
        discount_row = 'Discount: {0} (3 months)'.format(sub.discount)
    else:
        discount_row = 'Discount: {0}'.format(sub.discount)
    vnd = re.sub("^(-?\d+)(\d{3})", '\g<1>,\g<2>', str(int(round(sub.total * USD_VND_currency_rate, 0))))
    content = body.format(time, sub.name, sub.phone, sub.email, key_row, sub.licenses, sub.pack_name, sub.platforms, discount_row, sub.total, vnd)

    message.attach(MIMEText(content, 'html'))

    client = smtplib.SMTP(host, port)
    client.ehlo()
    # secure our email with tls encryption
    client.starttls()
    # re-identify ourselves as an encrypted connection
    client.ehlo()
    client.login(username, password)

    try:
        # client.sendmail(sender, receivers, message.as_string())
        client.send_message(message)
        error = 0
        msg = "Successfully sent email to admin"
    except smtplib.SMTPException:
        error = 1
        msg = "Unable to send email to admin"

    client.quit()

    return error, msg


class APICheckEmailSubscribeAU(APIView):
    @csrf_exempt
    def post(self, request):
        email = request.data.get('email', None)
        if email:
            if not is_email_allowed(email):
                return Response({
                    'error': 1,
                    'message': ['Sorry, your email address has been denied!']
                })
            check = check_upgrade(email)
            if check is not None:
                if check:
                    msg = 'upgrade'
                else:
                    msg = 'exists'
            else:
                msg = 'trial'
        else:
            return Response({
                'error': 1,
                'message': ['Need 1 email to check!']
            })

        return Response({
            'error': 0,
            'message': msg
        })
