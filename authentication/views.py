from authentication.forms import *
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.shortcuts import render
from django.views.decorators.csrf import csrf_protect
from teeinsight.decorators import superuser_login_required


pre_title = 'TeeInsight | '
suf_title = ''

body_class_lockscreen = 'hold-transition lockscreen'
body_class_login = 'hold-transition login-page'
body_class_register = 'hold-transition register-page'


@login_required(redirect_field_name='next')
def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            # send_mail(
            #     cd['subject'],
            #     cd['message'],
            #     cd.get('email', 'noreply@simplesite.com'),
            #     ['administrator@simplesite.com'],  # email address where message is sent.
            # )
            message = 'Thanks for contacting us!'
        else:
            message = 'Please correct the errors below.'
    else:
        form = ContactForm(initial={'subject': 'Simplesite Feedback'})
        message = 'To send us a message fill out the below form.'
    body_class_instance = body_class_login
    data_instance = {
        'title': pre_title + 'Contact' + suf_title,
        'body_class': body_class_instance,
        'active': ['contact'],
        'form': form,
        'message': message
    }
    return render(request, 'registration/contact.html', data_instance)


# registration
@superuser_login_required
def lockscreen(request):
    body_class_instance = body_class_lockscreen
    data_instance = {
        'title': pre_title + 'Lockscreen' + suf_title,
        'body_class': body_class_instance,
        'active': ['lockscreen', 'registration']
    }
    return render(request, 'registration/lockscreen.html', data_instance)


@csrf_protect
def register(request):
    if request.method == 'POST':
        form = ExpandRegistrationForm(request.POST)
        if form.is_valid() and request.POST.get('apply', None):
            form.save()
            message = 'Registration Completed Successfully!!!'
        else:
            message = 'Please try again'
    else:
        form = ExpandRegistrationForm()
        message = 'Register a new membership'
    body_class_instance = body_class_register
    data_instance = {
        'title': pre_title + 'Registration Page' + suf_title,
        'body_class': body_class_instance,
        'active': ['register', 'registration'],
        'form': form,
        'message': message
    }

    return render(request, 'registration/register.html', data_instance)


