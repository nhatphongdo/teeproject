from django.conf.urls import patterns, url
from authentication import views
from teeinsight.forms import EmailAuthenticationForm


urlpatterns = patterns(
    '',
    # Auth-related URLs:
    # url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'authentication_form': EmailAuthenticationForm}, name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/accounts/login/'}, name='logout'),
    # Registration URLs
    url(r'^accounts/register/$', views.register, name='register'),
    url(r'^lockscreen/$', views.lockscreen, name='lockscreen'),
    # Change Password URLs:
    url(r'^accounts/password_change/$', 'django.contrib.auth.views.password_change',
        {'post_change_redirect': '/accounts/password_change/done/'}, name="password_change"),
    (r'^accounts/password_change/done/$', 'django.contrib.auth.views.password_change_done'),
    # Password Reset URLs:
    url(r'^accounts/password_reset/$', 'django.contrib.auth.views.password_reset',
        {'post_reset_redirect': '/accounts/password_reset/done/'}, name="password_reset"),
    url(r'^accounts/password_reset/done/$', 'django.contrib.auth.views.password_reset_done'),
    url(r'^accounts/reset/(?P<uidb64>[0-9A-Za-z]{1,13})-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': '/accounts/reset/done/'}),
    url(r'^accounts/reset/done/$', 'django.contrib.auth.views.password_reset_complete'),

    url(r'^contact/$', views.contact, name='contact'),
)
