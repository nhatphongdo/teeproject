"""
Django settings for teeproject project.
"""

import os
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP


VERSION = '0.0.1'

PROJECT_ROOT = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

# ================================================ #
ROOT_URLCONF = 'teeproject.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'teeproject.wsgi.application'

# Make this unique, and don't share it with anybody.
SECRET_KEY = '=-oe(&wq!e7yortv^qhxqy742s7_((u(tlthth^rm4urw^g97h'

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# ================================================ #
LOGIN_URL = '/accounts/login/'

# Django 's default is to redirect to /accounts/profile when you log in
LOGIN_REDIRECT_URL = '/'

ADMINS = (
    ('Phat Nguyen', 'ntphat6691@gmail.com'),
)

MANAGERS = ADMINS

# Allow all host headers
ALLOWED_HOSTS = ['*']

SITE_ID = 1

INTERNAL_IPS = ('127.0.0.1', 'localhost')


# ================================================ #
DEBUG = True
TEMPLATE_DEBUG = DEBUG

# Debug Toolbar settings
DEBUG_TOOLBAR_PATCH_SETTINGS = False


# ================================================ #
# Application settings
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 86400  # 1 day
SHORT_PERIOD_SESSION = 15  # 15 minutes
LONG_PERIOD_SESSION = 30 * 24 * 60  # 30 days


# ================================================ #
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    # 'django.contrib.webdesign',
    'django.contrib.sessions',
    # 'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'djangobower',
    'compressor',
    'rest_framework',
    'rest_framework.authtoken',

    'authentication',
    'autoupload',
    'teeinsight',

    'debug_toolbar',
)


# ================================================ #
# Database
DATABASES = {
    'default': {
        'ENGINE': 'mysql.connector.django',
        'NAME': 'teeinsight',
        'USER': 'teeinsight',
        'PASSWORD': 'teeinsight123!@#',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}


# ================================================ #
BOWER_COMPONENTS_ROOT = os.path.join(PROJECT_ROOT, 'bower_components').replace('\\', '/')

BOWER_INSTALLED_APPS = (
    'Chart.js',
    'Ionicons',
    'bootstrap',
    'font-awesome',
    'jquery',
    'jquery-mousewheel',
    'moment',
    'respond'
)


# ================================================ #
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.admindocs.middleware.XViewMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.security.SecurityMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)


# ================================================ #
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    # 'DEFAULT_PERMISSION_CLASSES': [
    # 'rest_framework.permissions.IsAuthenticatedOrReadOnly',
    # ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    ),
    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'MAX_PAGINATE_BY': 100,
}


# ================================================ #
# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'uploads').replace('\\', '/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = ''


# ================================================ #
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static').replace('\\', '/')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    # ex.
    # os.path.join(PROJECT_ROOT, 'dist/static'),
    # os.path.join(PROJECT_ROOT, 'static'),
)

STATIC_PRECOMPILER_OUTPUT_DIR = ''

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
    'compressor.finders.CompressorFinder',
    'djangobower.finders.BowerFinder',
)


# ================================================ #
# TEMPLATES = [
#     {
#         'BACKEND': 'django.template.backends.django.DjangoTemplates',
#         'DIRS': [os.path.join(PROJECT_ROOT, 'templates')],
#         'APP_DIRS': True,
#         'OPTIONS': {
#             'context_processors': [
#                 'django.template.context_processors.debug',
#                 'django.template.context_processors.request',
#                 'django.contrib.auth.context_processors.auth',
#                 'django.contrib.messages.context_processors.messages',
#             ],
#         },
#     },
# ]

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

TEMPLATE_DIRS = (
    [os.path.join(PROJECT_ROOT, 'templates')]
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.template.context_processors.debug',
    'django.template.context_processors.i18n',
    'django.template.context_processors.media',
    'django.template.context_processors.static',
    'django.template.context_processors.tz',
    # 'django.template.context_processors.request',
)


# ================================================ #
COMPRESS_ENABLED = os.environ.get('COMPRESS_ENABLED', False)

if os.environ.get('COMPRESS_OFFLINE') is None:
    if DEBUG:
        COMPRESS_OFFLINE = False
    else:
        COMPRESS_OFFLINE = True
        # this is so that compress_offline is set to true during deployment to Heroku

COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile} {outfile}'),
    ('text/coffeescript', 'coffee --compile --stdio'),
)

COMPRESS_CSS_FILTERS = [
    #creates absolute urls from relative ones
    'compressor.filters.css_default.CssAbsoluteFilter',
    #css minimizer
    'compressor.filters.cssmin.CSSMinFilter'
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.jsmin.JSMinFilter'
]

# The URL that linked media will be read from and compressed
# media will be written to.
COMPRESS_URL = STATIC_URL
# The absolute file path that linked media will be read from
# and compressed media will be written to.
COMPRESS_ROOT = STATIC_ROOT


# ================================================ #
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Asia/Ho_Chi_Minh'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False


# ================================================ #
SENDGRID_KEY = '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'
SENDGRID_USERNAME = 'teeinsight'
SENDGRID_PASSWORD = 'teeinsight123$%^'
SENDGRID_API_ID = 'OAzzdRoRTpeGR0jCVnAdIA'


# if DEBUG:
#     EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
#     EMAIL_FILE_PATH = os.path.join(PROJECT_ROOT, 'messages').replace('\\', '/')
# else:
#     # SMTP backend
#     EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_FILE_PATH = os.path.join(PROJECT_ROOT, 'messages').replace('\\', '/')
# host: localhost
EMAIL_HOST = 'smtp.sendgrid.net'
# username: ''
EMAIL_HOST_USER = 'teeinsight'
# password: ''
EMAIL_HOST_PASSWORD = 'teeinsight123$%^'
# port: 25
EMAIL_PORT = '587'
# subject prefix: '[Django] '
EMAIL_SUBJECT_PREFIX = '[Teeinsight] '
# use_tls: False
EMAIL_USE_TLS = True
# use_ssl: False
# EMAIL_USE_SSL = True
# if EMAIL_USE_TLS or EMAIL_USE_SSL:
#     #ssl_keyfile: None
#     EMAIL_SSL_KEYFILE = None
#     #ssl_certfile: None
#     EMAIL_SSL_CERTFILE = None
# timeout: None
# EMAIL_TIMEOUT = None

DEFAULT_FROM_EMAIL = 'no-reply@teeinsight.com'
SERVER_EMAIL = 'administrator@teeinsight.com'


# ================================================ #
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
if not DEBUG:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': '/var/www/log/debug.log',
            }
        },
        'loggers': {
            'django.request': {
                'handlers': ['file'],
                'level': 'DEBUG',
                'propagate': True,
            },
        }
    }
else:
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            }
        },
        'handlers': {
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler'
            }
        },
        'loggers': {
            'django.request': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }


# ================================================ #
# Specify the default test runner.
TEST_RUNNER = 'django.test.runner.DiscoverRunner'


# ================================================ #
# Name: test facilitator
# Email: ntphat691-facilitator@gmail.com
# Merchant account ID: LL7W47JGLMGPC

# Payment Data Transfer
# Identity Token: teBvKoWO_uzQpNW1SoyawJTYRVcSlV3zBVycB9hcISH5Pi2_iG-TL_C00DC
# Return URL:       http://teeinsight.com/samples/

# Instant Payment Notification (IPN)
# Notification URL: [OFF]


# API Signature
# API Username: ntphat691-facilitator_api1.gmail.com
# API Password: WZWASTB47MUMSDG8
# Signature: AFcWxV21C7fd0v3bYYYRCpSSRl31AFGStA6vqhK8TsiV3x28t3FGkfcs
# Request Date:	Jun 30, 2016 20:59:27 PDT

# REST API apps
# App display name:camplans
# SANDBOX API CREDENTIALS
# Sandbox account: ntphat691-facilitator@gmail.com
# Client ID: AUq_JzW6tuvvSeZWDEP80sXoCmDOY_VYSMCbNJVXHFM7JmMr-wYkj0XGzQstDGjKjmZrXZdcWgB6T-po
# Secret: EBcRfVo8OI2GIJUjE967J1QhwnoRIvCysCnmLTXkotNinGkTAW3JLlCxYdA2e7P5uydmHeSAX0ER0cOR
# Aug 16, 2016 Enabled

PAYPAL_MODE = 'sandbox'  # sandbox or live
PAYPAL_CLIENT_ID = 'AUq_JzW6tuvvSeZWDEP80sXoCmDOY_VYSMCbNJVXHFM7JmMr-wYkj0XGzQstDGjKjmZrXZdcWgB6T-po'
PAYPAL_CLIENT_SECRET = 'EBcRfVo8OI2GIJUjE967J1QhwnoRIvCysCnmLTXkotNinGkTAW3JLlCxYdA2e7P5uydmHeSAX0ER0cOR'


# ================================================ #
# Name: test facilitator
# Email: itracker.family-facilitator@gmail.com
# Merchant account ID: PPX2ACUVHGE9S

# Payment Data Transfer
# Identity Token: SXGfPQnu53ZnpbPYWl25T9dSvTPq3ZMHOxvJc8IgdfGdLGAdAFsNoaVPoEG
# Return URL:       http://teeinsight.com/packages/pdt-listener/

# Instant Payment Notification (IPN)
# Notification URL: http://teeinsight.com/packages/ipn-listener/


# API Signature
# API Username: itracker.family-facilitator_api1.gmail.com
# API Password: 59XBKQV7CTWJZMAL
# Signature: AKtHAWL-t3xgSCKhXt58pfqTCx0xAWBIFMxB4YAt1cYW.baz1LLvTX7.
# Request Date: Jun 28, 2016 00:51:21 PDT

