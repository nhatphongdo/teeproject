import os, sys
from django.core.wsgi import get_wsgi_application
from dj_static import Cling

# Calculate the path based on the location of the WSGI script.
apache_configuration= os.path.dirname(__file__)
project = os.path.dirname(apache_configuration)
workspace = os.path.dirname(project)
sys.path.append(workspace)
sys.path.append(project)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "teeproject.settings")

application = Cling(get_wsgi_application())
