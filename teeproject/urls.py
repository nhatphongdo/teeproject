from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()
# The page_not_found() view is overridden by handler404:
handler404 = 'teeinsight.views.teeinsight_page_not_found'
# The server_error() view is overridden by handler500:
handler500 = 'teeinsight.views.teeinsight_error'
# The permission_denied() view is overridden by handler403:
handler403 = 'teeinsight.views.teeinsight_permission_denied'
# The bad_request() view is overridden by handler400:
handler400 = 'teeinsight.views.teeinsight_bad_request'

urlpatterns = patterns(
    '',
    url(r'^$', 'autoupload.views.autoupload_vi', name='home'),

    url(r'^(?i)media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'', include('authentication.urls')),

    url(r'', include('autoupload.urls')),

    url(r'', include('teeinsight.urls')),
)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns(
        '',
        url(r'^(?i)debug/', include(debug_toolbar.urls)),
    )
