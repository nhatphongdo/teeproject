import calendar
from collections import namedtuple
import datetime
import json
import random
import re
import string
from django.db import connection
from django.http import HttpResponse
from django.core.paginator import *
from django.db.models import QuerySet
from math import ceil


base_href = ''
pre_title = 'TeeInsight | '
suf_title = ''
MAX_CAMPS = 0
MAX_CAMPS_TITLE = ' [Max campaigns: ' + str(MAX_CAMPS) + ']'


def json_response(response_dict, status=200):
    response = HttpResponse(json.dumps(response_dict), content_type="application/json", status=status)
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    return response


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def set_items(items, attr):
    output = []
    seen = set()
    for item in items:
        if item[attr] not in seen:
            output.append(item)
            seen.add(item[attr])
    return output


def set_items_value_list(items, attr):
    output = []
    seen = set()
    for item in items:
        if item[attr] not in seen:
            output.append(item[attr])
            seen.add(item[attr])
    return output


def get_paginator(list_object, items=10, page=1):
    paginator = Paginator(list_object, items)
    try:
        list_on_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        list_on_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        list_on_page = paginator.page(paginator.num_pages)
    return list_on_page, paginator


# postgresql-x64-9.4
def count_estimate(tbl='campaign'):
    sql_raw = "SELECT reltuples::BIGINT AS estimate FROM pg_class WHERE relname='teeinsight_" + tbl + "'"
    with connection.cursor() as cursor:
        cursor.execute(sql_raw, None)
        row = cursor.fetchone()
        return row[0]


def explain_rows(query=None):
    if query:
        sql_raw = "explain " + query
        with connection.cursor() as cursor:
            cursor.execute(sql_raw, None)
            row = cursor.fetchone()
            m = re.search('(?<=rows=)\d+', row[0])
            if m is not None:
                return int(m.group(0))
    return None


def exe_sql(query=None, out_type='tuple'):
    if query:
        sql_raw = query
        with connection.cursor() as cursor:
            cursor.execute(sql_raw, None)
            if out_type == 'tuple':
                try:
                    rows = cursor.fetchall()
                    return rows
                except:
                    return None
            elif out_type == 'dict':
                try:
                    rows = dict_fetchall(cursor)
                    return rows
                except:
                    return None
            elif out_type == 'namedtuple':
                try:
                    rows = namedtuple_fetchall(cursor)
                    return rows
                except:
                    return None
    return None


def dict_fetchall(cursor):
    """
    Return all rows from a cursor as a dict
    :param cursor:
    :return:
    """
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]


def namedtuple_fetchall(cursor):
    """
    Return all rows from a cursor as a namedtuple
    :param cursor:
    :return:
    """
    desc = cursor.description
    nt_result = namedtuple('result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def get_camp_id_paginator(list_object, items=10, page=1, estimate_count=None):
    paginator = CampIDPaginator(list_object, items, estimate_count)
    try:
        list_on_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        list_on_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        list_on_page = paginator.page(paginator.num_pages)
    return list_on_page, paginator


class CampIDPaginator(object):
    def __init__(self, query, per_page, estimate_count=None):
        self.query = query
        self.per_page = int(per_page)
        self._count = int(estimate_count) if estimate_count is not None else None
        self._num_pages = None

    def validate_number(self, number):
        try:
            number = int(number)
        except (TypeError, ValueError):
            raise PageNotAnInteger('That page number is not an integer')
        if number < 1:
            raise EmptyPage('That page number is less than 1')
        if number > self.num_pages:
            if number == 1:
                pass
            else:
                raise EmptyPage('That page contains no results')
        return number

    def _get_count(self):
        if self._count is None:
            self._count = explain_rows(self.query)
        return self._count

    count = property(_get_count)

    def _get_num_pages(self):
        if self._num_pages is None:
            if self.count == 0:
                self._num_pages = 0
            else:
                hits = max(1, self.count)
                self._num_pages = int(ceil(hits / float(self.per_page)))
        return self._num_pages

    num_pages = property(_get_num_pages)

    def page(self, number):
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        page_query = self.query + ' limit ' + str(self.per_page) + ' offset ' + str(bottom)
        # print('page_query = "' + page_query + '"')
        page_object_list = exe_sql(page_query, 'namedtuple')
        page_id_list = []
        seen = set()
        try:
            for item in page_object_list:
                if item.id not in seen:
                    page_id_list.append(item.id)
                    seen.add(item.id)
        except (TypeError, ValueError):
            pass
        return page_id_list


def get_camp_paginator(list_object, items=10, page=1, estimate_count=None):
    paginator = CampPaginator(list_object, items, estimate_count)
    try:
        list_on_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        list_on_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        list_on_page = paginator.page(paginator.num_pages)
    return list_on_page, paginator


class CampPaginator(object):
    def __init__(self, object_list, per_page, estimate_count=None):
        self.object_list = object_list
        self.per_page = int(per_page)
        self._count = int(estimate_count) if estimate_count is not None else None
        self._num_pages = None

    def validate_number(self, number):
        try:
            number = int(number)
        except (TypeError, ValueError):
            raise PageNotAnInteger('That page number is not an integer')
        if number < 1:
            raise EmptyPage('That page number is less than 1')
        if number > self.num_pages:
            if number == 1:
                pass
            else:
                raise EmptyPage('That page contains no results')
        return number

    def _get_count(self):
        if self._count is None:
            try:
                self._count = count_estimate('campaign')
            except (AttributeError, TypeError):
                self._count = len(self.object_list)
        return self._count

    count = property(_get_count)

    def _get_num_pages(self):
        if self._num_pages is None:
            if self.count == 0:
                self._num_pages = 0
            else:
                hits = max(1, self.count)
                self._num_pages = int(ceil(hits / float(self.per_page)))
        return self._num_pages

    num_pages = property(_get_num_pages)

    def page(self, number):
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top >= self.count:
            top = self.count
        return self._get_page(self.object_list[bottom:top], number, self)

    def _get_page(self, *args, **kwargs):
        return CampPage(*args, **kwargs)


class CampPage(collections.Sequence):
    def __init__(self, object_list, number, paginator):
        self.object_list = object_list
        self.number = number
        self.paginator = paginator

    def __repr__(self):
        return '<Page %s of %s>' % (self.number, self.paginator.num_pages)

    def __len__(self):
        return len(self.object_list)

    def __getitem__(self, index):
        if not isinstance(index, (slice,) + six.integer_types):
            raise TypeError
        # The object_list is converted to a list so that if it was a QuerySet
        # it won't be a database hit per __getitem__.
        if not isinstance(self.object_list, list):
            self.object_list = list(self.object_list)
        return self.object_list[index]


def name_generator(size=32, chars=string.ascii_uppercase + string.digits + '-'):
    return ''.join(random.choice(chars) for _ in range(size))


def add_months(source_date, months):
    month = source_date.month - 1 + months
    year = int(source_date.year + month / 12 )
    month = month % 12 + 1
    day = min(source_date.day, calendar.monthrange(year, month)[1])
    return datetime.date(year, month,day)


EMAIL_TEST = ['ntphat6691@gmail.com', 'ntphat691@gmail.com']
EMAIL_BLACKLIST = []
# DENY_DOMAIN = []
DENY_DOMAIN = [
    'throwam.com', 'zasod.com', 'mvrht.com', 'yopmail.com', 'yopmail.fr', 'yopmail.net', 'cool.fr.nf', 'jetable.fr.nf',
    'nospam.ze.tc', 'nomail.xl.cx', 'mega.zik.dj', 'speed.1s.fr', 'courriel.fr.nf', 'moncourrier.fr.nf',
    'monemail.fr.nf', 'monmail.fr.nf', 'dropmail.me', 'yomail.info', 'emltmp.com', '10mail.org',
    'mailinator.com', 'xzsok.com'
]


def is_email_allowed(email):
    if email in EMAIL_TEST:
        return True
    if email in EMAIL_BLACKLIST:
        return False
    username, domain = re.search('(.+)@([\w.]+)', email).groups()
    return domain not in DENY_DOMAIN

