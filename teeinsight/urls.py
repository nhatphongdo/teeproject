from django.conf.urls import patterns, url
from teeinsight import views


urlpatterns = patterns(
    '',
    # url(r'^$',
    #     views.dashboard, name='teeinsight_home'),

    # campaign page
    url(r'^campaign/$',
        views.campaign, name='teeinsight_campaign'),
    url(r'^campaign/(?P<slug>[\w\d-]+)/$',
        views.campaign, name='teeinsight_campaign_slug'),

    # campaigns v2 (data table) page
    url(r'^campaigns/.*$',
        views.campaigns, name='teeinsight_campaigns'),

    # dashboard v2 page
    url(r'^dashboard/$',
        views.dashboard, name='teeinsight_dashboard'),

    # packages page
    url(r'^packages/$',
        views.packages, name='teeinsight_packages'),
    url(r'^packages/pdt-listener/.*$',
        views.packages_pdt_listener, name='teeinsight_packages_pdt_listener'),
    url(r'^packages/ipn-listener/$',
        views.packages_ipn_listener, name='teeinsight_packages_ipn_listener'),

    # duplicate page
    url(r'^duplicate/$',
        views.duplicate, name='teeinsight_duplicate'),

    # plans page
    url(r'^plans/$',
        views.plans, name='s_plans'),
    url(r'^plan/create/$',
        views.create_plan, name='s_create_plan'),
    url(r'^plan/(?P<id>[\w\d-]+)/activate/',
        views.activate_plan, name='s_activate_plan'),
    url(r'^plan/(?P<id>[\w\d-]+)/inactivate/',
        views.inactivate_plan, name='s_inactivate_plan'),
    url(r'^plan/(?P<id>[\w\d-]+)/delete/',
        views.delete_plan, name='s_delete_plan'),
    url(r'^plan/(?P<id>[\w\d-]+)/update/',
        views.update_plan, name='s_update_plan'),
    url(r'^plan/(?P<id>[\w\d-]+)/subscribe/.*',
        views.subscribe_plan, name='s_subscribe_plan'),
    url(r'^agreement/execute/.*',
        views.execute_agreement, name='s_execute_agreement'),
    url(r'^agreement/cancel/.*',
        views.cancel_agreement, name='s_cancel_agreement'),
    url(r'^agreement/(?P<id>[\w\d-]+)/',
        views.agreement_details, name='s_agreement_details'),
    url(r'^agreement/(?P<id>[\w\d-]+)/payment-history/(?P<description>.*)/',
        views.payment_history, name='s_payment_history'),


    # get template part
    url(r'^part/campaign/(?P<slug>[\w-]+)/metrics/$',
        views.campaign_metrics_chart, name='teeinsight_campaign_slug_metrics_chart'),

    # get template part
    url(r'^part/dashboard-recently/$',
        views.dashboard_recently, name='teeinsight_dashboard_recently'),
    url(r'^part/dashboard-recently/(?P<status>[\w-]+)/$',
        views.dashboard_recently, name='teeinsight_dashboard_recently_status'),
    url(r'^part/dashboard-hot/$',
        views.dashboard_hot, name='teeinsight_dashboard_hot'),
    url(r'^part/dashboard-hot/(?P<metric>[\w-]+)/(?P<period>[\w-]+)/$',
        views.dashboard_hot, name='teeinsight_dashboard_hot_metric_period'),
    url(r'^part/dashboard-highest/$',
        views.dashboard_highest, name='teeinsight_dashboard_highest'),
    url(r'^part/dashboard-highest/(?P<is_today>[1])/$',
        views.dashboard_highest, name='teeinsight_dashboard_highest_metric_period'),
    url(r'^part/count-today/$',
        views.dashboard_count_today, name='teeinsight_count_today'),
    url(r'^part/count-active/$',
        views.dashboard_count_active, name='teeinsight_count_active'),
    url(r'^part/count-campaigns/$',
        views.dashboard_count_campaigns, name='teeinsight_count_campaigns'),
    url(r'^part/total-sales/$',
        views.dashboard_total_sales, name='teeinsight_total_sales'),


    # api get data for chart
    url(r'^api/campaign/(?P<slug>[\w-]+)/(?P<attr>[\w-]+)/$',
        views.CampaignHist.as_view(), name='teeinsight_campaign_slug_history'),

    # api get data
    url(r'^api/campaigns/$',
        views.APICampaigns.as_view(), name='teeinsight_api_campaigns'),
    url(r'^api/sites/$',
        views.APISites.as_view(), name='teeinsight_api_sites'),

    # api get data
    url(r'^api/count-today/$',
        views.APICountToday.as_view(), name='teeinsight_api_count_today'),
    url(r'^api/count-active/$',
        views.APICountActive.as_view(), name='teeinsight_api_count_active'),
    url(r'^api/count-campaigns/$',
        views.APICountCampaigns.as_view(), name='teeinsight_api_count_campaigns'),
    url(r'^api/total-sales/$',
        views.APITotalSales.as_view(), name='teeinsight_api_total_sales'),

    url(r'^api/search-slug/(?P<slug>[\w\d-]+)/$',
        views.APISearchSlug.as_view(), name='teeinsight_api_search_slug'),
    url(r'^api/search-status/(?P<status>[\d]+)/$',
        views.APISearchStatus.as_view(), name='teeinsight_api_search_status'),
    url(r'^api/search/$',
        views.APISearchCampaigns.as_view(), name='teeinsight_api_search'),

)
