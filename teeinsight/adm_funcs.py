import random
import time
from teeinsight.models import *


def raw_display(value):
    if type(value) is bool or type(value) is int:
        return str(value)
    elif value is None:
        return 'None'
    else:
        return "'" + str(value) + "'"


# create_<>_code
def create_sites_code():
    sites = Site.objects.all()

    f_sites = open('sites_code.py', 'w')
    print('from teeinsight.models import *', file=f_sites)

    done = False
    fields = ['id', 'name', 'url', 'icon', 'created_at', 'updated_at', 'slug', 'is_deleted']

    try:
        for site in sites:
            create_site = "Site.objects.create("
            create_site = create_site + "id=" + raw_display(site.id) + ", "
            create_site = create_site + "name=" + raw_display(site.name) + ", "
            create_site = create_site + "url=" + raw_display(site.url) + ", "
            create_site = create_site + "icon=" + raw_display(site.icon) + ", "
            create_site = create_site + "created_at=" + raw_display(site.created_at) + ", "
            create_site = create_site + "updated_at=" + raw_display(site.updated_at) + ", "
            create_site = create_site + "slug=" + raw_display(site.slug) + ", "
            create_site = create_site + "is_deleted=" + raw_display(site.is_deleted)
            create_site += ")"
            try:
                print(create_site, file=f_sites)
            except (TypeError, ValueError):
                print('# pass ' + site.id, file=f_sites)
                print(site.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_sites.close()
    print('*** SITES *** ', 'successfully! view sites' if done else 'failed!')


def create_campaigns_code():
    campaigns = Campaign.objects.all()

    f_campaigns = open('campaigns_code.py', 'w')
    print('from teeinsight.models import *', file=f_campaigns)

    done = False
    fields = ['id', 'title', 'link', 'campaign_link', 'description', 'seller', 'site', 'language', 'status',
              'front_price', 'front_style', 'front_color', 'currency', 'total_sales', 'goal', 'ratio', 'times_seen',
              'ending_at', 'added_at', 'updated_at', 'created_at', 'times_ran', 'tipped', 'front_image', 'back_image',
              'styles', 'categories', 'facebook', 'twitter', 'google', 'pinterest', 'campaign_id', 'referral_id',
              'referral_platform', 'reference', 'slug', 'is_deleted']

    try:
        for camp in campaigns:
            create_camp = "Campaign.objects.create("
            create_camp = create_camp + "id=" + raw_display(camp.id) + ", "
            create_camp = create_camp + "title=" + raw_display(camp.title) + ", "
            create_camp = create_camp + "link=" + raw_display(camp.link) + ", "
            create_camp = create_camp + "campaign_link=" + raw_display(camp.campaign_link) + ", "
            create_camp = create_camp + "description=" + raw_display(camp.description) + ", "
            create_camp = create_camp + "seller=" + raw_display(camp.seller) + ", "
            create_camp = create_camp + "site_id=" + raw_display(camp.site_id) + ", "
            create_camp = create_camp + "language=" + raw_display(camp.language) + ", "
            create_camp = create_camp + "status=" + raw_display(camp.status) + ", "
            create_camp = create_camp + "front_price=" + raw_display(camp.front_price) + ", "
            create_camp = create_camp + "front_style=" + raw_display(camp.front_style) + ", "
            create_camp = create_camp + "front_color=" + raw_display(camp.front_color) + ", "
            create_camp = create_camp + "currency=" + raw_display(camp.currency) + ", "
            create_camp = create_camp + "total_sales=" + raw_display(camp.total_sales) + ", "
            create_camp = create_camp + "goal=" + raw_display(camp.goal) + ", "
            create_camp = create_camp + "ratio=" + raw_display(camp.ratio) + ", "
            create_camp = create_camp + "times_seen=" + raw_display(camp.times_seen) + ", "
            create_camp = create_camp + "ending_at=" + raw_display(camp.ending_at) + ", "
            create_camp = create_camp + "added_at=" + raw_display(camp.added_at) + ", "
            create_camp = create_camp + "updated_at=" + raw_display(camp.updated_at) + ", "
            create_camp = create_camp + "created_at=" + raw_display(camp.created_at) + ", "
            create_camp = create_camp + "times_ran=" + raw_display(camp.times_ran) + ", "
            create_camp = create_camp + "tipped=" + raw_display(camp.tipped) + ", "
            create_camp = create_camp + "front_image=" + raw_display(camp.front_image) + ", "
            create_camp = create_camp + "back_image=" + raw_display(camp.back_image) + ", "
            create_camp = create_camp + "styles=" + raw_display(camp.styles) + ", "
            create_camp = create_camp + "categories=" + raw_display(camp.categories) + ", "
            create_camp = create_camp + "facebook=" + raw_display(camp.facebook) + ", "
            create_camp = create_camp + "twitter=" + raw_display(camp.twitter) + ", "
            create_camp = create_camp + "google=" + raw_display(camp.google) + ", "
            create_camp = create_camp + "pinterest=" + raw_display(camp.pinterest) + ", "
            create_camp = create_camp + "campaign_id=" + raw_display(camp.campaign_id) + ", "
            create_camp = create_camp + "referral_id=" + raw_display(camp.referral_id) + ", "
            create_camp = create_camp + "referral_platform=" + raw_display(camp.referral_platform) + ", "
            create_camp = create_camp + "reference=" + raw_display(camp.reference) + ", "
            create_camp = create_camp + "slug=" + raw_display(camp.slug) + ", "
            create_camp = create_camp + "is_deleted=" + raw_display(camp.is_deleted)
            create_camp += ")"
            try:
                print(create_camp, file=f_campaigns)
            except (TypeError, ValueError):
                print('# pass ' + camp.id, file=f_campaigns)
                print(camp.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_campaigns.close()
    print('*** CAMPAIGNS *** ', 'successfully! view campaigns' if done else 'failed!')


def create_sales_code():
    sales = Sales.objects.all()

    f_sales = open('sales_code.py', 'w')
    print('from teeinsight.models import *', file=f_sales)

    done = False
    fields = ['id', 'campaign', 'sales', 'recorded_at', 'updated_at']

    try:
        for s in sales:
            create_site = "Sales.objects.create("
            create_site = create_site + "id=" + raw_display(s.id) + ", "
            create_site = create_site + "campaign_id=" + raw_display(s.campaign_id) + ", "
            create_site = create_site + "sales=" + raw_display(s.sales) + ", "
            create_site = create_site + "recorded_at=" + raw_display(s.recorded_at) + ", "
            create_site = create_site + "updated_at=" + raw_display(s.updated_at)
            create_site += ")"
            try:
                print(create_site, file=f_sales)
            except (TypeError, ValueError):
                print('# pass ' + s.id, file=f_sales)
                print(s.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_sales.close()
    print('*** SALES *** ', 'successfully! view sales' if done else 'failed!')


def create_engagement_code():
    engagement = Engagement.objects.all()

    f_engagement = open('engagement_code.py', 'w')
    print('from teeinsight.models import *', file=f_engagement)

    done = False
    fields = ['id', 'campaign', 'engagement', 'created_at', 'updated_at']

    try:
        for e in engagement:
            create_engagement = "Engagement.objects.create("
            create_engagement = create_engagement + "id=" + raw_display(e.id) + ", "
            create_engagement = create_engagement + "campaign_id=" + raw_display(e.campaign_id) + ", "
            create_engagement = create_engagement + "engagement=" + raw_display(e.engagement) + ", "
            create_engagement = create_engagement + "created_at=" + raw_display(e.created_at) + ", "
            create_engagement = create_engagement + "updated_at=" + raw_display(e.updated_at)
            create_engagement += ")"
            try:
                print(create_engagement, file=f_engagement)
            except (TypeError, ValueError):
                print('# pass ' + e.id, file=f_engagement)
                print(e.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_engagement.close()
    print('*** ENGAGEMENT *** ', 'successfully! view engagement' if done else 'failed!')


def create_social_metrics_code():
    social_metrics = SocialMetrics.objects.all()

    f_social_metrics = open('social_metrics_code.py', 'w')
    print('from teeinsight.models import *', file=f_social_metrics)

    done = False
    fields = ['id', 'campaign', 'fb_likes', 'fb_shares', 'fb_comments', 'twitter_tweets', 'google_pluses',
              'pinterest_pins', 'linkedin_shares', 'stumbleupon_stumbles', 'fb_likes_trend', 'fb_shares_trend',
              'fb_comments_trend', 'tweets_trend', 'created_at', 'updated_at']

    try:
        for soc in social_metrics:
            create_soc = "SocialMetrics.objects.create("
            create_soc = create_soc + "id=" + raw_display(soc.id) + ", "
            create_soc = create_soc + "campaign_id=" + raw_display(soc.campaign_id) + ", "
            create_soc = create_soc + "fb_likes=" + raw_display(soc.fb_likes) + ", "
            create_soc = create_soc + "fb_shares=" + raw_display(soc.fb_shares) + ", "
            create_soc = create_soc + "fb_comments=" + raw_display(soc.fb_comments) + ", "
            create_soc = create_soc + "twitter_tweets=" + raw_display(soc.twitter_tweets) + ", "
            create_soc = create_soc + "google_pluses=" + raw_display(soc.google_pluses) + ", "
            create_soc = create_soc + "pinterest_pins=" + raw_display(soc.pinterest_pins) + ", "
            create_soc = create_soc + "linkedin_shares=" + raw_display(soc.linkedin_shares) + ", "
            create_soc = create_soc + "stumbleupon_stumbles=" + raw_display(soc.stumbleupon_stumbles) + ", "
            create_soc = create_soc + "fb_likes_trend=" + raw_display(soc.fb_likes_trend) + ", "
            create_soc = create_soc + "fb_shares_trend=" + raw_display(soc.fb_shares_trend) + ", "
            create_soc = create_soc + "fb_comments_trend=" + raw_display(soc.fb_comments_trend) + ", "
            create_soc = create_soc + "tweets_trend=" + raw_display(soc.tweets_trend) + ", "
            create_soc = create_soc + "created_at=" + raw_display(soc.created_at) + ", "
            create_soc = create_soc + "updated_at=" + raw_display(soc.updated_at)
            create_soc += ")"
            try:
                print(create_soc, file=f_social_metrics)
            except (TypeError, ValueError):
                print('# pass ' + soc.id, file=f_social_metrics)
                print(soc.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_social_metrics.close()
    print('*** SOCIAL METRICS *** ', 'successfully! view social metrics' if done else 'failed!')


def create_packages_code():
    packages = SubscribedPackages.objects.all()

    f_packages = open('packages_code.py', 'w')
    print('from teeinsight.models import *', file=f_packages)

    done = False
    fields = ['id', 'name', 'price', 'description', 'features', 'is_deleted', 'created_at', 'updated_at']

    try:
        for pack in packages:
            create_soc = "SubscribedPackages.objects.create("
            create_soc = create_soc + "id=" + raw_display(pack.id) + ", "
            create_soc = create_soc + "name=" + raw_display(pack.name) + ", "
            create_soc = create_soc + "price=" + raw_display(pack.price) + ", "
            create_soc = create_soc + "description=" + raw_display(pack.description) + ", "
            create_soc = create_soc + "features=" + raw_display(pack.features) + ", "
            create_soc = create_soc + "is_deleted=" + raw_display(pack.is_deleted) + ", "
            create_soc = create_soc + "created_at=" + raw_display(pack.created_at) + ", "
            create_soc = create_soc + "updated_at=" + raw_display(pack.updated_at)
            create_soc += ")"
            try:
                print(create_soc, file=f_packages)
            except (TypeError, ValueError):
                print('# pass ' + pack.id, file=f_packages)
                print(pack.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_packages.close()
    print('*** PACKAGES *** ', 'successfully! view packages' if done else 'failed!')


def create_subscription_code():
    subs = Subscription.objects.all()

    f_subscription = open('subscription_code.py', 'w')
    print('from teeinsight.models import *', file=f_subscription)

    done = False
    fields = ['id', 'user', 'package', 'payment_opt', 'started_at', 'ending_at', 'created_at', 'updated_at']

    try:
        for sub in subs:
            create_sub = "SubscribedPackages.objects.create("
            create_sub = create_sub + "id=" + raw_display(sub.id) + ", "
            create_sub = create_sub + "user=" + raw_display(sub.user) + ", "
            create_sub = create_sub + "package=" + raw_display(sub.package) + ", "
            create_sub = create_sub + "payment_opt=" + raw_display(sub.payment_opt) + ", "
            create_sub = create_sub + "started_at=" + raw_display(sub.started_at) + ", "
            create_sub = create_sub + "ending_at=" + raw_display(sub.ending_at) + ", "
            create_sub = create_sub + "created_at=" + raw_display(sub.created_at) + ", "
            create_sub = create_sub + "updated_at=" + raw_display(sub.updated_at)
            create_sub += ")"
            try:
                print(create_sub, file=f_subscription)
            except (TypeError, ValueError):
                print('# pass ' + sub.id, file=f_subscription)
                print(sub.id)
        done = True
    except (TypeError, ValueError):
        print('error: [', TypeError, '] ', ValueError)
    f_subscription.close()
    print('*** SUBSCRIPTION *** ', 'successfully! view subscription' if done else 'failed!')


def update_metrics(steps=1, minutes=0, hours=1, days=0, weeks=0):
    for camp in Campaign.objects.all():

        # camp 's status
        if camp.status is CAMPAIGN_ACTIVE and camp.ending_at and camp.ending_at <= timezone.now():
            camp.status = CAMPAIGN_ENDED

        # sales_records
        if not camp.sales_records.exists():
            Sales.objects.create(campaign=camp, sales=0)
        elif camp.status is CAMPAIGN_ACTIVE and steps:
            step = random.randrange(1, steps, 1)
            lasted = camp.sales_records.order_by('-updated_at').first()
            lasted_sales = lasted.sales + step
            lasted_delta = timezone.now() - lasted.updated_at
            if lasted_delta >= timedelta(minutes=minutes, hours=hours, days=days, weeks=weeks):
                Sales.objects.create(campaign=camp, sales=lasted_sales)
            camp.total_sales = camp.sales_total()
        # camp 's properties
        if camp.total_sales and camp.goal:
            camp.ratio = camp.total_sales / camp.goal
        camp.updated_at = timezone.now()
        camp.save()

        # engagement
        if not camp.engagement_metrics.exists():
            Engagement.objects.create(campaign=camp, engagement=0)
        elif camp.status is CAMPAIGN_ACTIVE and steps:
            step = random.randrange(1, steps, 1)
            lasted = camp.engagement_metrics.order_by('-updated_at').first()
            lasted_engagement = lasted.engagement + step
            lasted_delta = timezone.now() - lasted.updated_at
            if lasted_delta >= timedelta(minutes=minutes, hours=hours, days=days, weeks=weeks):
                Engagement.objects.create(campaign=camp, engagement=lasted_engagement)

        # social metrics
        if not camp.social_metrics.count():
            SocialMetrics.objects.create(
                campaign=camp,
                fb_likes=0, fb_shares=0, fb_comments=0,
                twitter_tweets=0, google_pluses=0, pinterest_pins=0,
                linkedin_shares=0, stumbleupon_stumbles=0,
                fb_likes_trend=0, fb_shares_trend=0, fb_comments_trend=0, tweets_trend=0)
        elif camp.status is CAMPAIGN_ACTIVE and steps:
            lasted = camp.social_metrics.order_by('-updated_at').first()
            lasted_delta = timezone.now() - lasted.updated_at
            if lasted_delta >= timedelta(minutes=minutes, hours=hours, days=days, weeks=weeks):
                SocialMetrics.objects.create(
                    campaign=camp,
                    fb_likes=lasted.fb_likes + random.randrange(1, steps, 1),
                    fb_shares=lasted.fb_shares + random.randrange(1, steps, 1),
                    fb_comments=lasted.fb_comments + random.randrange(1, steps, 1),
                    twitter_tweets=lasted.twitter_tweets + random.randrange(1, steps, 1),
                    google_pluses=lasted.google_pluses + random.randrange(1, steps, 1),
                    pinterest_pins=lasted.pinterest_pins + random.randrange(1, steps, 1),
                    linkedin_shares=lasted.linkedin_shares + random.randrange(1, steps, 1),
                    stumbleupon_stumbles=lasted.stumbleupon_stumbles + random.randrange(1, steps, 1),
                    fb_likes_trend=lasted.fb_likes_trend + random.randrange(1, steps, 1),
                    fb_shares_trend=lasted.fb_shares_trend + random.randrange(1, steps, 1),
                    fb_comments_trend=lasted.fb_comments_trend + random.randrange(1, steps, 1),
                    tweets_trend=lasted.tweets_trend + random.randrange(1, steps, 1))


def auto_update_metrics(_weeks=0, _days=0, _hours=0, _minutes=0, _seconds=0, _milliseconds=0, _microseconds=0,
                        steps=1, minutes=0, hours=0, days=0, weeks=0, time_sleep=15):
    start_time = timezone.now()
    end_time = start_time + timedelta(
        weeks=_weeks, days=_days, hours=_hours, minutes=_minutes,
        seconds=_seconds, milliseconds=_milliseconds, microseconds=_microseconds)
    while start_time <= timezone.now() <= end_time:
        print(timezone.now())
        update_metrics(steps=steps, minutes=minutes, hours=hours, days=days, weeks=weeks)
        time.sleep(timedelta(minutes=time_sleep).total_seconds())
    print(timezone.now(), ' || End Here! ||')


