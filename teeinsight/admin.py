from django.contrib import admin
from django.utils.html import format_html
from teeinsight.models import *
from django.http import HttpResponse
from django.core import serializers
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect


def export_as_json(model_admin, request, queryset):
    response = HttpResponse(content_type="application/json")
    serializers.serialize("json", queryset, stream=response)
    return response


def export_selected_objects(model_admin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    ct = ContentType.objects.get_for_model(queryset.model)
    return HttpResponseRedirect("/export/?ct=%s&ids=%s" % (ct.pk, ",".join(selected)))


# admin.site.add_action(export_as_json, 'export_json')
# admin.site.add_action(export_selected_objects, 'export_selected')
admin.site.disable_action('delete_selected')


class SiteAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug', 'url_html', 'icon_html', 'is_deleted']
    fields = (('name', 'slug'), 'url', 'icon', 'is_deleted')
    ordering = ['name']
    actions = None

    def url_html(self, obj):
        return format_html('<a href="{}" target="_blank">{}</a>', obj.url, obj.name)

    def icon_html(self, obj):
        return format_html('<img src="{}" class="img-responsive" width=36>', obj.icon)


admin.site.register(Site, SiteAdmin)


class CampaignAdmin(admin.ModelAdmin):
    list_display = ['title', 'slug', 'link', 'site', 'status', 'total_sales']
    ordering = ['title']
    actions = None
    empty_value_display = '-empty-'
    list_filter = ('status', 'site')


admin.site.register(Campaign, CampaignAdmin)


class SalesAdmin(admin.ModelAdmin):
    list_display = ['campaign', 'sales', 'recorded_at']
    ordering = ['campaign']
    actions = None
    date_hierarchy = 'recorded_at'


admin.site.register(Sales, SalesAdmin)


class EngagementAdmin(admin.ModelAdmin):
    list_display = ['campaign', 'engagement', 'created_at']
    ordering = ['campaign']
    actions = None


admin.site.register(Engagement, EngagementAdmin)


class SocialMetricsAdmin(admin.ModelAdmin):
    list_display = ['campaign', 'fb_likes', 'fb_shares', 'fb_comments', 'twitter_tweets', 'google_pluses',
                    'pinterest_pins', 'linkedin_shares', 'stumbleupon_stumbles', 'fb_likes_trend', 'fb_shares_trend',
                    'fb_comments_trend', 'tweets_trend']
    ordering = ['campaign']
    actions = None


admin.site.register(SocialMetrics, SocialMetricsAdmin)


class SubscribedPackagesAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'description', 'features']
    ordering = ['name']
    actions = None


admin.site.register(SubscribedPackages, SubscribedPackagesAdmin)


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ['user', 'package', 'payment_opt', 'started_at', 'ending_at']
    ordering = ['user']
    actions = None


admin.site.register(Subscription, SubscriptionAdmin)


class SubscriptionAUpAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'facebook_profile', 'phone', 'pack_name', 'total', 'platforms', 'license_keys', 'licenses', 'discount', 'emails_file_link', 'created_at']
    ordering = ['-created_at']
    actions = ['delete_selected']
    empty_value_display = '-empty-'

    def emails_file_link(self, obj):
        if obj.emails_file:
            return format_html('<a href="/{}" target="_blank">Open</a>', obj.emails_file)
        else:
            return obj.emails_file

    def facebook_profile(self, obj):
        if obj.facebook_id:
            return format_html('<a href="https://facebook.com/{}" target="_blank">{}</a>', obj.facebook_id, obj.facebook_id)
        else:
            return obj.facebook_id


admin.site.register(SubscriptionAUp, SubscriptionAUpAdmin)