import six
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from django.core.validators import MinValueValidator, MaxValueValidator
from teeinsight.models import *


class BigIntegerField(serializers.Field):
    default_error_messages = {
        'invalid': _('A valid integer is required.'),
        'max_value': _('Ensure this value is less than or equal to {max_value}.'),
        'min_value': _('Ensure this value is greater than or equal to {min_value}.'),
        'max_string_length': _('String value too large.')
    }
    MAX_STRING_LENGTH = 1000  # Guard against malicious string inputs.

    def __init__(self, **kwargs):
        self.max_value = kwargs.pop('max_value', None)
        self.min_value = kwargs.pop('min_value', None)
        super(BigIntegerField, self).__init__(**kwargs)
        if self.max_value is not None:
            message = self.error_messages['max_value'].format(max_value=self.max_value)
            self.validators.append(MaxValueValidator(self.max_value, message=message))
        if self.min_value is not None:
            message = self.error_messages['min_value'].format(min_value=self.min_value)
            self.validators.append(MinValueValidator(self.min_value, message=message))

    def to_internal_value(self, data):
        if isinstance(data, six.text_type) and len(data) > self.MAX_STRING_LENGTH:
            self.fail('max_string_length')

        try:
            data = int(data)
        except (ValueError, TypeError):
            self.fail('invalid')
        return data

    def to_representation(self, value):
        return int(value)


class DynamicFieldsModelSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class DynamicFieldsSerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        # Don't pass the 'fields' arg up to the superclass
        fields = kwargs.pop('fields', None)

        # Instantiate the superclass normally
        super(DynamicFieldsSerializer, self).__init__(*args, **kwargs)

        if fields is not None:
            # Drop any fields that are not specified in the `fields` argument.
            allowed = set(fields)
            existing = set(self.fields.keys())
            for field_name in existing - allowed:
                self.fields.pop(field_name)


class RecursiveField(DynamicFieldsSerializer):
    def to_representation(self, value=None):
        serializer = self.parent.parent.__class__(value, context=self.context, fields=self.parent.parent.fields)
        return serializer.data


class MyItemSerializer(DynamicFieldsSerializer):
    def to_representation(self, obj=None):
        # print('===================================')
        output = {}
        for attribute_name in dir(obj):
            if attribute_name.startswith('_'):
                # Ignore private attributes.
                # print(attribute_name, 'private attributes')
                pass
            elif 'objects' == attribute_name:
                # print(attribute_name, 'objects')
                pass
            else:
                attribute = getattr(obj, attribute_name)

                if hasattr(attribute, '__call__'):
                    # Ignore methods and other callables.
                    # methodList = [method for method in dir(object) if callable(getattr(object, method))]
                    # print(attribute_name, 'methods and other callables')
                    pass
                elif isinstance(attribute, (str, int, bool, float, type(None))):
                    # Primitive types can be passed through unmodified.
                    # print(attribute_name, type(attribute))
                    output[attribute_name] = attribute
                elif isinstance(attribute, list):
                    # Recursively deal with items in lists.
                    # print(attribute_name, 'lists')
                    output[attribute_name] = [
                        self.to_representation(item) for item in attribute
                    ]
                elif isinstance(attribute, dict):
                    # Recursively deal with items in dictionaries.
                    # print(attribute_name, 'dictionaries')
                    output[attribute_name] = {
                        str(key): self.to_representation(value)
                        for key, value in attribute.items()
                    }
                else:
                    # Force anything else to its string representation.
                    # print(attribute_name, 'Force anything else to its string representation')
                    output[attribute_name] = str(attribute)
        # print('===================================')
        # print(output)
        # print('===================================')
        return output


class SalesTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='sales')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class FBLikeTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='fb_likes')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class FBShareTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='fb_shares')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class FBCmtTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='fb_comments')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class TWTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='twitter_tweets')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class GPTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='google_pluses')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class PinTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='pinterest_pins')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class LinTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='linkedin_shares')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class SBTimeValues(DynamicFieldsSerializer):
    time = serializers.ReadOnlyField(source='updated_at')
    value = serializers.ReadOnlyField(source='stumbleupon_stumbles')

    class Meta:
        model = Sales
        fields = ('time', 'value')


class SiteSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = Site
        fields = ('id', 'slug', 'name', 'url', 'icon', 'updated_at', 'created_at', 'is_deleted')


class SocialMetricsSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = SocialMetrics
        fields = ('id', 'fb_likes', 'fb_shares', 'fb_comments', 'twitter_tweets',
                  'google_pluses', 'pinterest_pins', 'linkedin_shares', 'stumbleupon_stumbles',
                  'fb_likes_trend', 'fb_shares_trend', 'fb_comments_trend', 'tweets_trend',
                  'updated_at', 'created_at')


class CampaignsSerializer(DynamicFieldsModelSerializer):
    site = SiteSerializer()
    metrics = SocialMetricsSerializer()

    class Meta:
        model = Campaign
        fields = ('id', 'slug', 'front_image', 'back_image', 'title', 'campaign_link', 'description',
                  'site', 'facebook', 'google', 'pinterest', 'twitter', 'link',
                  'status', 'get_status_display', 'added_at', 'ending_at', 'goal', 'ratio',
                  'sales_now', 'total_sales',
                  'metrics',
                  # 'fb_likes', 'fb_shares', 'fb_comments', 'twitter_tweets', 'google_pluses',
                  # 'pinterest_pins', 'linkedin_shares', 'stumbleupon_stumbles',
                  'currency', 'front_price', 'front_style', 'front_color', 'styles',
                  'times_seen', 'times_ran', 'tipped', 'language', 'get_language_display',
                  'updated_at', 'created_at', 'is_deleted')

        # def to_representation(self, value=None):
        # return super(CampaignsSerializer, self).to_representation(value)


class CampaignsDefaultSerializer(DynamicFieldsModelSerializer):
    class Meta:
        model = Campaign
        fields = ('id', 'slug', 'front_image', 'back_image', 'title', 'campaign_link', 'description',
                  'site', 'facebook', 'google', 'pinterest', 'twitter', 'link',
                  'status', 'get_status_display', 'added_at', 'ending_at', 'goal', 'ratio',
                  'total_sales',
                  'currency', 'front_price', 'front_style', 'front_color', 'styles',
                  'times_seen', 'times_ran', 'tipped', 'language', 'get_language_display',
                  'updated_at', 'created_at', 'is_deleted')


class CampaignModel(object):
    def __init__(self, id='', slug='', front_image='', back_image='', title='', campaign_link='', description='',
                 site={}, facebook='', google='', pinterest='', twitter='', link='',
                 status='', added_at='', ending_at='', total_sales='', goal='', ratio='',
                 metrics={}, sales_now='',
                 currency='', front_price='', front_style='', front_color='', styles='',
                 times_seen='', times_ran='', tipped='', language='', updated_at='', created_at='', is_deleted=''):
        self.id = id
        self.slug = slug
        self.front_image = front_image
        self.back_image = back_image
        self.title = title
        self.campaign_link = campaign_link
        self.description = description
        self.site = site
        self.facebook = facebook
        self.google = google
        self.pinterest = pinterest
        self.twitter = twitter
        self.link = link
        self.status = status
        self.added_at = added_at
        self.ending_at = ending_at
        self.total_sales = total_sales
        self.goal = goal
        self.ratio = ratio
        self.metrics = metrics
        self.sales_now = sales_now
        self.currency = currency
        self.front_price = front_price
        self.front_style = front_style
        self.front_color = front_color
        self.styles = styles
        self.times_seen = times_seen
        self.times_ran = times_ran
        self.tipped = tipped
        self.language = language
        self.updated_at = updated_at
        self.created_at = created_at
        self.is_deleted = is_deleted

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-added_at']


class CampaignModelSerializer(DynamicFieldsModelSerializer):
    id = serializers.ReadOnlyField(source='id')
    slug = serializers.CharField()
    front_image = serializers.CharField()
    back_image = serializers.CharField()
    title = serializers.CharField()
    campaign_link = serializers.URLField()
    description = serializers.CharField()
    site = SiteSerializer()
    facebook = serializers.URLField()
    google = serializers.URLField()
    pinterest = serializers.URLField()
    twitter = serializers.URLField()
    link = serializers.URLField()
    status = serializers.CharField()
    added_at = serializers.DateTimeField()
    ending_at = serializers.DateTimeField()
    total_sales = BigIntegerField()
    goal = serializers.IntegerField()
    ratio = serializers.FloatField()
    metrics = SocialMetricsSerializer(many=True)
    sales_now = serializers.ReadOnlyField(source='sales_now')
    currency = serializers.CharField()
    front_price = serializers.FloatField()
    front_style = serializers.CharField()
    front_color = serializers.CharField()
    styles = serializers.CharField()
    times_seen = serializers.IntegerField()
    times_ran = serializers.IntegerField()
    tipped = serializers.BooleanField()
    language = serializers.ReadOnlyField(source='get_language_display')
    updated_at = serializers.DateTimeField()
    created_at = serializers.DateTimeField()
    is_deleted = serializers.BooleanField()

    class Meta:
        model = CampaignModel
        fields = ('id', 'slug', 'front_image', 'back_image', 'title', 'campaign_link', 'description',
                  'site', 'facebook', 'google', 'pinterest', 'twitter', 'link',
                  'status', 'added_at', 'ending_at', 'total_sales', 'goal', 'ratio',
                  'metrics', 'sales_now',
                  'currency', 'front_price', 'front_style', 'front_color', 'styles',
                  'times_seen', 'times_ran', 'tipped', 'language', 'updated_at', 'created_at', 'is_deleted')


