(function ($) {
    "use strict";

    var attributes = {};

    $('.frequency-btn-check').click(function (event) {
        var this_child = $(this).children('input[type="checkbox"]');
        var name = this_child.attr('name');
        if ($('input[name=' + name + ']:checked').length <= 1) {
            if (this_child.is(":checked")) {
                return false;
            }
        }
    });

    $('[id^=frequency_]').change(function (event) {
        var target = '#' + this.dataset.target;
        if ($(this).is(":checked")) {
            $(target).collapse('show');
        }
        else {
            $(target).collapse('hide');
        }
    });

    $('.frequency-block').on({
        'show.bs.collapse': function () {
        },
        'shown.bs.collapse': function () {
            var name = $(this).find('.frequency-type-btn-check').first().children().attr('name');
            init_block(name);
        },
        'hide.bs.collapse': function () {
        },
        'hidden.bs.collapse': function () {
        }
    });

    $('.frequency-type-btn-check').click(function (event) {
        var this_child = $(this).children('input[type="checkbox"]');
        var name = this_child.attr('name');
        if ($('input[name=' + name + ']:checked').length <= 1) {
            if (this_child.is(":checked")) {
                return false;
            }
        }
    });

    $('[id^=payment_type_]').change(function (event) {
        var target = '#' + this.dataset.target;
        if ($(this).is(":checked")) {
            $(target).collapse('show');
        }
        else {
            $(target).collapse('hide');
        }
    });

    $('.frequency-type-block').on({
        'show.bs.collapse': function () {
        },
        'shown.bs.collapse': function () {
            $(this).find('input[name$="_payment_name"]').prop('required', true);
            $(this).find('input[name$="_frequency_interval"]').prop('required', true);
            $(this).find('input[name$="_amount"]').prop('required', true);
            var name = $(this).find('.charge-btn-check').first().children().attr('name');
            init_block(name);
        },
        'hide.bs.collapse': function () {
        },
        'hidden.bs.collapse': function () {
            $(this).find('input[name$="_payment_name"]').prop('required', false);
            $(this).find('input[name$="_frequency_interval"]').prop('required', false);
            $(this).find('input[name$="_amount"]').prop('required', false);
        }
    });

    $('.charge-btn-check').click(function (event) {
        var this_child = $(this).children('input[type="checkbox"]');
        var name = this_child.attr('name');
        if ($('input[name=' + name + ']:checked').length <= 0) {
            if (this_child.is(":checked")) {
                return false;
            }
        }
    });

    $('[id^=charge_mode_]').change(function (event) {
        var target = '#' + this.dataset.target;
        if ($(this).is(":checked")) {
            $(target).collapse('show');
        }
        else {
            $(target).collapse('hide');
        }
    });

    $('.charge-block').on({
        'show.bs.collapse': function () {
        },
        'shown.bs.collapse': function () {
        },
        'hide.bs.collapse': function () {
        },
        'hidden.bs.collapse': function () {
        }
    });

    $('form .val').change(function () {
        update_attributes();
    });

    $(document).ready(function () {
        init_block('frequency');

        update_attributes();
    });

    function init_block(name) {
        $('input[name=' + name + ']:checked').each(function () {
            var target = '#' + this.dataset.target;
            $(target).collapse('show');
        });
    }

    function update_attributes() {
        var name = $('#name').val();
        var description = $('#description').val();
        var type = $('input[name="type"]:checked').val();

        var currency = $('input[name="currency"]:checked').val();
        var setup_fee = $('#setup_fee').val();
        var return_url = $('#return_url').val();
        var cancel_url = $('#cancel_url').val();
        var max_fail_attempts = $('#max_fail_attempts').val();
        var auto_bill_amount = $('input[name="auto_bill_amount"]:checked').val();
        var initial_fail_amount_action = $('input[name="initial_fail_amount_action"]:checked').val();
        var merchant_preferences = {
            setup_fee: {
                currency: currency,
                value: setup_fee
            },
            return_url: return_url,
            cancel_url: cancel_url,
            max_fail_attempts: max_fail_attempts,
            auto_bill_amount: auto_bill_amount,
            initial_fail_amount_action: initial_fail_amount_action
        };

        var payment_definitions = [];
        var payment_definition = {};
        $('input[name="frequency"]:checked').each(function () {
            var frequency = this.value.toLowerCase();
            var payment_type_frequency = 'payment_type_' + frequency;
            $('input[name=' + payment_type_frequency + ']:checked').each(function () {
                var frequency_type = this.value.toLowerCase();
                var prefix = frequency + '_' + frequency_type + '_';
                var payment_name = $('#' + prefix + 'payment_name').val();
                var frequency_interval = $('#' + prefix + 'frequency_interval').val();
                var cycles = $('#' + prefix + 'cycles').val();
                var amount = $('#' + prefix + 'amount').val();
                var shipping = $('#' + prefix + 'shipping').val();
                var tax = $('#' + prefix + 'tax').val();

                var charge_modes = [];
                var charge_mode_name = 'charge_mode_' + frequency + '_' + frequency_type;
                $('input[name=' + charge_mode_name + ']:checked').each(function () {
                    var charge_mode_type = this.value.toLowerCase();
                    var charge_mode_id = frequency + '_' + frequency_type + '_' + charge_mode_type;
                    var charge_mode = {
                            type: charge_mode_type,
                            amount: {
                                currency: currency,
                                value: $('#' + charge_mode_id).val()
                            }
                        };
                    charge_modes.push(charge_mode);
                });

                payment_definition = {
                    name: payment_name,
                    type: frequency_type,
                    frequency: frequency,
                    frequency_interval: frequency_interval,
                    cycles: cycles,
                    amount: {
                        currency: currency,
                        value: amount
                    }
                };
                if (charge_modes.length){
                    payment_definition.charge_models = charge_modes;
                }
                payment_definitions.push(payment_definition);
            });
        });
        attributes = {
            name: name,
            description: description,
            type: type,
            merchant_preferences: merchant_preferences,
            payment_definitions: payment_definitions
        };
        $('#attributes').text(JSON.stringify(attributes, null, 4));
        $('.suf_currency').text(currency);
    }

})(jQuery);
