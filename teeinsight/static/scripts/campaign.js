(function ($) {
    "use strict";

    var base_href = '';

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        // Pace
        if (typeof Pace != 'undefined') {
            Pace.restart();
        }
        else {
            console.log('Pace is not found!');
        }
    });

    $('[img-preview]').hover(function (e) {
        $('#img-preview').attr('href', $(this).attr('href'));
        $('#img-preview > img').attr('src', $(this).attr('href'));
    });

    $('#img-preview').click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).removeData('_lighter');
        $(this).lighter();
    });

    $('[data-load-remote]').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var remote = $this.data('load-remote');
        if (remote) {
            $($this.data('remote-target')).load(remote);
        }
    });

    //INITIALIZE SPARKLINE CHARTS
    var values = [6, 4, 7, 8, 4, 3, 2, 2, 5, 6],
        spark_options = {
            type: "line",
            width: "50px",
            height: "50px",
            lineColor: "#39CCCC",
            spotColor: "#39CCCC",
            minSpotColor: "#f56954",
            maxSpotColor: "#00a65a",
            spotRadius: 1.5,
            highlightLineColor: "#222",
            highlightSpotColor: "#f39c12",
            lineWidth: 2,
            offset: 90
        },
        url_data_chart = '';

    var camp_slug = $("[camp_slug]").attr('camp_slug');
    $.ajax({
        url: "/part/campaign/" + camp_slug + "/metrics/",
        success: function (result) {
            $('#tab_all').html(result);
            draw_sparkline();

            $('.draw_chart .sparkline').bind('sparklineClick', function (ev) {
                //var sparkline = ev.sparklines[0],
                //    region = sparkline.getCurrentRegionFields();
                //console.log("Clicked on x="+region.x+" y="+region.y);
                values = $(this).closest('.box.draw_chart').data('values');
                url_data_chart = $(this).closest('.box.draw_chart').data('apiurl');

                var $chart_modal = $('#chart_modal');
                $chart_modal.attr('data-apiurl', url_data_chart);
                $chart_modal.modal('show');
            });
        }
    });

    $('.box ul.nav a').on('shown.bs.tab', function () {
        draw_sparkline();
    });

    $('#chart_modal').on('shown.bs.modal', function (event) {
        if (event.relatedTarget != undefined) {
            var button = $(event.relatedTarget);
            values = button.closest('.box.draw_chart').data('values');
            url_data_chart = button.closest('.box.draw_chart').data('apiurl');
        }
        //$('#chart_here .chart').html('<canvas id="line-chart-chartjs" style="height:250px"></canvas>');

        //draw_morris_chart('line-chart-morris');
        draw_chartjs('#line-chart-chartjs');
    });

    var chart_drew = undefined;

    function draw_sparkline() {
        $(".draw_chart .sparkline").each(function () {
            var $this = $(this);
            $this.sparkline(values, spark_options);

            var data_line = $this.closest('.box.draw_chart').data().values;
            if (data_line != undefined && data_line != '' && data_line.length > 1) {
                $this.sparkline(data_line, spark_options);
            }
        });
    }

    function draw_morris_chart(element) {
        $.get(url_data_chart, function (data) {
            $('#chart_modal_title').text(data.label);

            if (typeof chart_drew === 'object') {
                chart_drew.destroy();
            }

            if (data.data.length > 1) {
                chart_drew = new Morris.Line({
                    element: element,
                    resize: true,
                    data: data.data,
                    // required
                    xkey: 'time',
                    // required
                    ykeys: ['value'],
                    // required
                    labels: [data.label],
                    lineColors: ['#3c8dbc'],
                    lineWidth: 2,
                    hideHover: 'auto',
                    gridTextColor: "#000",
                    gridStrokeWidth: 0.4,
                    pointSize: 4,
                    pointStrokeColors: ["#efefef"],
                    gridLineColor: "#efefef",
                    gridTextFamily: "Open Sans",
                    gridTextSize: 10
                });
            }
        });
    }

    function draw_chartjs(element) {
        var lineChartCanvas = $(element).get(0).getContext("2d");
        var lineChartData = {
                labels: [],
                datasets: [
                    {
                        label: "Sales",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(255,75,5,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 2,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(255,75,5,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        pointStyle: 'circle',
                        data: []
                    }
                ]
            },
            lineChartOptions = {
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [
                        {
                            type: 'time',
                            time: {
                                displayFormats: {
                                    quarter: 'MMM YYYY'
                                },
                                tooltipFormat: 'MMM Do, hh:mm A'
                                //round,unit : 'millisecond', 'second', 'minute', 'hour', 'day', 'week', 'month', 'quarter', 'year'
                            }
                        }
                    ]
                }
            };

        $.get(url_data_chart, function (data) {
            $('#chart_modal_title').text(data.label);

            if (typeof chart_drew === 'object') {
                chart_drew.destroy();
            }

            var labels = [],
                values = [];
            if (data.data.length > 1) {
                for (var i = 0; i < data.data.length; i++) {
                    labels.push(moment(data.data[i].time));
                    values.push(data.data[i].value)
                }
                lineChartData.labels = labels;
                lineChartData.datasets[0].data = values;
                lineChartData.datasets[0].label = data.label;

                chart_drew = new Chart(lineChartCanvas, {
                    type: 'line',
                    data: lineChartData,
                    options: lineChartOptions
                });
            }

            $("#time_slider").ionRangeSlider({
                type: "double",
                values: labels,
                min_interval: 2,
                drag_interval: true,
                to: labels.length,
                grid: true,
                force_edges: true,
                prettify: function (num) {
                    return moment(num).format("MMM Do, hh:mm A");
                },
                onChange: function (data) {
                    var sub_labels = $.grep(labels, function (n, i) {
                        return i >= data.from && i <= data.to;
                    });
                    var sub_values = $.grep(values, function (n, i) {
                        return i >= data.from && i <= data.to;
                    });
                    chart_drew.data.labels = sub_labels;
                    chart_drew.data.datasets[0].data = sub_values;
                    chart_drew.update();
                }
            });
        });
    }

})
(jQuery);

function imgError(image) {
    image.onerror = "";
    image.src = "/static/images/no_picture.png";
    image.parentElement.href = "/static/images/no_picture.png";
    return true;
}
