(function ($) {
    "use strict";

    reset_filters_params();

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-bottom-left",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var base_href = '',
        campaigns_getting = false, campaigns_ajax, ajax_running,
        companies = '', companies_array = [],
        columns_opts = [
            {
                name: 'front_image',
                title: "Front Image",
                orderable: false,
                class: "as-img",
                data: 'front_image',
                render: function (data, type, row, meta) {
                    var div_image = '<div class="campaign-img"><a href="/static/images/no_picture.png" target="_blank" title="no image"><img class="img-responsive" src="/static/images/no_picture.png" alt="thumb"></a></div>';
                    if (data != null && data.length && data.toLowerCase() !== 'none') {
                        div_image = '<div class="campaign-img"><a href="' + data + '" target="_blank" data-spzoom title="' + row.title + '"><img class="img-responsive" alt="thumb" src="' + data + '" data-zoom-image="' + data + '" onError="imgError(this);"></a></div>';
                    }
                    return div_image;
                }
            },
            {
                name: 'back_image',
                title: "Back Image",
                orderable: false,
                class: "as-img",
                data: 'back_image',
                render: function (data, type, row, meta) {
                    var div_image = '<div class="campaign-img"><a href="/static/images/no_picture.png" target="_blank" title="no image"><img class="img-responsive" src="/static/images/no_picture.png" alt="thumb"></a></div>';
                    if (data != null && data.length && data.toLowerCase() !== 'none') {
                        div_image = '<div class="campaign-img"><a href="' + data + '" target="_blank" data-spzoom title="' + row.title + '"><img class="img-responsive" alt="thumb" src="' + data + '" data-zoom-image="' + data + '" onError="imgError(this);"></a></div>';
                    }
                    return div_image;
                }
            },
            {
                name: 'campaign',
                title: "Campaign",
                sort_name: "title",
                class: "as-text wrap-text",
                data: 'title',
                render: function (data, type, row, meta) {
                    var title = '<b><a href="/campaign/' + row.slug + '">' + data + '</a></b>';
                    var camp_link = '';
                    if (row.campaign_link != null && row.campaign_link.length && row.campaign_link.toLowerCase() !== 'none') {
                        camp_link = '<a href="' + row.campaign_link + '" rel="nofollow" target="_blank">' + row.campaign_link + '</a>';
                    }
                    var site_icon = '';
                    if (!jQuery.isEmptyObject(row.site)) {
                        if (row.site.icon == null || !row.site.icon.length || row.site.icon.toLowerCase() == 'none') {
                            site_icon = '<div class="link-site"><img class="img-responsive icon" width="20" src="/static/images/' + row.site.name + '.png" site-slug="' + row.site.slug + '" alt="' + row.site.name + '" title="' + row.site.name + '" data-toggle="tooltip" data-placement="top">' + camp_link + '<br/></div>';
                        }
                        else {
                            site_icon = '<div class="link-site"><img class="img-responsive icon" width="20" src="' + row.site.icon + '" alt="' + row.site.name + '" title="' + row.site.name + '" data-toggle="tooltip" data-placement="top">' + camp_link + '<br/></div>';
                        }
                    }
                    var description = '<div class="campaign-description clear">No description</div>';
                    if (row.description != null && row.description.length && row.description.toLowerCase() !== 'none') {
                        var desc_text = '<div>' + row.description + '</div>';
                        desc_text = $(desc_text).text();
                        if (type === 'display') {
                            desc_text = ellipsis(desc_text, 80);
                        }
                        description = '<div class="campaign-description clear">' + desc_text + '</div>';
                    }
                    var link_encode = encodeURIComponent('#');
                    if (row.link != null && row.link.length && row.link.toLowerCase() !== 'none') {
                        link_encode = encodeURIComponent(row.link);
                    }
                    var search_links = '<div class="social-search-links"><a href="https://www.google.com/search?q=google+exact+match&oq=google+exact+match&ie=UTF-8#q=' + link_encode + '" title="Google" target="_blank" data-toggle="tooltip" data-placement="top"><i class="fa fa-google"></i></a><a href="https://www.facebook.com/search/str/' + link_encode + '/stories-keyword/intersect" title="Facebook" target="_blank" data-toggle="tooltip" data-placement="top"><i class="fa fa-facebook"></i></a><a href="https://twitter.com/search?f=tweets&q=' + link_encode + '" title="Twitter" target="_blank" data-toggle="tooltip" data-placement="top"><i class="fa fa-twitter"></i></a><a href="https://www.pinterest.com/search/pins/?q=' + link_encode + '" title="Pinterest" target="_blank" data-toggle="tooltip" data-placement="top"><i class="fa fa-pinterest"></i></a></div>';
                    return title + site_icon + description + search_links;
                }
            },
            {
                name: 'status',
                title: "Status",
                sort_name: "status",
                class: "as-text",
                data: 'status',
                render: function (data, type, row, meta) {
                    var status_display = row.get_status_display;
                    if (data == 0) {
                        status_display = 'Active';
                    }
                    return status_display;
                }
            },
            {
                name: 'sales_now',
                title: "Sales Now",
                orderable: true,
                sort_name: "sales_now",
                class: "as-int",
                data: 'sales_now',
                render: function (data, type, row, meta) {
                    var sales_now_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        sales_now_display = intcomma(data);
                    }
                    return sales_now_display;
                }
            },
            {
                name: 'sales_total',
                title: "Sales Total",
                sort_name: "total_sales",
                class: "as-int",
                data: 'total_sales',
                render: function (data, type, row, meta) {
                    var total_sales_display = '-';
                    //if (typeof row.sales_total == 'int' && row.sales_total >= 0) {
                    //    total_sales_display = row.sales_total;
                    //}else
                    if (typeof data == 'number' && data >= 0) {
                        total_sales_display = intcomma(data);
                    }
                    return total_sales_display;
                }
            },
            {
                name: 'goal',
                title: "Goal",
                visible: false,
                sort_name: "goal",
                class: "as-int",
                data: 'goal',
                render: function (data, type, row, meta) {
                    var goal_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        goal_display = intcomma(data);
                    }
                    return goal_display;
                }
            },
            {
                name: 'ratio',
                title: "Ratio",
                visible: false,
                sort_name: "ratio",
                class: "as-int",
                data: 'ratio',
                render: function (data, type, row, meta) {
                    var ratio_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        ratio_display = data;
                    }
                    return ratio_display;
                }
            },
            {
                name: 'front_price',
                title: "Price",
                sort_name: "front_price",
                class: "as-int",
                data: 'front_price',
                render: function (data, type, row, meta) {
                    var currency_display = '$';
                    if (row.currency != null && row.currency.length && row.currency !== 'none') {
                        if (row.currency.toLowerCase() !== 'usd') {
                            currency_display = row.currency;
                        }
                    }
                    var front_price_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        front_price_display = intcomma(data.toFixed(2));
                    }
                    return currency_display + ' ' + front_price_display;
                }
            },
            {
                name: 'tipped',
                title: "Tipped",
                visible: false,
                sort_name: "tipped",
                class: "as-int",
                data: 'tipped',
                render: function (data, type, row, meta) {
                    var tipped_display = 'No';
                    if (data == true || (typeof data == 'string' && data.toLowerCase() == 'true')) {
                        tipped_display = 'Yes';
                    }
                    return tipped_display;
                }
            },
            {
                name: 'added_at',
                title: "Added",
                sort_name: "added_at",
                class: "as-text wrap-text",
                data: 'added_at',
                render: function (data, type, row, meta) {
                    var added_at_display = '-';
                    if (data != null && data.length && data.toLowerCase() !== 'none') {
                        added_at_display = '<abbr title="' + moment(data).toString() + '">' + moment(data).fromNow() + '</abbr>';
                    }
                    return added_at_display;
                }
            },
            {
                name: 'ending_at',
                title: "Ending",
                sort_name: "ending_at",
                class: "as-text wrap-text",
                data: 'ending_at',
                render: function (data, type, row, meta) {
                    var ending_at_display = '-';
                    if (data != null && data.length && data.toLowerCase() !== 'none') {
                        ending_at_display = '<abbr title="' + moment(data).toString() + '">' + moment(data).fromNow() + '</abbr>';
                    }
                    return ending_at_display;
                }
            },
            {
                name: 'fb_likes',
                title: "FB Likes",
                orderable: true,
                sort_name: "fb_likes",
                class: "as-int",
                data: 'metrics.fb_likes',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'fb_shares',
                title: "FB Shares",
                orderable: true,
                sort_name: "fb_shares",
                class: "as-int",
                data: 'metrics.fb_shares',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'fb_comments',
                title: "FB Comments",
                visible: false,
                orderable: true,
                sort_name: "fb_comments",
                class: "as-int",
                data: 'metrics.fb_comments',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'twitter_tweets',
                title: "Tweets",
                visible: false,
                orderable: true,
                sort_name: "twitter_tweets",
                class: "as-int",
                data: 'metrics.twitter_tweets',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'google_pluses',
                title: "Google +",
                visible: false,
                orderable: true,
                sort_name: "google_pluses",
                class: "as-int",
                data: 'metrics.google_pluses',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'pinterest_pins',
                title: "Pins",
                visible: false,
                orderable: true,
                sort_name: "pinterest_pins",
                class: "as-int",
                data: 'metrics.pinterest_pins',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'linkedin_shares',
                title: "LinkedIn Shares",
                visible: false,
                orderable: true,
                sort_name: "linkedin_shares",
                class: "as-int",
                data: 'metrics.linkedin_shares',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },
            {
                name: 'stumbleupon_stumbles',
                title: "Stumbles",
                visible: false,
                orderable: true,
                sort_name: "stumbleupon_stumbles",
                class: "as-int",
                data: 'metrics.stumbleupon_stumbles',
                render: function (data, type, row, meta) {
                    var value_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        value_display = intcomma(data);
                    }
                    return value_display;
                }
            },

            {
                name: 'front_style',
                title: "Style",
                visible: false,
                sort_name: "front_style",
                class: "as-text wrap-text",
                data: 'front_style',
                render: function (data, type, row, meta) {
                    var front_style_display = '-';
                    if (data != null && data.length && data.toLowerCase() != 'none') {
                        front_style_display = data;
                    }
                    return front_style_display;
                }
            },
            {
                name: 'front_color',
                title: "Color",
                visible: false,
                sort_name: "front_color",
                class: "as-text wrap-text",
                data: 'front_color',
                render: function (data, type, row, meta) {
                    var front_color_display = '-';
                    if (data != null && data && data.toLowerCase() !== 'none') {
                        front_color_display = data;
                        if (data.match("^{") || data.match("^#")) {
                            var colors = data.replace('}', '').replace('{', '').split(',');
                            var i, div_display = '<div class="color-palette-set">';
                            for (i = 0; i < colors.length; i++) {
                                var style = 'style="background-color:' + colors[i] + '; color:' + getContrastYIQ(colors[i]) + ';"';
                                div_display = div_display + '<div class="color-palette" ' + style + '>';
                                div_display = div_display + '<span>' + colors[i] + '</span>';
                                div_display += '</div>';
                            }
                            div_display += '</div>';
                            front_color_display = div_display;
                        }
                    }
                    return front_color_display;
                }
            },
            {
                name: 'times_ran',
                title: "Times Ran",
                visible: false,
                sort_name: "times_ran",
                class: "as-int",
                data: 'times_ran',
                render: function (data, type, row, meta) {
                    var times_ran_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        times_ran_display = data;
                    }
                    return times_ran_display;
                }
            },
            {
                name: 'times_seen',
                title: "Times Seen",
                visible: false,
                sort_name: "times_seen",
                class: "as-int",
                data: 'times_seen',
                render: function (data, type, row, meta) {
                    var times_seen_display = '-';
                    if (typeof data == 'number' && data >= 0) {
                        times_seen_display = data;
                    }
                    return times_seen_display;
                }
            },
            {
                name: 'site',
                title: "Site",
                visible: false,
                sort_name: "site_id",
                class: "as-text",
                data: 'site.name',
                render: function (data, type, row, meta) {
                    var site_display = '-';
                    if (!jQuery.isEmptyObject(row.site)) {
                        if (row.site.name != null && row.site.name.length && row.site.name.toLowerCase() !== 'none') {
                            site_display = data;
                        }
                    }
                    return site_display;
                }
            },
            {
                name: 'language',
                title: "Language",
                visible: false,
                sort_name: "language",
                class: "as-text wrap-text",
                data: 'language',
                render: function (data, type, row, meta) {
                    var language_display = data || '-';
                    if (row.get_language_display != null && row.get_language_display.length && row.get_language_display.toLowerCase() !== 'none') {
                        language_display = row.get_language_display;
                    }
                    return language_display;
                }
            },
            {
                name: 'updated_at',
                title: "Last Updated",
                visible: false,
                sort_name: "updated_at",
                class: "as-text wrap-text",
                data: 'updated_at',
                render: function (data, type, row, meta) {
                    var updated_at_display = '-';
                    if (data != null && data.length && data.toLowerCase() !== 'none') {
                        updated_at_display = '<abbr title="' + moment(data).toString() + '">' + moment(data).fromNow() + '</abbr>';
                    }
                    return updated_at_display;
                }
            }
        ],
        daterangepicker_opts = {
            locale: {
                format: 'YYYY/MM/DD',
                cancelLabel: 'Clear'
            },
            separator: '-'
        },
        $campaigns = $('#campaigns');
    var filter = {
            search: $('input[name="search"]').val(),
            min_price: $('input[name="min_price"]').val(),
            min_sales: $('input[name="min_sales"]').val(),
            time_added: $('input[name="time_added"]').val(),
            time_end: $('input[name="time_end"]').val(),
            company: $('input[name="company"]').val(),
            status: $('input[name="status"]').val()
        },
        order = {
            order_by: $('input[name="order_by"]').val() || 'added_at',
            asc_desc: $('input[name="asc_desc"]').val() || 'desc'
        },
        paginator = {
            page: $('input[name="page"]').val() || 1,
            items_per_page: $('input[name="items_per_page"]').val() || 25
        };

    localStorage.setItem('search_key', filter.search);

    localStorage.setItem('filter_min_sales', filter.min_sales);
    localStorage.setItem('filter_min_price', filter.min_price);
    localStorage.setItem('filter_time_end', filter.time_end);
    localStorage.setItem('filter_time_added', filter.time_added);
    localStorage.setItem('filter_company', filter.company);
    localStorage.setItem('filter_status', filter.status);

    localStorage.setItem('order_by', order.order_by);
    localStorage.setItem('asc_desc', order.asc_desc);

    localStorage.setItem('page', paginator.page);
    localStorage.setItem('items_per_page', paginator.items_per_page);

    var col_idx = 10;
    for (var i = 0; i < columns_opts.length; i++) {
        if (columns_opts[i].sort_name == order.order_by) {
            col_idx = i;
            break;
        }
    }
    var first_jsXHR = true;
    var table = $campaigns.DataTable({
        retrieve: true,
        dom: '<"campaigns-top"<"btn-toolbar pull-left"p<"page-lengths"<"#pagelen.btn-group">>><"btn-toolbar pull-right"<"#gshowcols.btn-group cols-vis">><"clear">>rt<"campaigns-bottom"<"btn-toolbar pull-left"l><"btn-toolbar pull-right"p><"clear">><"clear">',
        info: true,
        paging: true,
        lengthChange: true,
        lengthMenu: [ 10, 25, 50, 100 ],
        pageLength: paginator.items_per_page || 25,
        displayStart: (paginator.page - 1) * (paginator.items_per_page || 25),
        searching: false,
        order: [
            [col_idx, order.asc_desc]
        ],
        language: {
            decimal: '.',
            thousands: ',',
            lengthMenu: 'Display _MENU_ campaigns',
            paginate: {
                next: '>',
                previous: '<'
            },
            info: "Showing _START_ to _END_ of _MAX_ campaigns",
            infoFiltered: ''
        },
        columns: columns_opts,
        fixedHeader: true,
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: "/api/campaigns/",
            type: "POST",
            timeout: 60000 * 3,
            dataSrc: "data",
            data: function (d) {
                if (!first_jsXHR) {
                    local_storage_filters();
                }
                return $.extend({}, d, get_filters_params());
            }
        },
        deferRender: true
    });
    //table.settings()[0].jqXHR.abort();

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        //console.log('new ajax', table);
        // Pace
        if (typeof Pace != 'undefined') {
            Pace.restart();
        }
        else {
            console.log('Pace is not found!');
        }
        $("html, body").animate({ scrollTop: $('#campaigns_box').offset().top }, 1000);
    }).ajaxStop(function () {
        campaigns_getting = false;
        $('#campaigns_box i.loading').hide();
        $('[data-spzoom] > img').elevateZoom({
            responsive: true,
            tint: true,
            tintColour: '#F90',
            tintOpacity: 0.5,
            zoomWindowPosition: 2,
            zoomWindowHeight: 320,
            zoomWindowWidth: 500,
            easing: true,
            cursor: "crosshair"
        });
    }).ajaxSuccess(function () {
        var json = table.ajax.json();
        $("[paginator-display]").text(json.paginator.display);
        if (first_jsXHR) {
            //table.page(parseInt(paginator.page) - 1).draw(false);
            first_jsXHR = false;
        }
        local_storage_filters();
        $("html, body").animate({ scrollTop: $('#campaigns_box').offset().top }, 1000);
    });

    /*
     * filter-block
     */
    // init
    if (typeof $.fn.iCheck != 'undefined') {
        $('input[type="checkbox"], input[type="radio"]').iCheck('destroy').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%'
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifChecked', function () {
            $(this).prop('checked', true);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifUnchecked', function () {
            $(this).prop('checked', false);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifDisabled', function () {
            $(this).prop('disabled', true);
        });

        $('input[type="checkbox"], input[type="radio"]').on('ifEnabled', function () {
            $(this).prop('disabled', false);
        });

        $('#filter_company input[type="checkbox"]').on('ifChecked', function () {
            var this_value = $(this).val();
            companies_array = $.grep(companies_array, function (n, i) {
                return n !== this_value;
            });
            companies_array.push($(this).val());
            companies = companies_array.join(', ');
        });

        $('#filter_company input[type="checkbox"]').on('ifUnchecked', function () {
            var this_value = $(this).val();
            companies_array = $.grep(companies_array, function (n, i) {
                return n !== this_value;
            });
            companies = companies_array.join(', ');
        });
    }
    else {
        console.log('iCheck is not found!');
    }

    $('#filter_time_end').daterangepicker(daterangepicker_opts);
    $('#filter_time_added').daterangepicker(daterangepicker_opts);

    $("#btn_clear_filters").on('click', clear_filters);

    var val_filter_company;
    $('#company_selector').on({
        'show.bs.modal': function (event) {
            var button = $(event.relatedTarget);
            val_filter_company = companies_array;

            $('#company_selector button[type="reset"]').on('click', function () {
                $('#filter_company input[type="checkbox"]').iCheck('uncheck');
            });

            $("button[done]").unbind('click').on('click', function () {
                var webs = companies_array || [];
                $('#company_selector').modal('hide');
                companies_array = webs;
                update_filter_company_icheck(companies_array);
                companies = webs.join(', ');
                // console.log('done - companies', companies);
                if (webs.length && webs.length != $('#filter_company input[type="checkbox"]').length) {
                    $('#sites_modal_toggle').text(webs.join(', ')).attr('title', webs.join(', '));
                }
                else {
                    //Choose Websites
                    $('#filter_company input[type="checkbox"]').iCheck('check');
                    $('#sites_modal_toggle').text('All Sites').attr('title', 'All Sites');
                }
                //code get data table here
                localStorage.setItem('filter_company', companies);
                $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
                window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
                table.draw();
            });
        },
        'shown.bs.modal': function () {
            var _height = $('#company_selector .modal-body').height();
            var height = Math.min(($(window).height() - 180), _height);
            if (height <= 200) {
                height = 200;
            }
            $('#company_selector .modal-body').slimScroll({destroy: true})
                .slimscroll({
                    height: height + "px",
                    color: "rgba(0,0,0,0.2)",
                    size: "3px"
                });
        },
        'hide.bs.modal': function (event) {
            companies_array = val_filter_company;
            update_filter_company_icheck(companies_array);
            var webs = companies_array || [];
            companies = webs.join(', ');
            if (webs.length && webs.length != $('#filter_company input[type="checkbox"]').length) {
                $('#sites_modal_toggle').text(webs.join(', ')).attr('title', webs.join(', '));
            }
            else {
                //Choose Websites
                $('#filter_company input[type="checkbox"]').iCheck('check');
                $('#sites_modal_toggle').text('All Sites').attr('title', 'All Sites');
            }
        }
    });

    var change_only_active = false;
    $('#filter_status').change(function () {
        change_only_active = true;
        if ($(this).val() == 'active') {
            $('#filter_only_active').iCheck('check');
            $('#filter_status').prop('disabled', false);
        }
        else {
            $('#filter_only_active').iCheck('uncheck');
        }
        change_only_active = false;
        //code get data table here
        localStorage.setItem('filter_status', $(this).val());
        $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
        window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
        table.draw();
    });

    $('#filter_only_active').on({
        'ifChecked': function () {
            $('#filter_status').val('active');
            //code get data table here
            localStorage.setItem('filter_status', $('#filter_status').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            if (!change_only_active) {
                table.draw();
            }
        },
        'ifUnchecked': function () {
            $('#filter_status').val('');
            //code get data table here
            localStorage.setItem('filter_status', $('#filter_status').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            if (!change_only_active) {
                table.draw();
            }
        }
    });

    var change_has_sales = false;
    $('#filter_min_sales').change(function () {
        change_has_sales = true;
        if ($('#filter_min_sales').val() >= 0 && $('#filter_min_sales').val() !== '') {
            $('#filter_has_sales').iCheck('check');
        }
        else {
            $('#filter_has_sales').iCheck('uncheck');
        }
        change_has_sales = false;
        //code get data table here
        localStorage.setItem('filter_min_sales', $('#filter_min_sales').val());
        $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
        window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
        table.draw();
    });
    $('#filter_has_sales').on({
        'ifChecked': function () {
            if ($('#filter_min_sales').val() == '' || $('#filter_min_sales').val() < 0) {
                $('#filter_min_sales').val(0);
            }
            //code get data table here
            localStorage.setItem('filter_min_sales', $('#filter_min_sales').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            if (!change_has_sales) {
                table.draw();
            }
        },
        'ifUnchecked': function () {
            $('#filter_min_sales').val('');
            //code get data table here
            localStorage.setItem('filter_min_sales', $('#filter_min_sales').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            if (!change_has_sales) {
                table.draw();
            }
        }
    });

    $('#filter_min_price').change(function () {
        //code get data table here
        localStorage.setItem('filter_min_price', $('#filter_min_price').val());
        $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
        window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
        table.draw();
    });

    $('#filter_time_end, #filter_time_added').on({
        'apply.daterangepicker': function (ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
            //code get data table here
            localStorage.setItem('filter_time_end', $('#filter_time_end').val());
            localStorage.setItem('filter_time_added', $('#filter_time_added').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            table.draw();
        },
        'cancel.daterangepicker': function (ev, picker) {
            $(this).val('');
            //code get data table here
            localStorage.setItem('filter_time_end', $('#filter_time_end').val());
            localStorage.setItem('filter_time_added', $('#filter_time_added').val());
            $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
            window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
            table.draw();
        }
    });

    //default filter
    $('#navbar-search-input, #error-search-input').val(localStorage.getItem('search_key'));
    $('#filter_min_price').val(localStorage.getItem('filter_min_price'));
    $('#filter_min_sales').val(localStorage.getItem('filter_min_sales'));
    if (typeof $('#filter_min_sales').val() == 'number' || parseInt($('#filter_min_sales').val()) >= 0) {
        change_has_sales = true;
        $('#filter_has_sales').iCheck('check');
        change_has_sales = false;
    }
    $('#filter_status').val(localStorage.getItem('filter_status'));
    if ($('#filter_status').val() == 'active' || $('#filter_status').val() == 'undefined') {
        change_only_active = true;
        $('#filter_only_active').iCheck('check');
        change_only_active = false;
    }
    $('#filter_time_added').val(localStorage.getItem('filter_time_added'));
    $('#filter_time_end').val(localStorage.getItem('filter_time_end'));
    companies = localStorage.getItem('filter_company');
    if (companies == '' || companies == undefined) {
        //Choose Websites
        companies_array = [];
        $('#filter_company input[type="checkbox"]').iCheck('check');
        $('#sites_modal_toggle').text('All Sites').attr('title', 'All Sites');
    }
    else {
        companies_array = companies.split(', ');
        if (companies_array.length && companies_array.length != $('#filter_company input[type="checkbox"]').length) {
            $('#sites_modal_toggle').text(companies).attr('title', companies);
            update_filter_company_icheck(companies_array);
        }
        else {
            $('#filter_company input[type="checkbox"]').iCheck('check');
            $('#sites_modal_toggle').text('All Sites').attr('title', 'All Sites');
        }
    }

    $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));

    /*
     * end filter-block
     */

    $('#pagelen').append('<a type="button" class="btn btn-default" rel="pag_item" data-page-items="10" href="#">10</a><a type="button" class="btn btn-default" rel="pag_item" data-page-items="25" href="#">25</a><a type="button" class="btn btn-default" rel="pag_item" data-page-items="50" href="#">50</a><a type="button" class="btn btn-default" rel="pag_item" data-page-items="100" href="#">100</a>');

    $('a[rel="pag_item"]').each(function () {
        if (parseInt($(this)[0].dataset.pageItems) == paginator.items_per_page) {
            $(this).addClass('active');
        }
    });
    $('a[rel="pag_item"]').unbind('click').on('click', function (event) {
        event.preventDefault();
        var pageItems = parseInt($(this)[0].dataset.pageItems);
        table.page.len(pageItems).draw();
        $('a[rel="pag_item"]').removeClass('active');
        $(this).addClass('active');
    });

    $('#gshowcols').append('<div class="btn-group" id="toggle_columns"><button type="button" class="btn btn-default collapsed" data-toggle="collapse" data-target="#list_columns" aria-expanded="false"><span class="glyphicon glyphicon-list fa-fw"></span>Columns<span class="caret"></span></button></div><button type="button" class="btn btn-default" id="btn_refresh"><span class="fa fa-refresh fa-fw"></span>Refresh</button>');
    var tools_cols = $('<ul />', {'id': 'list_columns', 'class': 'dropdown-menu dropdown-menu-right columns'});
    $.each(columns_opts, function (i, val) {
        var val_of_name = localStorage.getItem(val.name);
        if (val_of_name == null) {
            val_of_name = (val.visible == undefined || val.visible == true);
            localStorage.setItem(val.name, val_of_name);
        }
        tools_cols.append(
                '<li class="col-md-4"><a role="menuitem" tabindex="-1" href="#">' +
                '<input type="checkbox" ' +
                'id="' + val.name + '" ' +
                'class="toggle-vis minimal" ' +
                'data-column="' + i + '" ' +
                'data-column-name="' + val.name + '" ' +
                ((val_of_name == 'true' || val_of_name == true) ? 'checked ' : '') +
                'value="' + val.name + '">' +
                '<label for="' + val.name + '">' +
                val.title +
                '</label></a>' +
                '</li>'
        );
    });
    $('#toggle_columns').append(tools_cols);
    $(window).click(function (e) {
        var subject = $("#toggle_columns");
        if (e.target.id != subject.attr('id') && !subject.has(e.target).length) {
            $('#list_columns').collapse("hide");
        }
    });


    // iCheck
    if (typeof $.fn.iCheck != 'undefined') {
        $('#toggle_columns input[type="checkbox"], #toggle_columns input[type="radio"]').iCheck('destroy')
            .iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue',
                increaseArea: '20%'
            });

        $('#toggle_columns input[type="checkbox"], #toggle_columns input[type="radio"]')
            .on('ifChecked', function () {
                $(this).prop('checked', true);
            });

        $('#toggle_columns input[type="checkbox"], #toggle_columns input[type="radio"]')
            .on('ifUnchecked', function () {
                $(this).prop('checked', false);
            });

        $('#toggle_columns input[type="checkbox"], #toggle_columns input[type="radio"]')
            .on('ifDisabled', function () {
                $(this).prop('disabled', true);
            });

        $('#toggle_columns input[type="checkbox"], #toggle_columns input[type="radio"]')
            .on('ifEnabled', function () {
                $(this).prop('disabled', false);
            });

        $('#toggle_columns input[type="checkbox"]').on('ifChecked', function () {
            checkboxToggleVis(table, this);
        });

        $('#toggle_columns input[type="checkbox"]').on('ifUnchecked', function () {
            checkboxToggleVis(table, this);
        });
    }
    else {
        $('input[type="checkbox"].toggle-vis').on('change', function (e) {
            checkboxToggleVis(table, this);
        });
    }

    $.each(columns_opts, function (i, val) {
        var val_of_name = localStorage.getItem(val.name);
        table.column(val.name + ':name').visible(val_of_name == 'true', false);
        table.columns.adjust();
        table.fixedHeader.adjust();
    });

    $(window).scroll(function (event) {
        table.fixedHeader.adjust();
    });

    $(".box-table-responsive").scroll(function (event) {
        //console.log(event, $("#campaigns").offset());
        table.fixedHeader.adjust();
        $(".campaigns-top, .campaigns-bottom").css({'left': -($("#campaigns").position().left - 10)});
    });

    $('#btn_refresh').click(function () {
        table.ajax.reload(null, false);
    });

    /*
     * functions
     */
    function checkboxToggleVis(table, element) {
        // Get the column API object
        // var column = table.column($(this).attr('data-column'));
        var column = table.column($(element).attr('data-column-name') + ':name');

        localStorage.setItem($(element).attr('data-column-name'), !column.visible());
        // Toggle the visibility
        column.visible(!column.visible(), false);
        table.columns.adjust();
        table.fixedHeader.adjust();

        $('#campaigns').css("width", "");
    }

    function update_filter_company_icheck(companies_array) {
        $('#filter_company input[type="checkbox"]').each(function (i, e) {
            var this_value = $(e).val();
            if ($.inArray(this_value, companies_array) >= 0) {
                $(e).iCheck('check');
            }
            else {
                $(e).iCheck('uncheck');
            }
        });
    }

    function clear_filters() {
        $('#navbar-search-input, #error-search-input').val('');

        $('#filter_only_active').iCheck('uncheck').iCheck('enable');
        $('#filter_has_sales').iCheck('uncheck').iCheck('enable');
        $('#filter_min_sales').val('');
        $('#filter_min_price').val('');
        $('#filter_time_end').val('');
        $('#filter_time_added').val('');
        $('#filter_status').prop('disabled', false);
        $('#filter_status').val('');
        companies_array = [];
        $('#filter_company input[type="checkbox"]').iCheck('uncheck');
        //Choose Websites
        $('#filter_company input[type="checkbox"]').iCheck('check');
        $('#sites_modal_toggle').text('All Sites').attr('title', 'All Sites');
        companies = '';
        //code get data table here
        local_storage_filters();
        $('#btn_filters_page').attr('href', '/campaigns/' + '?' + $.param(get_filters_params()));
        window.history.replaceState({}, '', '/campaigns/' + '?' + $.param(get_filters_params()));
        table.draw();
    }

    function local_storage_filters() {
        localStorage.setItem('search_key', $('#navbar-search-input').val());

        localStorage.setItem('filter_min_sales', $('#filter_min_sales').val());
        localStorage.setItem('filter_min_price', $('#filter_min_price').val());
        localStorage.setItem('filter_time_end', $('#filter_time_end').val());
        localStorage.setItem('filter_time_added', $('#filter_time_added').val());
        var multipleValues = companies_array || [];
        localStorage.setItem('filter_company', multipleValues.join(", "));
        localStorage.setItem('filter_status', $('#filter_status').val());

        localStorage.setItem('order_by', columns_opts[table.order()[0][0]].sort_name);
        localStorage.setItem('asc_desc', table.order()[0][1]);

        localStorage.setItem('page', parseInt(table.page.info().page) + 1);
        localStorage.setItem('items_per_page', parseInt(table.page.info().length));
    }

    function get_filters_params() {
        var filters_params = {};
        filters_params.csrfmiddlewaretoken = $('input[name="csrfmiddlewaretoken"]').val();
        if (localStorage.getItem('search_key'))
            filters_params.search = localStorage.getItem('search_key');

        if (localStorage.getItem('filter_min_sales'))
            filters_params.min_sales = localStorage.getItem('filter_min_sales');
        if (localStorage.getItem('filter_min_price'))
            filters_params.min_price = localStorage.getItem('filter_min_price');
        if (localStorage.getItem('filter_time_end'))
            filters_params.time_end = localStorage.getItem('filter_time_end');
        if (localStorage.getItem('filter_time_added'))
            filters_params.time_added = localStorage.getItem('filter_time_added');
        if (localStorage.getItem('filter_company'))
            filters_params.company = localStorage.getItem('filter_company');
        if (localStorage.getItem('filter_status'))
            filters_params.status = localStorage.getItem('filter_status');

        if (localStorage.getItem('order_by'))
            filters_params.order_by = localStorage.getItem('order_by');
        if (localStorage.getItem('asc_desc'))
            filters_params.asc_desc = localStorage.getItem('asc_desc');

        if (localStorage.getItem('page'))
            filters_params.page = localStorage.getItem('page');
        if (localStorage.getItem('items_per_page'))
            filters_params.items_per_page = localStorage.getItem('items_per_page');

        return filters_params;
    }

    function reset_filters_params() {
        localStorage.removeItem('search_key');
        localStorage.removeItem('filter_min_sales');
        localStorage.removeItem('filter_min_price');
        localStorage.removeItem('filter_time_end');
        localStorage.removeItem('filter_time_added');
        localStorage.removeItem('filter_company');
        localStorage.removeItem('filter_status');

        localStorage.removeItem('order_by');
        localStorage.removeItem('asc_desc');

        localStorage.removeItem('page');
        localStorage.removeItem('items_per_page');
    }


    function init_filter_block() {
        ajax_running = $.ajax({
            url: '/api/sites/',
            data: {},
            method: 'GET',
            crossDomain: false,
            success: function (result) {
                var sites = result.sites,
                    i, input, label, div_site;
                $('#filter_company').html('');
                for (i = 0; i < sites.length; i++) {
                    var site = sites[i];
                    div_site = $('<div />', {'class': 'form-control trans-border'});
                    input = $('<input />', {class: 'minimal', type: 'checkbox', id: site.name.toLowerCase(), value: site.slug});
                    label = $('<label />', {class: 'company-check', for: site.name.toLowerCase(), value: site.slug});
                    div_site.append(input).append(label.text(site.name));
                    $('#filter_company').append(div_site);
                    re_icheck();
                }
            },
            error: function () {
                //init_filter_block();
            }
        });
    }

    /*
     *  Common functions
     */
    function refresh_load() {
        location.reload();
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    function invertColor(hexTripletColor) {
        var color = hexTripletColor;
        color = color.substring(1); // remove #
        color = parseInt(color, 16); // convert to integer
        color = 0xFFFFFF ^ color; // invert three bytes
        color = color.toString(16); // convert to hex
        color = ("000000" + color).slice(-6); // pad with leading zeros
        color = "#" + color; // prepend #
        return color;
    }

    function getContrast50(hexcolor) {
        hexcolor = hexcolor.substring(1);
        return (parseInt(hexcolor, 16) > 0xffffff / 2) ? 'black' : 'white';
    }

    function getContrastYIQ(hexcolor) {
        hexcolor = hexcolor.substring(1);
        var r = parseInt(hexcolor.substr(0, 2), 16);
        var g = parseInt(hexcolor.substr(2, 2), 16);
        var b = parseInt(hexcolor.substr(4, 2), 16);
        var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
        return (yiq >= 128) ? 'black' : 'white';
    }

    // Truncate a string
    function ellipsis(str, max, add) {
        add = add || '...';
        return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
    }

    function intcomma(str) {
        if (typeof str == 'string' || typeof str == 'number') {
            return String(str).replace(/(\d)(?=(\d\d\d)+(?!\d)\.?)/g, "$1,");
        }
        return str;
    }
})(jQuery);

function imgError(image) {
    image.onerror = "";
    image.src = "/static/images/no_picture.png";
    image.dataset.zoomImage = "/static/images/no_picture.png";
    return true;
}
