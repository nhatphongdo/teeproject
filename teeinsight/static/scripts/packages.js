//from https://google-developers.appspot.com/maps/pricing-and-plans/javascript/licensing-selector/licensing.js
(function ($) {
    "use strict";

    var cardContainers = document.querySelectorAll('.card-container');
    var activeCard;

    $(document).ready(function () {
        $('.card-container').each(function (i, cardContainer) {
            cardContainer.addEventListener('click', function (event) {
                var target = event.target;
                findCard(target, 'card');
            });
        });
    });

    function removeActiveState() {
        if (activeCard) {
            activeCard.classList.remove('active');
            activeCard = null;
        }
    }

    function findCard(target, attributeToFind) {
        var targetAttribute = target.getAttribute('data-type');
        var targetParent = target.parentNode;

        if (targetAttribute == 'none') {
            return false;
        }
        if (targetAttribute == 'card-container') {
            removeActiveState();
        }
        else if (targetAttribute != attributeToFind) {
            findCard(targetParent, attributeToFind);
        }
        else {
            var hasActiveClass = target.classList.contains('active');
            removeActiveState();
            if (!hasActiveClass) {
                target.classList.add('active');
                activeCard = target;
            }
        }
    }

})(jQuery);
