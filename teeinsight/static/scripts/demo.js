/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */
(function ($, AdminLTE) {

    "use strict";

    /**
     * List of all the available skins
     *
     * @type Array
     */
    var my_skins = [
        "skin-blue",
        "skin-black",
        "skin-red",
        "skin-yellow",
        "skin-purple",
        "skin-green",
        "skin-blue-light",
        "skin-black-light",
        "skin-red-light",
        "skin-yellow-light",
        "skin-purple-light",
        "skin-green-light"
    ];

    setup();

    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    if (typeof CKEDITOR != 'undefined') {
        CKEDITOR.replace('editor1');
    }
    else {
        console.log('CKEDITOR is not found!');
    }

    // bootstrap WYSIHTML5 - text editor
    if (typeof wysihtml5 != 'undefined') {
        $(".textarea").wysihtml5();
        $("#compose-textarea").wysihtml5();
    }
    else {
        console.log('wysihtml5 is not found!');
    }

    // DataTable
    if (typeof $.fn.DataTable != 'undefined') {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        $('#campaigns').DataTable();
    }
    else {
        console.log('Datatable is not found!');
    }

    // Bootstrap slider
    if (typeof $.fn.slider != 'undefined') {
        $('.slider').slider();
    }
    else {
        console.log('Bootstrap slider is not found!');
    }

    // ion Range Slider
    if (typeof $.fn.ionRangeSlider != 'undefined') {
        /* ION SLIDER */
        $("#slider_range_1").ionRangeSlider();
        $("#slider_range_2").ionRangeSlider({
            min: 0,
            max: 5000,
            from: 1000,
            to: 4000,
            type: 'double',
            step: 1,
            prefix: "$",
            prettify: false,
            hasGrid: true
        });
        $("#slider_range_3").ionRangeSlider({
            min: 0,
            max: 10,
            type: 'single',
            step: 0.1,
            postfix: " mm",
            prettify: false,
            hasGrid: true
        });
        $("#slider_range_4").ionRangeSlider({
            min: -50,
            max: 50,
            from: 0,
            type: 'single',
            step: 1,
            postfix: "°",
            prettify: false,
            hasGrid: true
        });
        $("#slider_range_5").ionRangeSlider({
            type: "single",
            step: 100,
            postfix: " light years",
            from: 55000,
            hideMinMax: true,
            hideFromTo: false
        });
        $("#slider_range_6").ionRangeSlider({
            type: "double",
            postfix: " miles",
            step: 10000,
            from: 25000000,
            to: 35000000,
            onChange: function (obj) {
                var t = "";
                for (var prop in obj) {
                    t += prop + ": " + obj[prop] + "\r\n";
                }
                $("#result").html(t);
            },
            onLoad: function (obj) {
                //
            }
        });
    }
    else {
        console.log('IonRangeSlider is not found!');
    }

    // iCheck
    if (typeof $.fn.iCheck != 'undefined') {
        $('input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat-blue',
            increaseArea: '20%'
        });
    }
    else {
        console.log('iCheck is not found!');
    }

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        // Pace
        if (typeof Pace != 'undefined') {
            Pace.restart();
        }
        else {
            console.log('Pace is not found!');
        }
    });
    $('#pace-ajax').click(function () {
        $.ajax({url: '#', success: function (result) {
            $('#ajax-content').append('<hr>Ajax Request Completed !');
        }});
    });

    /**
     * Toggles layout classes
     *
     * @param cls (String) the layout class to toggle
     * @returns void
     */
    function change_layout(cls) {
        $("body").toggleClass(cls);
        AdminLTE.layout.fixSidebar();
        //Fix the problem with right sidebar and layout boxed
        var $control_sidebar_bg = $(".control-sidebar-bg");
        if (cls == "layout-boxed")
            AdminLTE.controlSidebar._fix($control_sidebar_bg);
        if ($('body').hasClass('fixed') && cls == 'fixed') {
            AdminLTE.pushMenu.expandOnHover();
            AdminLTE.layout.activate();
        }
        AdminLTE.controlSidebar._fix($control_sidebar_bg);
        AdminLTE.controlSidebar._fix($(".control-sidebar"));
    }

    /**
     * Replaces the old skin with the new skin
     * @param cls (String) the new skin class
     * @returns Boolean false to prevent link's default action
     */
    function change_skin(cls) {
        $.each(my_skins, function (i) {
            $("body").removeClass(my_skins[i]);
        });

        $("body").addClass(cls);
        store('skin', cls);
        return false;
    }

    /**
     * Store a new settings in the browser
     *
     * @param name (String) Name of the setting
     * @param val (String) Value of the setting
     * @returns void
     */
    function store(name, val) {
        if (typeof (Storage) !== "undefined") {
            localStorage.setItem(name, val);
        } else {
            window.alert('Please use a modern browser to properly view this template!');
        }
    }

    /**
     * Get a prestored setting
     *
     * @param name (String) Name of of the setting
     * @returns String The value of the setting | null
     */
    function get(name) {
        if (typeof (Storage) !== "undefined") {
            return localStorage.getItem(name);
        } else {
            window.alert('Please use a modern browser to properly view this template!');
        }
    }

    /**
     * Retrieve default settings and apply them to the template
     *
     * @returns void
     */
    function setup() {
        var tmp = get('skin');
        if (tmp && $.inArray(tmp, my_skins))
            change_skin(tmp);

        //Add the change skin listener
        $("#layout-skins-list [data-skin], .skins-list [data-skin]").on('click', function (e) {
            e.preventDefault();
            change_skin($(this).data('skin'));
        });

        //Add the layout manager
        $("[data-layout]").on('click', function () {
            change_layout($(this).data('layout'));
        });

        $("[data-controlsidebar]").on('click', function () {
            change_layout($(this).data('controlsidebar'));
            var slide = !AdminLTE.options.controlSidebarOptions.slide;
            AdminLTE.options.controlSidebarOptions.slide = slide;
            if (!slide)
                $('.control-sidebar').removeClass('control-sidebar-open');
        });

        $("[data-sidebarskin='toggle']").on('click', function () {
            var sidebar = $(".control-sidebar");
            if (sidebar.hasClass("control-sidebar-dark")) {
                sidebar.removeClass("control-sidebar-dark");
                sidebar.addClass("control-sidebar-light");
            } else {
                sidebar.removeClass("control-sidebar-light");
                sidebar.addClass("control-sidebar-dark");
            }
        });

        $("[data-enable='expandOnHover']").on('click', function () {
            $(this).attr('disabled', true);
            AdminLTE.pushMenu.expandOnHover();
            if (!$('body').hasClass('sidebar-collapse'))
                $("[data-layout='sidebar-collapse']").click();
        });

        // Reset options
        var $body = $('body');
        if ($body.hasClass('fixed')) {
            $("[data-layout='fixed']").attr('checked', 'checked');
        }
        if ($body.hasClass('layout-boxed')) {
            $("[data-layout='layout-boxed']").attr('checked', 'checked');
        }
        if ($body.hasClass('sidebar-collapse')) {
            $("[data-layout='sidebar-collapse']").attr('checked', 'checked');
        }

    }
})(jQuery, $.AdminLTE);
