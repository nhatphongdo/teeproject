(function ($) {
    "use strict";

    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () {
        // Pace
        if (typeof Pace != 'undefined') {
            Pace.restart();
        }
        else {
            console.log('Pace is not found!');
        }
    });

    var base_href = '',
        _recently_url = base_href + '/part/dashboard-recently/',
        _hot_url = base_href + '/part/dashboard-hot/',
        _highest_url = base_href + '/part/dashboard-highest/',
        $hot_metric_select = $('#hot_metric_select'),
        $hot_period_select = $('#hot_period_select');

    $('#ra_reload_link').click(function () {
        recently_load();
    });

    $('#ra_active_only_checkbox').change(function () {
        if ($(this).is(":checked")) {
            _recently_url = base_href + '/part/dashboard-recently/active/';
            recently_load();
        }
        else {
            _recently_url = base_href + '/part/dashboard-recently/';
            recently_load();
        }
    });

    $('#hot_reload_link').click(function () {
        hot_load();
    });
    $hot_metric_select.change(function () {
        _hot_url = base_href + '/part/dashboard-hot/' + this.value + '/' + $hot_period_select.val() + '/';
        hot_load();
    });
    $hot_period_select.change(function () {
        _hot_url = base_href + '/part/dashboard-hot/' + $hot_metric_select.val() + '/' + this.value + '/';
        hot_load();
    });

    $('#hs_reload_link').click(function () {
        highest_load();
    });

    $('#hs_today_checkbox').change(function () {
        if ($(this).is(":checked")) {
            _highest_url = base_href + '/part/dashboard-highest/1/';
            highest_load();
        }
        else {
            _highest_url = base_href + '/part/dashboard-highest/';
            highest_load();
        }
    });

    $(document).ready(function () {
        setInterval(interval_load(), 60000 * 5);
        setInterval(recently_load(), 60000 * 5);
        //setInterval(hot_load(), 60000 * 5);
        setInterval(highest_load(), 60000 * 5);
    });

    function recently_load() {
        $.ajax({
            url: _recently_url,
            success: function (result) {
                $('#recently_wrapper').html(result);
            }
        });
    }

    function hot_load() {
        $.ajax({
            url: _hot_url,
            success: function (result) {
                $('#hot_wrapper').html(result);
            }
        });
    }

    function highest_load() {
        $.ajax({
            url: _highest_url,
            success: function (result) {
                $('#hs_wrapper').html(result);
            }
        });
    }

    function interval_load() {
        $('#new_campaigns_today').text(localStorage.getItem('count-today') || 0);
        //$('#new_campaigns_today').load(base_href + '/part/count-today/',
        //    function () {
        //        localStorage.setItem('count-today', $(this).text());
        //    });
        $.ajax({
            url: base_href + '/part/count-today/',
            timeout: 60000,
            beforeSend: function () {
                $('#new_campaigns_today ~ .inner-title').addClass('loading');
            },
            error: function () {
                $('#new_campaigns_today ~ .inner-title').removeClass('loading');
            },
            success: function (data) {
                $('#new_campaigns_today').text(data);
                localStorage.setItem('count-today', $(this).text());
                $('#new_campaigns_today ~ .inner-title').removeClass('loading');
            }
        });

        $('#total_active_campaigns').text(localStorage.getItem('count-active') || 0);
        //$('#total_active_campaigns').load(base_href + '/part/count-active/',
        //    function () {
        //        localStorage.setItem('count-active', $(this).text());
        //    });
        $.ajax({
            url: base_href + '/part/count-active/',
            timeout: 60000 * 2,
            beforeSend: function () {
                $('#total_active_campaigns ~ .inner-title').addClass('loading');
            },
            error: function () {
                $('#total_active_campaigns ~ .inner-title').removeClass('loading');
            },
            success: function (data) {
                $('#total_active_campaigns').text(data);
                localStorage.setItem('count-active', $(this).text());
                $('#total_active_campaigns ~ .inner-title').removeClass('loading');
            }
        });

        $('#total_campaigns').text(localStorage.getItem('count-campaigns') || 0);
        //$('#total_campaigns').load(base_href + '/part/count-campaigns/',
        //    function () {
        //        localStorage.setItem('count-campaigns', $(this).text());
        //    });
        $.ajax({
            url: base_href + '/part/count-campaigns/',
            timeout: 60000,
            beforeSend: function () {
                $('#total_campaigns ~ .inner-title').addClass('loading');
            },
            error: function () {
                $('#total_campaigns ~ .inner-title').removeClass('loading');
            },
            success: function (data) {
                $('#total_campaigns').text(data);
                localStorage.setItem('count-campaigns', $(this).text());
                $('#total_campaigns ~ .inner-title').removeClass('loading');
            }
        });

        $('#top_selling').text(localStorage.getItem('total-sales') || 0);
        //$('#top_selling').load(base_href + '/part/total-sales/',
        //    function () {
        //        localStorage.setItem('total-sales', $(this).text());
        //    });
        $.ajax({
            url: base_href + '/part/total-sales/',
            timeout: 60000 * 2,
            beforeSend: function () {
                $('#top_selling ~ .inner-title').addClass('loading');
            },
            error: function () {
                $('#top_selling ~ .inner-title').removeClass('loading');
            },
            success: function (data) {
                $('#top_selling').text(data);
                localStorage.setItem('total-sales', $(this).text());
                $('#top_selling ~ .inner-title').removeClass('loading');
            }
        });
    }

    function imgError(image) {
        image.onerror = "";
        image.src = "/static/images/no_picture.png";
        return true;
    }
})(jQuery);
