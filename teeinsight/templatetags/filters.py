from collections import OrderedDict
import json
import re
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models.query import QuerySet
# from django.utils import simplejson
from django.utils.safestring import mark_safe
from django.template import Library

register = Library()


@register.filter
def get_range(value):
    """
    Filter - returns a list containing range made from given value
    Usage (in template):

    <ul>{% for i in 3|get_range %}
        <li>{{ i }}. Do something</li>
    {% endfor %}</ul>

    Results with the HTML:
    <ul>
        <li>0. Do something</li>
        <li>1. Do something</li>
        <li>2. Do something</li>
    </ul>

    Instead of 3 one may use the variable set in the views
    """
    return range(value)


@register.filter(is_safe=False)
def none_if(value, eq=-1):
    if value == eq:
        return None
    return value


@register.filter(is_safe=False)
def zero_true(value):
    if value is 0:
        return True
    return value


@register.filter(is_safe=False)
def add_prefix(value, pre='$'):
    return pre + str(value)


@register.filter(is_safe=False)
def colors_display(value='{}'):
    html = '<div class="color-palette-set">'
    if value:
        value = value.replace('}', '').replace('{', '').split(',')
        for color in value:
            style = 'style="background-color:' + color + '; color:' + get_contrast_yiq(color) + ';"'
            html = html + '<div class="color-palette" ' + style + '>'
            html = html + '<span>' + color + '</span>'
            html += '</div>'
    html += '</div>'
    return html


def get_contrast_yiq(hex_color):
    m = re.search('(?<=#)[0-9a-fA-F]+', hex_color)
    if m is not None:
        hex_color = m.group(0)
        # hex_color = hex_color[1:]
        r = int(hex_color[:2], 16)
        g = int(hex_color[2:4], 16)
        b = int(hex_color[4:6], 16)
        yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000
        return 'black' if yiq >= 128 else 'white'
    return 'yellow'


def get_contrast_50(hex_color):
    m = re.search('(?<=#)[0-9a-fA-F]+', hex_color)
    if m is not None:
        hex_color = m.group(0)
        # hex_color = hex_color[1:]
        return 'black' if int(hex_color, 16) > 0xffffff / 2 else 'white'
    return 'yellow'


@register.filter
def camp_udf(sts):
    if sts.lower() == 'undefined':
        return 'active'
    return sts

from urllib.parse import urlparse


@register.filter
def url_validator(url):
    o = urlparse(url)
    # print(o, o.geturl())
    if o.scheme:
        return url
    elif o.netloc:
        return url
    else:
        o = urlparse('//' + url)
        # print(o, o.geturl())
        if o.scheme or o.netloc:
            return o.geturl()


@register.filter
def jsonify(obj, indent='    '):
    if isinstance(obj, QuerySet):
        return mark_safe(serialize('json', obj))
    # return mark_safe(simplejson.dumps(obj, cls=DjangoJSONEncoder))
    return mark_safe(json.dumps(obj, indent=indent, sort_keys=False))


@register.filter
def jsonify_s(obj, indent='    '):
    if isinstance(obj, QuerySet):
        return mark_safe(serialize('json', obj))
    # return mark_safe(simplejson.dumps(obj, cls=DjangoJSONEncoder))
    return mark_safe(json.dumps(obj, indent=indent, sort_keys=True))
