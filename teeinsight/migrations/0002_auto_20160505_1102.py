# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('teeinsight', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Engagement',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('engagement', models.IntegerField(default=0)),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'ordering': ['-created_at'],
            },
        ),
        migrations.RemoveField(
            model_name='campaign',
            name='engagement',
        ),
        migrations.AlterField(
            model_name='campaign',
            name='ending_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 6, 4, 4, 1, 59, 847584, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='engagement',
            name='campaign',
            field=models.ForeignKey(related_name='engagement_metrics', to='teeinsight.Campaign'),
        ),
    ]
