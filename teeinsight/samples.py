import os
from django.conf import settings
from datetime import datetime, timedelta
# from flask import Flask, session, render_template, url_for, redirect, request, jsonify
import paypalrestsdk
from paypalrestsdk import BillingPlan, BillingAgreement, Order, WebProfile, Payment, Payout, PayoutItem, Sale, \
    ResourceNotFound, Webhook, WebhookEvent, WebhookEventType, Invoice, CreditCard, Capture, Authorization
from paypalrestsdk.openid_connect import Tokeninfo, Userinfo
import random
import string
import unittest
import json
# from mock import patch, Mock

# app = Flask(__name__)

# # Merchant and User login for the app
# app.config.update(dict(
#     DEBUG=True,
#     SECRET_KEY='secret',
#     MERCHANT_USERNAME='merchant',
#     MERCHANT_PASSWORD='123456',
#     CUSTOMER_USERNAME='customer',
#     CUSTOMER_PASSWORD='123456'
# ))

# Initialize PayPal sdk
paypalrestsdk.configure({
    'mode': 'sandbox',  # sandbox or live
    'client_id': settings.PAYPAL_CLIENT_ID,
    'client_secret': settings.PAYPAL_CLIENT_SECRET,
    # 'openid_redirect_uri': 'http://teeinsight.com/packages/'
})

api = paypalrestsdk.Api({
    'mode': 'sandbox',  # sandbox or live
    'client_id': settings.PAYPAL_CLIENT_ID,
    'client_secret': settings.PAYPAL_CLIENT_SECRET,
    # 'openid_redirect_uri': 'http://127.0.3.1/samples/'
})

# payment = paypalrestsdk.Payment({...}, api=api)


# #############################
# paypal_client

# map from user email to refresh_token
CUSTOMER_TOKEN_MAP = "customer_token_map.txt"

# store ids of verified payments
# use a database instead of files in production
VERIFIED_PAYMENTS = "paypal_verified_payments.txt"


def verify_payment(payment_client):
    """Verify credit card of paypal payment made using rest apis
    https://developer.paypal.com/docs/integration/mobile/verify-mobile-payment/
    """
    payment_id = payment_client['response']['id']

    try:
        payment_server = paypalrestsdk.Payment.find(payment_id)

        if payment_server.state != 'approved':
            return False, 'Payment has not been approved yet. Status is ' + payment_server.state + '.'

        amount_client = payment_client['payment']['amount']
        currency_client = payment_client['payment']['currency_code']

        # Get most recent transaction
        transaction = payment_server.transactions[0]
        amount_server = transaction.amount.total
        currency_server = transaction.amount.currency
        sale_state = transaction.related_resources[0].sale.state

        if (amount_server != amount_client):
            return False, 'Payment amount does not match order.'
        elif (currency_client != currency_server):
            return False, 'Payment currency does not match order.'
        elif sale_state != 'completed':
            return False, 'Sale not completed.'
        elif used_payment(payment_id):
            return False, 'Payment already been verified.'
        else:
            return True, None

    except paypalrestsdk.ResourceNotFound:
        return False, 'Payment Not Found'


def used_payment(payment_id):
    """Make sure same payment does not get reused
    """
    pp_verified_payments = set([line.strip()
                                for line in open(VERIFIED_PAYMENTS, "rw")])
    if payment_id in pp_verified_payments:
        return True
    else:
        with open(VERIFIED_PAYMENTS, "a") as f:
            f.write(payment_id + "\n")
            return False


def add_consent(customer_id=None, auth_code=None):
    """Send authorization code after obtaining customer
    consent. Exchange for long living refresh token for
    creating payments in future
    """
    refresh_token = api.get_refresh_token(auth_code)
    save_refresh_token(customer_id, refresh_token)


def remove_consent(customer_id):
    """Remove previously granted consent by customer
    """
    customer_token_map = dict([line.strip().split(",") for line in open(CUSTOMER_TOKEN_MAP)])
    customer_token_map.pop(customer_id, None)
    with open(CUSTOMER_TOKEN_MAP, "w") as f:
        for customer, token in customer_token_map:
            f.write(customer + "," + token + "\n")


def save_refresh_token(customer_id=None, refresh_token=None):
    """Store refresh token, likely in a database for app in production
    """
    with open(CUSTOMER_TOKEN_MAP, "a") as f:
        f.write(customer_id + "," + refresh_token + "\n")


def get_stored_refresh_token(customer_id=None):
    """If customer has already consented, return cached refresh token
    """
    try:
        customer_token_map = dict([line.strip().split(",") for line in open(CUSTOMER_TOKEN_MAP)])
        return customer_token_map.get(customer_id)
    except (OSError, IOError):
        return None


def charge_wallet(transaction, customer_id=None, correlation_id=None, intent="authorize"):
    """Charge a customer who formerly consented to future payments
    from paypal wallet.
    """
    payment = paypalrestsdk.Payment({
        "intent": intent,
        "payer": {
            "payment_method": "paypal"
        },
        "transactions": [
            {
                "amount": {
                    "total": transaction["amount"]["total"],
                    "currency": transaction["amount"]["currency"]
                },
                "description": transaction["description"]
            }
        ]
    })

    refresh_token = get_stored_refresh_token(customer_id)

    if not refresh_token:
        return False, "Customer has not granted consent as no refresh token has been found for customer. Authorization code needed to get new refresh token."

    if payment.create(refresh_token, correlation_id):
        print("Payment %s created successfully" % (payment.id))
        if payment['intent'] == "authorize":
            authorization_id = payment['transactions'][0][
                'related_resources'][0]['authorization']['id']
            print(
                "Payment %s authorized. Authorization id is %s" % (
                    payment.id, authorization_id
                )
            )
        return True, "Charged customer " + customer_id + " " + transaction["amount"]["total"]
    else:
        return False, "Error while creating payment:" + str(payment.error)


# # #############################
# # Flask application for a developer/merchant working with the subscription apis
#
#
# @app.route("/")
# def index():
#     if not session.get('logged_in'):
#         return redirect(url_for('login'))
#     else:
#         if session.get('merchant'):
#             return redirect(url_for('merchant'))
#         elif session.get('customer'):
#             return redirect(url_for('subscriptions'))
#
#
# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     error = None
#     if request.method == 'POST':
#         if request.form['username'] == app.config['MERCHANT_USERNAME'] \
#                 and request.form['password'] == app.config['MERCHANT_PASSWORD']:
#             session['logged_in'] = True
#             session['merchant'] = True
#             return redirect(request.args.get('next') or url_for('merchant'))
#         elif request.form['username'] == app.config['CUSTOMER_USERNAME'] \
#                 and request.form['password'] == app.config['CUSTOMER_PASSWORD']:
#             session['logged_in'] = True
#             session['customer'] = True
#             return redirect(request.args.get('next') or url_for('subscriptions'))
#         else:
#             error = "Invalid username or password"
#     return render_template('login.html', error=error)
#
#
# @app.route('/logout')
# def logout():
#     """
#     Clear session caches when logout is called
#     and return to base url
#     """
#     for key in ('customer', 'merchant', 'logged_in'):
#         session.pop(key, None)
#     return redirect(request.args.get('next') or '/')
#
#
# @app.route("/merchant")
# def merchant():
#     """
#     If merchant is logged in display billing plans in created and active states
#     """
#     if session.get('logged_in') and session.get('merchant'):
#         plans_created_query_dict = BillingPlan.all({
#             "status": "CREATED",
#             "sort_order": "DESC"
#         })
#         plans_created = plans_created_query_dict.to_dict().get('plans')
#         if not plans_created:
#             plans_created = []
#
#         plans_active_query_dict = BillingPlan.all({
#             "status": "ACTIVE",
#             "page_size": 5, "page": 0, "total_required": "yes"
#         })
#         plans_active = plans_active_query_dict.to_dict().get('plans')
#         if not plans_active:
#             plans_active = []
#
#         return render_template('merchant.html', plans_created=plans_created, plans_active=plans_active)
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/create", methods=['GET', 'POST'])
# def create():
#     """
#     Merchant creates new billing plan from input values in the form
#     """
#     if session.get('logged_in') and session.get('merchant'):
#         if request.method == 'POST':
#             billing_plan_attributes = {
#                 "name": request.form['name'],
#                 "description": request.form['description'],
#                 "type": request.form['type'],
#                 "merchant_preferences": {
#                     "auto_bill_amount": "yes",
#                     "cancel_url": "http://www.cancel.com",
#                     "initial_fail_amount_action": "continue",
#                     "max_fail_attempts": "1",
#                     "return_url": request.form['return_url'],
#                     "setup_fee": {
#                         "currency": request.form['currency'],
#                         "value": request.form['setup_fee']
#                     }
#                 },
#                 "payment_definitions": [
#                     {
#                         "name": request.form['payment_name'],
#                         "type": request.form['payment_type'],
#                         "frequency": request.form['frequency'],
#                         "frequency_interval": request.form['frequency_interval'],
#                         "cycles": request.form['cycles'],
#                         "amount": {
#                             "currency": request.form['currency'],
#                             "value": request.form['amount']
#                         },
#                         "charge_models": [
#                             {
#                                 "type": "SHIPPING",
#                                 "amount": {
#                                     "currency": request.form['currency'],
#                                     "value": request.form['shipping']
#                                 }
#                             },
#                             {
#                                 "type": "TAX",
#                                 "amount": {
#                                     "currency": request.form['currency'],
#                                     "value": request.form['tax']
#                                 }
#                             }
#                         ]
#                     }
#                 ]
#             }
#             billing_plan = BillingPlan(billing_plan_attributes)
#             if billing_plan.create():
#                 print("Billing Plan [%s] created successfully" % billing_plan.id)
#             else:
#                 print(billing_plan.error)
#             return redirect(url_for('merchant'))
#         return render_template('create.html')
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/activate", methods=['POST'])
# def activate():
#     """
#     Merchant activates plan after creation
#     """
#     if session.get('logged_in') and session.get('merchant'):
#         billing_plan = BillingPlan.find(request.args.get('id', ''))
#         if billing_plan.activate():
#             print("Billing Plan [%s] activated successfully" % billing_plan.id)
#         else:
#             print(billing_plan.error)
#         return redirect(url_for('merchant'))
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/subscriptions")
# def subscriptions():
#     """
#     If customer is logged in display active billing plans in
#     descending order of creation time
#     """
#     if session.get('logged_in') and session.get('customer'):
#
#         plans_active_query_dict = BillingPlan.all({"status": "ACTIVE",
#                                                    "sort_order": "DESC"})
#
#         if plans_active_query_dict:
#             plans_active = plans_active_query_dict.to_dict().get('plans')
#         else:
#             plans_active = []
#         return render_template('subscriptions.html', plans=plans_active)
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/subscribe", methods=['POST'])
# def subscribe():
#     """
#     Customer subscribes to a billing plan to form a billing agreement
#     """
#     if session.get('logged_in') and session.get('customer'):
#         billing_agreement = BillingAgreement({
#             "name": "Organization plan name",
#             "description": "Agreement for " + request.args.get('name', ''),
#             "start_date": (datetime.now() + timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%SZ'),
#             "plan": {
#                 "id": request.args.get('id', '')
#             },
#             "payer": {
#                 "payment_method": "paypal"
#             },
#             "shipping_address": {
#                 "line1": "StayBr111idge Suites",
#                 "line2": "Cro12ok Street",
#                 "city": "San Jose",
#                 "state": "CA",
#                 "postal_code": "95112",
#                 "country_code": "US"
#             }
#         })
#         if billing_agreement.create():
#             for link in billing_agreement.links:
#                 if link.rel == "approval_url":
#                     approval_url = link.href
#                     return redirect(approval_url)
#         else:
#             print(billing_agreement.error)
#         return redirect(url_for('subscriptions'))
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/execute")
# def execute():
#     """
#     Customer redirected to this endpoint by PayPal after payment approval
#     """
#     if session.get('logged_in') and session.get('customer'):
#         payment_token = request.args.get('token', '')
#         billing_agreement_response = BillingAgreement.execute(payment_token)
#         return redirect(url_for('agreement_details', id=billing_agreement_response.id))
#     else:
#         return redirect(url_for('login'))
#
#
# @app.route("/agreement-details")
# def agreement_details():
#     billing_agreement = BillingAgreement.find(request.args.get('id', ''))
#     return render_template('details.html', agreement=billing_agreement)
#
#
# @app.route("/payment-history")
# def payment_history():
#     """
#     Display transactions that happened over a billing agreement
#     """
#     start_date, end_date = "2016-06-01", "2017-06-30"
#     billing_agreement = BillingAgreement.find(request.args.get('id', ''))
#     transactions = billing_agreement.search_transactions(start_date, end_date)
#     return render_template('history.html', transactions=transactions.agreement_transaction_list,
#                            description=request.args.get('description', ''))
#
#
# # Flask application for a developer/merchant verifying payments and executing
# # payments on behalf of customer who has consented to future payments
#
#
# @app.route('/client_responses', methods=['POST'])
# def parse_response():
#     """
#     Check validity of a mobile payment made via credit card or PayPal,
#     or save customer consented to future payments
#     """
#     if not request.json or not 'response' or 'response_type' not in request.json:
#         raise InvalidUsage('Invalid mobile client response ')
#
#     if request.json['response_type'] == 'payment':
#         result, message = verify_payment(request.json)
#         if result:
#             return jsonify({"status": "verified"}), 200
#         else:
#             raise InvalidUsage(message, status_code=404)
#
#     elif request.json['response_type'] == 'authorization_code':
#         add_consent(request.json['customer_id'], request.json['response']['code'])
#         return jsonify({"status": "Received consent"}), 200
#
#     else:
#         raise InvalidUsage('Invalid response type')
#
#
# @app.route('/correlations', methods=['POST'])
# def correlations():
#     """
#     Send correlation id, customer id (e.g email) and transactions details for
#     purchase made by customer who formerly consented to future payments.
#     Can be used for immediate payments or authorize a payment for later execution
#
#     https://developer.paypal.com/docs/integration/direct/capture-payment/
#     """
#     result, message = charge_wallet(
#         transaction=request.json['transactions'][0],
#         customer_id=request.json['customer_id'],
#         correlation_id=request.json['correlation_id'],
#         intent=request.json['intent']
#     )
#     if result:
#         return jsonify({"status": message}), 200
#     else:
#         raise InvalidUsage(message)
#
#
# class InvalidUsage(Exception):
#     """
#     Errorhandler class to enable custom error message propagation
#     """
#     status_code = 400
#
#     def __init__(self, message, status_code=None, payload=None):
#         Exception.__init__(self)
#         self.message = message
#         if status_code is not None:
#             self.status_code = status_code
#         self.payload = payload
#
#     def to_dict(self):
#         rv = dict(self.payload or ())
#         rv['message'] = self.message
#         return rv
#
#
# @app.errorhandler(InvalidUsage)
# def handle_invalid_usage(error):
#     response = jsonify(error.to_dict())
#     response.status_code = error.status_code
#     return response
#
#
# if __name__ == '__main__':
#     port = int(os.environ.get("PORT", 888))
#     app.run(host='127.0.0.1', port=port, debug=True)
#
#
# # #############################
# # Test merchant_server
#
#
# class TestMerchantServer(unittest.TestCase):
#     def setUp(self):
#         """Before each test, set up a test client"""
#         app.config['TESTING'] = True
#         self.app = app.test_client()
#         self.response_dict = dict(
#             create_time='2014-02-12T22:29:49Z',
#             id='PAY-564191241M8701234KL57LXI',
#             intent='sale',
#             state='approved'
#         )
#         self.client_json = json.dumps(dict(
#             response_type='payment',
#             response=self.response_dict
#         ))
#
#     def test_empty_request(self):
#         """Check that request without body raises 400"""
#         rv = self.app.post('/client_responses')
#         self.assertEqual(rv.status_code, 400)
#         self.assertIn('Invalid mobile client response', rv.data)
#
#     def test_invalid_response_type(self):
#         """Check invalid response type is handled properly"""
#         json_data = json.dumps(dict(response_type='test', response='test'))
#         rv = self.app.post(
#             '/client_responses', data=json_data, content_type='application/json')
#         self.assertEqual(rv.status_code, 400)
#         self.assertIn('Invalid response type', rv.data)
#
#     @patch('merchant_server.verify_payment')
#     def test_verify_payment(self, mock):
#         """verify correct response on successful paypal payment verification"""
#         mock.return_value = True, None
#         rv = self.app.post(
#             '/client_responses', data=self.client_json, content_type='application/json')
#         self.assertEqual(rv.status_code, 200)
#         self.assertIn('verified', rv.data)
#
#     @patch('merchant_server.verify_payment')
#     def test_verify_payment_twice_fails(self, mock):
#         """Trying to verify an already verified payment is a bad request"""
#         mock.return_value = True, None
#         rv = self.app.post(
#             '/client_responses', data=self.client_json, content_type='application/json')
#         self.assertEqual(rv.status_code, 200)
#         self.assertIn('verified', rv.data)
#         mock.return_value = False, 'Payment already been verified.'
#         rv = self.app.post(
#             '/client_responses', data=self.client_json, content_type='application/json')
#         self.assertEqual(rv.status_code, 404)
#         self.assertIn('Payment already been verified', rv.data)
#
#     @patch('merchant_server.add_consent')
#     def test_send_future_payment_consent(self, mock):
#         """Test consent is received properly on merchant_server"""
#         mock.return_value = None
#         response_dict = dict(
#             code='EBYhRW3ncivudQn8UopLp4A28xIlqPDpAoqd7bi'
#         )
#         client_dict = dict(
#             environment='live',
#             paypal_sdk_version='2.0.1',
#             platform='iOS',
#             product_name='PayPal iOS SDK'
#         )
#         json_data = json.dumps(dict(
#             response_type='authorization_code',
#             response=response_dict,
#             customer_id='customer@gmail.com',
#             client=client_dict
#         ))
#         rv = self.app.post(
#             '/client_responses', data=json_data, content_type='application/json')
#         self.assertEqual(rv.status_code, 200)
#         self.assertIn('Received consent', rv.data)
#
#
# class TestPaypalClient(unittest.TestCase):
#     def setUp(self):
#         self.transaction = {
#             "amount": {
#                 "total": "1.00",
#                 "currency": "USD"
#             },
#             "description": "This is the payment transaction description."
#         }
#
#     def test_get_stored_refresh_token(self):
#         """Test that the correct refresh token is getting fetched for the customer"""
#         save_refresh_token(
#             'customer1@gmail.com', 'ref_token_sample')
#         refresh_token = get_stored_refresh_token(
#             'customer1@gmail.com')
#         self.assertEqual(refresh_token, 'ref_token_sample')
#
#     def test_remove_consent(self):
#         """Test removing consent deletes stored refresh token"""
#         save_refresh_token(
#             'customer1@gmail.com', 'ref_token_sample')
#         refresh_token = get_stored_refresh_token(
#             'customer1@gmail.com')
#         self.assertEqual(refresh_token, 'ref_token_sample')
#         remove_consent('customer1@gmail.com')
#         refresh_token = get_stored_refresh_token(
#             'customer1@gmail.com')
#         self.assertEqual(refresh_token, None)
#
#     def test_charge_wallet_missing_consent(self):
#         """Charging a new customer without consent will not work"""
#         return_status, message = charge_wallet(
#             self.transaction, 'new_customer@gmail.com', None, 'sale')
#         self.assertEqual(return_status, False)
#         self.assertIn("Customer has not granted consent", message)
#
#     @patch('paypal_client.paypalrestsdk.Payment.create')
#     @patch('paypal_client.get_stored_refresh_token')
#     def test_charge_wallet_failure(self, mock_create, mock_token):
#         """Test charge wallet fails with correct message"""
#         mock_token.return_value = False
#         mock_create.return_value = 'refresh_token'
#         return_status, message = charge_wallet(
#             self.transaction, 'customer1@gmail.com', 'correlation_id', 'sale')
#         self.assertEqual(return_status, False)
#         self.assertIn("Error while creating payment", message)
#
#     @patch('paypal_client.paypalrestsdk.Payment.create')
#     def test_charge_wallet_success(self, mock):
#         mock.return_value = True
#         save_refresh_token(
#             'customer1@gmail.com', 'ref_token_sample')
#         return_status, message = charge_wallet(
#             self.transaction, 'customer1@gmail.com', 'correlation_id', 'sale')
#         self.assertEqual(return_status, True)
#         self.assertIn("Charged customer customer1@gmail.com " +
#                       self.transaction["amount"]["total"], message)
#
#
# #############################
# Samples


def openid_connect():
    # Generate login url
    login_url = Tokeninfo.authorize_url({'scope': 'openid email'})

    # Create tokeninfo with Authorize code
    code = input('Authorize code: ')
    tokeninfo = Tokeninfo.create(code)
    print('tokeninfo', tokeninfo)

    # Refresh tokeninfo
    tokeninfo = tokeninfo.refresh()
    print('tokeninfo', tokeninfo)

    # Create tokeninfo with refresh_token
    refresh_token = input('Refresh token: ')
    tokeninfo = Tokeninfo.create_with_refresh_token(refresh_token)
    print('tokeninfo', tokeninfo)

    # Get userinfo
    userinfo = tokeninfo.userinfo()
    print('userinfo', userinfo)

    # Get userinfo with access_token
    access_token = input('Access token: ')
    userinfo = Userinfo.get(access_token)
    print('userinfo', userinfo)

    # Generate logout url
    logout_url = tokeninfo.logout_url()
    print('logout_url', logout_url)


# billing_plans
def subscription_billing_plans_get_all():
    history = BillingPlan.all({
        "status": "CREATED",
        "page_size": 5, "page": 1, "total_required": "yes"
    })
    print(history)

    print("List BillingPlan:")
    for plan in history.plans:
        print("  -> BillingPlan[%s]" % plan.id)


def subscription_billing_plans_get():
    try:
        billing_plan = BillingPlan.find("P-0NJ10521L3680291SOAQIVT")
        print("Got Billing Plan Details for Billing Plan[%s]" % billing_plan.id)

    except ResourceNotFound as error:
        print("Billing Plan Not Found")


def subscription_billing_plans_create():
    billing_plan = BillingPlan({
        "name": "Testing1-Regular1",
        "description": "Create Plan for Regular",
        "type": "INFINITE",
        "merchant_preferences": {
            "auto_bill_amount": "yes",
            "cancel_url": "http://www.cancel.com",
            "initial_fail_amount_action": "continue",
            "max_fail_attempts": "1",
            "return_url": "http://www.success.com",
            "setup_fee": {
                "currency": "USD",
                "value": "25"
            }
        },
        "payment_definitions": [
            {
                "name": "Regular 1",
                "type": "REGULAR",
                "frequency": "MONTH",
                "frequency_interval": "1",
                "cycles": "0",
                "amount": {
                    "currency": "USD",
                    "value": "100"
                },
                "charge_models": [
                    {
                        "type": "SHIPPING",
                        "amount": {
                            "currency": "USD",
                            "value": "10.60"
                        }
                    },
                    {
                        "type": "TAX",
                        "amount": {
                            "currency": "USD",
                            "value": "20"
                        }
                    }
                ]
            },
            {
                "amount": {
                    "currency": "USD",
                    "value": "20"
                },
                "charge_models": [
                    {
                        "amount": {
                            "currency": "USD",
                            "value": "10.60"
                        },
                        "type": "SHIPPING"
                    },
                    {
                        "amount": {
                            "currency": "USD",
                            "value": "20"
                        },
                        "type": "TAX"
                    }
                ],
                "cycles": "4",
                "frequency": "MONTH",
                "frequency_interval": "1",
                "name": "Trial 1",
                "type": "TRIAL"
            }
        ]
    })

    if billing_plan.create():
        print("Billing Plan [%s] created successfully" % billing_plan.id)
    else:
        print(billing_plan.error)


def subscription_billing_plans_activate():
    try:
        billing_plan = BillingPlan.find("P-0NJ10521L3680291SOAQIVT")
        print("Got Billing Plan Details for Billing Plan[%s]" % billing_plan.id)

        if billing_plan.activate():
            billing_plan = BillingPlan.find("P-0NJ10521L3680291SOAQIVT")
            print("Billing Plan [%s] state changed to [%s]" % (billing_plan.id, billing_plan.state))
        else:
            print(billing_plan.error)

    except ResourceNotFound as error:
        print("Billing Plan Not Found")


def subscription_billing_plans_replace():
    try:
        billing_plan = BillingPlan.find("P-0NJ10521L3680291SOAQIVT")
        print("Got Billing Plan Details for Billing Plan[%s]" % billing_plan.id)

        billing_plan_update_attributes = [
            {
                "op": "replace",
                "path": "/",
                "value": {
                    "state": "ACTIVE"
                }
            }
        ]

        if billing_plan.replace(billing_plan_update_attributes):
            billing_plan = BillingPlan.find("P-0NJ10521L3680291SOAQIVT")
            print("Billing Plan [%s] state changed to [%s]" % (billing_plan.id, billing_plan.state))
        else:
            print(billing_plan.error)

    except ResourceNotFound as error:
        print("Billing Plan Not Found")


BILLING_AGREEMENT_ID = "I-HT38K76XPMGJ"


def subscription_billing_agreements_create():
    billing_agreement = BillingAgreement({
        "name": "Fast Speed Agreement",
        "description": "Agreement for Fast Speed Plan",
        "start_date": "2015-02-19T00:37:04Z",
        "plan": {
            "id": "P-0NJ10521L3680291SOAQIVTQ"
        },
        "payer": {
            "payment_method": "paypal"
        },
        "shipping_address": {
            "line1": "StayBr111idge Suites",
            "line2": "Cro12ok Street",
            "city": "San Jose",
            "state": "CA",
            "postal_code": "95112",
            "country_code": "US"
        }
    })

    if billing_agreement.create():
        print("Billing Agreement created successfully")
    else:
        print(billing_agreement.error)


def subscription_billing_agreements_execute():
    # Execute an approved PayPal Billing Agreement
    # Use this call to execute (complete) a PayPal BillingAgreement that has
    # been approved by the payer.
    billing_agreement = BillingAgreement({
        "name": "Fast Speed Agreement",
        "description": "Agreement for Fast Speed Plan",
        "start_date": "2015-02-19T00:37:04Z",
        "plan": {
            "id": "P-0NJ10521L3680291SOAQIVTQ"
        },
        "payer": {
            "payment_method": "paypal"
        },
        "shipping_address": {
            "line1": "StayBr111idge Suites",
            "line2": "Cro12ok Street",
            "city": "San Jose",
            "state": "CA",
            "postal_code": "95112",
            "country_code": "US"
        }
    })

    # After creating the agreement, redirect user to the url provided in links array
    # entry with method field set to REDIRECT
    if billing_agreement.create():
        print("Billing Agreement created successfully")
        for link in billing_agreement.links:
            if link.rel == "approval_url":
                approval_url = link.href
                print("For approving billing agreement, redirect user to\n [%s]" % approval_url)
    else:
        print(billing_agreement.error)

    # After user approves the agreement, call execute with the payment token appended to
    # the redirect url to execute the billing agreement.
    # https://github.paypal.com/pages/lkutch/paypal-developer-docs/api/#execute-an-agreement
    payment_token = ''  # ????
    billing_agreement_response = BillingAgreement.execute(payment_token)
    print("BillingAgreement[%s] executed successfully" % billing_agreement_response.id)


def subscription_billing_agreements_replace():
    try:
        billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
        print("Got Billing Agreement Details for Billing Agreement[%s]" % billing_agreement.id)

        billing_agreement_update_attributes = [
            {
                "op": "replace",
                "path": "/",
                "value": {
                    "description": "New Description",
                    "name": "New Name",
                    "shipping_address": {
                        "line1": "StayBr111idge Suites",
                        "line2": "Cro12ok Street",
                        "city": "San Jose",
                        "state": "CA",
                        "postal_code": "95112",
                        "country_code": "US"
                    }
                }
            }
        ]

        if billing_agreement.replace(billing_agreement_update_attributes):
            print("Billing Agreement [%s] name changed to [%s]" % (billing_agreement.id, billing_agreement.name))
            print("Billing Agreement [%s] description changed to [%s]" % (
                billing_agreement.id, billing_agreement.description))
        else:
            print(billing_agreement.error)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


def subscription_billing_agreements_get():
    try:
        billing_agreement = BillingAgreement.find("I-HT38K76XPMGJ")
        print("Got Billing Agreement Details for Billing Agreement[%s]" % billing_agreement.id)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


def subscription_billing_agreements_suspend_and_re_activate():
    try:
        billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
        print("Billing Agreement [%s] has state %s" %
              (billing_agreement.id, billing_agreement.state))

        suspend_note = {
            "note": "Suspending the agreement"
        }

        if billing_agreement.suspend(suspend_note):
            # Would expect state has changed to Suspended
            billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
            print("Billing Agreement [%s] has state %s" %
                  (billing_agreement.id, billing_agreement.state))

            reactivate_note = {
                "note": "Reactivating the agreement"
            }

            if billing_agreement.reactivate(reactivate_note):
                # Would expect state has changed to Active
                billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
                print("Billing Agreement [%s] has state %s" % (billing_agreement.id, billing_agreement.state))

            else:
                print(billing_agreement.error)

        else:
            print(billing_agreement.error)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


def subscription_billing_agreements_cancel():
    try:
        billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
        print("Billing Agreement [%s] has state %s" %
              (billing_agreement.id, billing_agreement.state))

        cancel_note = {"note": "Canceling the agreement"}

        if billing_agreement.cancel(cancel_note):
            # Would expect status has changed to Cancelled
            billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
            print("Billing Agreement [%s] has state %s" %
                  (billing_agreement.id, billing_agreement.state))

        else:
            print(billing_agreement.error)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


def subscription_billing_agreements_search_transactions():
    try:
        billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)

        start_date = "2014-07-01"
        end_date = "2014-07-20"

        transactions = billing_agreement.search_transactions(start_date, end_date)
        for transaction in transactions.agreement_transaction_list:
            print("  -> Transaction[%s]" % transaction.transaction_id)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


def subscription_billing_agreements_set_balance_and_bill_balance():
    try:
        billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
        print("Billing Agreement [%s] has outstanding balance %s" % (
            billing_agreement.id, billing_agreement.outstanding_balance.value))

        outstanding_amount = {
            "value": "10",
            "currency": "USD"
        }

        if billing_agreement.set_balance(outstanding_amount):
            billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
            print("Billing Agreement [%s] has outstanding balance %s" % (
                billing_agreement.id, billing_agreement.outstanding_balance.value))

            outstanding_amount_note = {
                "note": "Billing Balance Amount",
                "amount": outstanding_amount
            }

            if billing_agreement.bill_balance(outstanding_amount_note):
                billing_agreement = BillingAgreement.find(BILLING_AGREEMENT_ID)
                print("Billing Agreement [%s] has outstanding balance %s" % (
                    billing_agreement.id, billing_agreement.outstanding_balance.value))

            else:
                print(billing_agreement.error)

        else:
            print(billing_agreement.error)

    except ResourceNotFound as error:
        print("Billing Agreement Not Found")


# end billing_agreements


def sale_find():
    # Get Details of a Sale Transaction Sample
    # This sample code demonstrates how you can retrieve
    # details of completed Sale Transaction.
    # API used: /v1/payments/sale/{sale-id}
    try:
        # Get Sale object by passing sale id
        sale = Sale.find("7DY409201T7922549")
        print("Got Sale details for Sale[%s]" % sale.id)

    except ResourceNotFound as error:
        print("Sale Not Found")


def sale_refund():
    # SaleRefund Sample
    # This sample code demonstrate how you can
    # process a refund on a sale transaction created
    # using the Payments API.
    # API used: /v1/payments/sale/{sale-id}/refund
    sale = Sale.find("7DY409201T7922549")

    # Make Refund API call
    # Set amount only if the refund is partial
    refund = sale.refund({
        "amount": {
            "total": "0.01",
            "currency": "USD"}})

    # Check refund status
    if refund.success():
        print("Refund[%s] Success" % refund.id)
    else:
        print("Unable to Refund")
        print(refund.error)


def payout_item_cancel():
    try:
        payout_item = PayoutItem.find("YKV9JPD7C2PCY")
        print("Got Details for PayoutItem[%s]" % payout_item.payout_item_id)

        if payout_item.cancel():
            print("Payout Item[%s] cancelled successfully" % payout_item.payout_item_id)
        else:
            print(payout_item.error)

    except ResourceNotFound as error:
        print("Payout Item Not Found")


def payout_item_get():
    try:
        payout_item = PayoutItem.find("YKV9JPD7C2PCY")
        print("Got Details for PayoutItem[%s]" % payout_item.payout_item_id)

    except ResourceNotFound as error:
        print("Payout Item Not Found")


def payout_create():
    sender_batch_id = ''.join(random.choice(string.ascii_uppercase) for i in range(12))

    payout = Payout({
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": 0.99,
                    "currency": "USD"
                },
                "receiver": "shirt-supplier-one@mail.com",
                "note": "Thank you.",
                "sender_item_id": "item_1"
            },
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": 0.90,
                    "currency": "USD"
                },
                "receiver": "shirt-supplier-two@mail.com",
                "note": "Thank you.",
                "sender_item_id": "item_2"
            },
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": 2.00,
                    "currency": "USD"
                },
                "receiver": "shirt-supplier-three@mail.com",
                "note": "Thank you.",
                "sender_item_id": "item_3"
            }
        ]
    })

    if payout.create():
        print("payout[%s] created successfully" %
              (payout.batch_header.payout_batch_id))
    else:
        print(payout.error)


def payout_create_single_payout():
    sender_batch_id = ''.join(random.choice(string.ascii_uppercase) for i in range(12))

    payout = Payout({
        "sender_batch_header": {
            "sender_batch_id": sender_batch_id,
            "email_subject": "You have a payment"
        },
        "items": [
            {
                "recipient_type": "EMAIL",
                "amount": {
                    "value": 0.99,
                    "currency": "USD"
                },
                "receiver": "shirt-supplier-one@mail.com",
                "note": "Thank you.",
                "sender_item_id": "item_1"
            }
        ]
    })

    if payout.create(sync_mode=True):
        print("payout[%s] created successfully" %
              (payout.batch_header.payout_batch_id))
    else:
        print(payout.error)


def payout_get():
    try:
        payout = Payout.find("R3LFR867ESVQY")
        print("Got Details for Payout[%s]" % payout.batch_header.payout_batch_id)

    except ResourceNotFound as error:
        print("Web Profile Not Found")


def payment_experience_web_profile_create_payment_with_customized_experience():
    # Create a PayPal Payment with custom PayPal checkout experience
    # For a simple payment example, refer to samples/payment/create_with_paypal.py
    #
    # Refer to https://developer.paypal.com/docs/integration/direct/rest-experience-overview/
    # to see the available options

    # Name needs to be unique so just generating a random one
    wpn = ''.join(random.choice(string.ascii_uppercase) for i in range(12))

    web_profile = WebProfile({
        "name": wpn,
        "presentation": {
            "brand_name": "YeowZa Paypal",
            "logo_image": "http://s3-ec.buzzfed.com/static/2014-07/18/8/enhanced/webdr02/anigif_enhanced-buzz-21087-1405685585-12.gif",
            "locale_code": "US"
        },
        "input_fields": {
            "allow_note": True,
            "no_shipping": 1,
            "address_override": 1
        },
        "flow_config": {
            "landing_page_type": "billing",
            "bank_txn_pending_url": "http://www.yeowza.com"
        }
    })

    if web_profile.create():
        print("Web Profile[%s] created successfully" % web_profile.id)
    else:
        print(web_profile.error)

    payment = Payment({
        "intent": "sale",
        "experience_profile_id": web_profile.id,
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://return.url",
            "cancel_url": "http://cancel.url"
        },
        "transactions": [
            {
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "1.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },
                "amount": {
                    "currency": "USD",
                    "total": "1.00"
                },
                "description": "This is the payment description."
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                redirect_url = str(link.href)
                print("Redirect for approval: %s" % redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_experience_web_profile_delete():
    web_profile = WebProfile.find("XP-9R62-L4QJ-M8H6-UNV5")

    if web_profile.delete():
        print("WebProfile deleted")
    else:
        print(web_profile.error)


def payment_experience_web_profile_get():
    try:
        web_profile = WebProfile.find("XP-3NWU-L5YK-X5EC-6KJM")
        print("Got Details for Web Profile[%s]" % web_profile.id)

    except ResourceNotFound as error:
        print("Web Profile Not Found")


def payment_experience_web_profile_get_all():
    history = WebProfile.all()
    print(history)

    print("List WebProfile:")
    for profile in history:
        print("  -> WebProfile[%s]" % profile.name)


def payment_experience_web_profile_replace():
    try:
        web_profile = WebProfile.find("XP-3NWU-L5YK-X5EC-6KJM")
        print("Got Web Profile Details for Web Profile[%s]" % web_profile.id)

        web_profile_update_attributes = [
            {
                "op": "replace",
                "path": "/presentation/brand_name",
                "value": "New Brand Name"
            }
        ]

        if web_profile.replace(web_profile_update_attributes):
            web_profile = WebProfile.find("XP-3NWU-L5YK-X5EC-6KJM")
            print("Web Profile [%s] name changed to [%s]" % (web_profile.id, web_profile.presentation.brand_name))
        else:
            print(web_profile.error)

    except ResourceNotFound as error:
        print("Web Profile Not Found")


def payment_experience_web_profile_update():
    web_profile_update_attributes = {
        "name": "YeowZa! T-Shirt Shop",
        "presentation": {
            "logo_image": "http://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Nature_photographer.jpg/1599px-Nature_photographer.jpg"
        },
        "input_fields": {
            "no_shipping": 1,
            "address_override": 1
        },
        "flow_config": {
            "landing_page_type": "billing",
            "bank_txn_pending_url": "http://www.yeowza.com"
        }
    }

    web_profile = WebProfile.find("XP-9R62-L4QJ-M8H6-UNV5")

    if web_profile.update(web_profile_update_attributes):
        print("Web Profile[%s] updated successfully" % web_profile.id)
    else:
        print(web_profile.error)


def payment_all():
    # GetPaymentList Sample
    # This sample code demonstrate how you can
    # retrieve a list of all Payment resources
    # you've created using the Payments API.
    # Note various query parameters that you can
    # use to filter, and paginate through the
    # payments list.
    # API used: GET /v1/payments/payments

    # Retrieve
    # Retrieve the PaymentHistory  by calling the
    # `all` method
    # on the Payment class
    # Refer the API documentation
    # for valid values for keys
    # Supported paramters are :count, :next_id
    payment_history = Payment.all({"count": 2})

    # List Payments
    print("List Payment:")
    for payment in payment_history.payments:
        print("  -> Payment[%s]" % payment.id)


def payment_create_future_payment():
    # Create Future Payment Using PayPal Wallet
    # https://developer.paypal.com/docs/integration/mobile/make-future-payment/

    # authorization code from mobile sdk
    authorization_code = ''

    # Exchange authorization_code for long living refresh token. You should store
    # it in a database for later use
    refresh_token = api.get_refresh_token(authorization_code)

    # correlation id from mobile sdk
    correlation_id = ''

    # Initialize the payment object
    payment = paypalrestsdk.Payment({
        "intent": "authorize",
        "payer": {
            "payment_method": "paypal"},
        "transactions": [
            {
                "amount": {
                    "total": "0.17",
                    "currency": "USD"},
                "description": "This is the payment transaction description."
            }
        ]
    })

    # Create the payment. Set intent to sale to charge immediately,
    # else if authorize use the authorization id to capture
    # payment later using samples/authorization/capture.py
    if payment.create(refresh_token, correlation_id):
        print("Payment %s created successfully" % payment.id)
        if payment['intent'] == "authorize":
            print("Payment %s authorized. Authorization id is %s" % (
                payment.id, payment['transactions'][0]['related_resources'][0]['authorization']['id']))
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_create_with_credit_card():
    # CreatePayment using credit card Sample
    # This sample code demonstrate how you can process
    # a payment with a credit card.
    # API used: /v1/payments/payment

    # Payment
    # A Payment Resource; create one using
    # the above types and intent as 'sale'
    payment = Payment({
        "intent": "sale",

        # Payer
        # A resource representing a Payer that funds a payment
        # Use the List of `FundingInstrument` and the Payment Method
        # as 'credit_card'
        "payer": {
            "payment_method": "credit_card",

            # FundingInstrument
            # A resource representing a Payeer's funding instrument.
            # Use a Payer ID (A unique identifier of the payer generated
            # and provided by the facilitator. This is required when
            # creating or using a tokenized funding instrument)
            # and the `CreditCardDetails`
            "funding_instruments": [
                {
                    # CreditCard
                    # A resource representing a credit card that can be
                    # used to fund a payment.
                    "credit_card": {
                        "type": "visa",
                        "number": "4417119669820331",
                        "expire_month": "11",
                        "expire_year": "2018",
                        "cvv2": "874",
                        "first_name": "Joe",
                        "last_name": "Shopper",

                        # Address
                        # Base Address used as shipping or billing
                        # address in a payment. [Optional]
                        "billing_address": {
                            "line1": "52 N Main ST",
                            "city": "Johnstown",
                            "state": "OH",
                            "postal_code": "43210",
                            "country_code": "US"
                        }
                    }
                }
            ]
        },
        # Transaction
        # A transaction defines the contract of a
        # payment - what is the payment for and who
        # is fulfilling it.
        "transactions": [
            {
                # ItemList
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "1.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },

                # Amount
                # Let's you specify a payment amount.
                "amount": {
                    "total": "1.00",
                    "currency": "USD"},
                "description": "This is the payment transaction description."
            }
        ]
    })

    # Create Payment and return status( True or False )
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
    else:
        # Display Error message
        print("Error while creating payment:")
        print(payment.error)


def payment_create_with_credit_card_token():
    # CreatePayment Using Saved Card Sample
    # This sample code demonstrates how you can process a
    # Payment using a previously saved credit card.
    # API used: /v1/payments/payment

    # Payment
    # A Payment Resource; create one using
    # the above types and intent as 'sale'
    payment = Payment({
        "intent": "sale",
        # Payer
        # A resource representing a Payer that funds a payment
        # Use the List of `FundingInstrument` and the Payment Method
        # as 'credit_card'
        "payer": {
            "payment_method": "credit_card",

            # FundingInstrument
            # A resource representing a Payeer's funding instrument.
            # In this case, a Saved Credit Card can be passed to
            # charge the payment.
            "funding_instruments": [
                {
                    # CreditCardToken
                    # A resource representing a credit card that can be
                    # used to fund a payment.
                    "credit_card_token": {
                        "credit_card_id": "CARD-5BT058015C739554AKE2GCEI"
                    }
                }
            ]
        },

        # Transaction
        # A transaction defines the contract of a
        # payment - what is the payment for and who
        # is fulfilling it
        "transactions": [
            {
                # ItemList
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "1.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },

                # Amount
                # Let's you specify a payment amount.
                "amount": {
                    "total": "1.00",
                    "currency": "USD"
                },
                "description": "This is the payment transaction description."
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_create_with_paypal():
    # Create Payment Using PayPal Sample
    # This sample code demonstrates how you can process a
    # PayPal Account based Payment.
    # API used: /v1/payments/payment

    # Payment
    # A Payment Resource; create one using
    # the above types and intent as 'sale'
    payment = Payment({
        "intent": "sale",

        # Payer
        # A resource representing a Payer that funds a payment
        # Payment Method as 'paypal'
        "payer": {
            "payment_method": "paypal"},

        # Redirect URLs
        "redirect_urls": {
            "return_url": "http://localhost:3000/payment/execute",
            "cancel_url": "http://localhost:3000/"
        },

        # Transaction
        # A transaction defines the contract of a
        # payment - what is the payment for and who
        # is fulfilling it.
        "transactions": [
            {
                # ItemList
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "5.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },

                # Amount
                # Let's you specify a payment amount.
                "amount": {
                    "total": "5.00",
                    "currency": "USD"
                },
                "description": "This is the payment transaction description."
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                # Convert to str to avoid google appengine unicode issue
                # https://github.com/paypal/rest-api-sdk-python/pull/58
                redirect_url = str(link.href)
                print("Redirect for approval: %s" % redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_create_with_paypal_further_capabilities():
    # Create Payment Using PayPal Sample
    # For a simple payment example, refer to create_with_paypal.py
    #
    # Refer to https://developer.paypal.com/docs/integration/direct/explore-payment-capabilities/
    # to and https://developer.paypal.com/docs/release-notes/#updates-for-13-august-2014 to explore
    # extra payment options available such as fee, tax, shipping discount,
    # invoice number etc.

    payment = Payment({
        "intent": "sale",
        "redirect_urls": {
            "return_url": "http://www.return.com",
            "cancel_url": "http://www.cancel.com"
        },
        "payer": {
            "payment_method": "paypal",
            "payer_info": {
                "tax_id_type": "BR_CPF",
                "tax_id": "Fh618775690"
            }
        },
        "transactions": [
            {
                "amount": {
                    "total": "34.07",
                    "currency": "USD",
                    "details": {
                        "subtotal": "30.00",
                        "tax": "0.07",
                        "shipping": "1.00",
                        "handling_fee": "1.00",
                        "shipping_discount": "1.00",
                        "insurance": "1.00"
                    }
                },
                "description": "This is the payment transaction description.",
                "custom": "PP_EMS_90048630024435",
                "invoice_number": "48787589677",
                "payment_options": {
                    "allowed_payment_method": "INSTANT_FUNDING_SOURCE"
                },
                "soft_descriptor": "ECHI5786786",
                "item_list": {
                    "items": [
                        {
                            "name": "bowling",
                            "description": "Bowling Team Shirt",
                            "quantity": "5",
                            "price": "3",
                            "tax": "0.01",
                            "sku": "1",
                            "currency": "USD"
                        },
                        {
                            "name": "mesh",
                            "description": "80s Mesh Sleeveless Shirt",
                            "quantity": "1",
                            "price": "17",
                            "tax": "0.02",
                            "sku": "product34",
                            "currency": "USD"
                        },
                        {
                            "name": "discount",
                            "quantity": "1",
                            "price": "-2.00",
                            "sku": "product",
                            "currency": "USD"
                        }
                    ],
                    "shipping_address": {
                        "recipient_name": "Betsy Buyer",
                        "line1": "111 First Street",
                        "city": "Saratoga",
                        "country_code": "US",
                        "postal_code": "95070",
                        "state": "CA"
                    }
                }
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                # Convert to str to avoid google appengine unicode issue
                # https://github.com/paypal/rest-api-sdk-python/pull/58
                redirect_url = str(link.href)
                print("Redirect for approval: %s" % redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_create_with_paypal_security_test():
    # Create Payment Using PayPal Sample against endpoint
    # that only allows an acceptable highest TLS version

    # Payment
    # A Payment Resource; create one using
    # the above types and intent as 'sale'
    payment = paypalrestsdk.Payment({
        "intent": "sale",

        # Payer
        # A resource representing a Payer that funds a payment
        # Payment Method as 'paypal'
        "payer": {
            "payment_method": "paypal"},

        # Redirect URLs
        "redirect_urls": {
            "return_url": "http://localhost:3000/payment/execute",
            "cancel_url": "http://localhost:3000/"
        },

        # Transaction
        # A transaction defines the contract of a
        # payment - what is the payment for and who
        # is fulfilling it.
        "transactions": [
            {
                # ItemList
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "5.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },

                # Amount
                # Let's you specify a payment amount.
                "amount": {
                    "total": "5.00",
                    "currency": "USD"},
                "description": "This is the payment transaction description."
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                # Convert to str to avoid google appengine unicode issue
                # https://github.com/paypal/rest-api-sdk-python/pull/58
                redirect_url = str(link.href)
                print("Redirect for approval: %s" % redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)


def payment_execute():
    # Execute an approved PayPal payment
    # Use this call to execute (complete) a PayPal payment that has been approved by the payer.
    # You can optionally update transaction information by passing in one or more transactions.
    # API used: /v1/payments/payment

    # ID of the payment. This ID is provided when creating payment.
    payment = Payment.find("PAY-28103131SP722473WKFD7VGQ")

    # PayerID is required to approve the payment.
    if payment.execute({"payer_id": "DUFRQ8GWYMJXC"}):  # return True or False
        print("Payment[%s] execute successfully" % payment.id)
    else:
        print(payment.error)


def payment_execute_with_updated_shipping():
    payment = Payment.find("<PAYMENT_ID>")

    # e.g. Update shipping amount in the transactions array after payment creation
    # and calling payment execute
    # https://developer.paypal.com/webapps/developer/docs/api/#transaction-object
    execute_payment_json = {
        "payer_id": "HCXTE7DLHVTDN",
        "transactions": [
            {
                "amount": {
                    "total": "35.07",
                    "currency": "USD",
                    "details": {
                        "subtotal": "30.00",
                        "tax": "0.07",
                        "shipping": "2.00",
                        "handling_fee": "1.00",
                        "shipping_discount": "1.00",
                        "insurance": "1.00"
                    }
                }
            }
        ]
    }

    if payment.execute(execute_payment_json):  # return True or False
        print("Payment[%s] execute successfully" % payment.id)
    else:
        print(payment.error)


def payment_find():
    # GetPayment Sample
    # This sample code demonstrates how you can retrieve
    # the details of a payment resource.
    # API used: /v1/payments/payment/{payment-id}

    try:
        # Retrieve the payment object by calling the
        # `find` method
        # on the Payment class by passing Payment ID
        payment = Payment.find("PAY-0XL713371A312273YKE2GCNI")
        print("Got Payment Details for Payment[%s]" % payment.id)

    except ResourceNotFound as error:
        # It will through ResourceNotFound exception if the payment not found
        print("Payment Not Found")


def order_authorize():
    order = Order.find("<ORDER_ID>")

    response = order.authorize({
        "amount": {
            "currency": "USD",
            "total": "0.08"
        }
    })

    if order.success():
        print("Authorized[%s] successfully" % order.id)
    else:
        print(order.error)


def order_capture():
    order = Order.find("<ORDER_ID>")
    capture = order.capture({
        "amount": {
            "currency": "USD",
            "total": "4.54"},
        "is_final_capture": True})

    if capture.success():
        print("Capture[%s] successfully" % capture.id)
    else:
        print(capture.error)


def order_create():
    # Payment
    # A Payment Resource of type order; intent set as 'order'
    payment = Payment({
        "intent": "order",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": "http://return.url",
            "cancel_url": "http://cancel.url"
        },
        "transactions": [
            {
                "item_list": {
                    "items": [
                        {
                            "name": "item",
                            "sku": "item",
                            "price": "1.00",
                            "currency": "USD",
                            "quantity": 1
                        }
                    ]
                },
                "amount": {
                    "currency": "USD",
                    "total": "1.00"
                },
                "description": "This is the payment description."
            }
        ]
    })

    # Create Payment and return status
    if payment.create():
        print("Payment[%s] created successfully" % payment.id)
        # Redirect the user to given approval url
        for link in payment.links:
            if link.method == "REDIRECT":
                redirect_url = str(link.href)
                print("Redirect for approval: %s" % redirect_url)
    else:
        print("Error while creating payment:")
        print(payment.error)


def order_find():
    try:
        order = Order.find("99M58264FG144833V")
        print("Got Order details for Order[%s]" % order.id)

    except ResourceNotFound as error:
        print("Order Not Found")


def order_void():
    order = Order.find("O-0FJ734297A068505V")

    if order.void():
        print("Void Order successfully")
    else:
        print(order.error)


def notification_webhook_create():
    webhook = Webhook({
        "url": "https://www.yeowza.com/paypal_webhook",
        "event_types": [
            {
                "name": "PAYMENT.AUTHORIZATION.CREATED"
            },
            {
                "name": "PAYMENT.AUTHORIZATION.VOIDED"
            }
        ]
    })

    if webhook.create():
        print("Webhook[%s] created successfully" % webhook.id)
    else:
        print(webhook.error)


def notification_webhook_delete():
    webhook = Webhook.find("8TJ12214WP9291246")

    if webhook.delete():
        print("Webhook deleted")
    else:
        print(webhook.error)


def notification_webhook_get():
    try:
        webhook = Webhook.find("8TJ12214WP9291246")
        print("Got Details for Webhook[%s]" % webhook.id)

    except ResourceNotFound as error:
        print("Webhook Not Found")


def notification_webhook_get_all():
    history = Webhook.all()
    print(history)

    print("List Webhook:")
    for webhook in history:
        print("  -> Webhook[%s]" % webhook.name)


def notification_webhook_get_event_types():
    try:
        webhook = Webhook.find("70Y03947RF112050J")
        webhook_event_types = webhook.get_event_types()
        print(webhook_event_types)

    except ResourceNotFound as error:
        print("Webhook Not Found")


def notification_webhook_replace():
    try:
        webhook = Webhook.find("70Y03947RF112050J")
        print("Got Webhook Details for Webhook[%s]" % webhook.id)

        webhook_replace_attributes = [
            {
                "op": "replace",
                "path": "/url",
                "value": "https://www.yeowza.com/paypal_webhook_url"
            },
            {
                "op": "replace",
                "path": "/event_types",
                "value": [
                    {
                        "name": "PAYMENT.SALE.REFUNDED"
                    }
                ]
            }
        ]

        if webhook.replace(webhook_replace_attributes):
            webhook = Webhook.find("70Y03947RF112050J")
            print("Webhook [%s] url changed to [%s]" % (webhook.id, webhook.url))
        else:
            print(webhook.error)

    except ResourceNotFound as error:
        print("Webhook Not Found")


def notification_webhook_events_get_webhook_event_resource():
    """
    An example to demonstrate getting the paypal resource sent via
    the webhook event to the merchant server. It is recommended that
    the payload is verified before this. Refer to verify_webhook_events.py
    in the samples for more information regarding this.
    """
    webhook_event_json = {
        "id": "WH-8JH82006PH834764Y-9XE6617312678932V",
        "create_time": "2014-11-03T01:07:13Z",
        "resource_type": "authorization",
        "event_type": "PAYMENT.AUTHORIZATION.CREATED",
        "summary": "A successful payment authorization was created for $ 5.0 USD",
        "resource": {
            "id": "3WG12057BW522780J",
            "create_time": "2014-11-03T01:05:27Z",
            "update_time": "2014-11-03T01:07:08Z",
            "amount": {
                "total": "5.00",
                "currency": "USD",
                "details": {
                    "subtotal": "5.00"
                }
            },
            "payment_mode": "INSTANT_TRANSFER",
            "state": "authorized",
            "protection_eligibility": "ELIGIBLE",
            "protection_eligibility_type": "ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE",
            "parent_payment": "PAY-3KA96482AM105222NKRLNJVY",
            "valid_until": "2014-12-02T01:05:27Z",
            "links": [
                {
                    "href": "https://api.sandbox.paypal.com/v1/payments/authorization/3WG12057BW522780J",
                    "rel": "self",
                    "method": "GET"
                },
                {
                    "href": "https://api.sandbox.paypal.com/v1/payments/authorization/3WG12057BW522780J/capture",
                    "rel": "capture",
                    "method": "POST"
                },
                {
                    "href": "https://api.sandbox.paypal.com/v1/payments/authorization/3WG12057BW522780J/void",
                    "rel": "void",
                    "method": "POST"
                },
                {
                    "href": "https://api.sandbox.paypal.com/v1/payments/authorization/3WG12057BW522780J/reauthorize",
                    "rel": "reauthorize",
                    "method": "POST"
                },
                {
                    "href": "https://api.sandbox.paypal.com/v1/payments/payment/PAY-3KA96482AM105222NKRLNJVY",
                    "rel": "parent_payment",
                    "method": "GET"
                }
            ]
        },
        "links": [
            {
                "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-8JH82006PH834764Y-9XE6617312678932V",
                "rel": "self",
                "method": "GET"
            },
            {
                "href": "https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-8JH82006PH834764Y-9XE6617312678932V/resend",
                "rel": "resend",
                "method": "POST"
            }
        ]
    }

    webhook_event = WebhookEvent(webhook_event_json)
    # event_resource is wrapped the corresponding paypalrestsdk class
    # this is dynamically resolved by the sdk
    event_resource = webhook_event.get_resource()


def notification_webhook_events_list_webhooks_event_types():
    history = WebhookEventType.all()
    print(history)


def notification_webhook_events_resend():
    webhook_event = WebhookEvent.find("8PT597110X687430LKGECATA")

    if webhook_event.resend():  # return True or False
        print("webhook event[%s] resend successfully" % webhook_event.id)
    else:
        print(webhook_event.error)


def notification_webhook_events_search_webhook_events():
    """Searching webhook events. As of Nov 2014, only searching
    events within last six months and for a 45 day window is
    supported
    """
    parameters = {
        "page_size": 15,
        "start_time": "2014-11-06T11:00:00Z",
        "end_time": "2014-12-06T11:00:00Z"
    }

    events = WebhookEvent.all(parameters)
    print(events)


def notification_webhook_events_verify_webhook_events():
    """Verify received webhook payload on merchant server.
    Recommended first step before starting to parse the message e.g.
    get_webhook_event_resource.py in the samples folder

    Helpful link https://developer.paypal.com/docs/integration/direct/rest-webhooks-overview/
    """
    from paypalrestsdk.notifications import WebhookEvent
    # The payload body sent in the webhook event
    event_body = '{"id":"WH-0G2756385H040842W-5Y612302CV158622M","create_time":"2015-05-18T15:45:13Z","resource_type":"sale","event_type":"PAYMENT.SALE.COMPLETED","summary":"Payment completed for $ 20.0 USD","resource":{"id":"4EU7004268015634R","create_time":"2015-05-18T15:44:02Z","update_time":"2015-05-18T15:44:21Z","amount":{"total":"20.00","currency":"USD"},"payment_mode":"INSTANT_TRANSFER","state":"completed","protection_eligibility":"ELIGIBLE","protection_eligibility_type":"ITEM_NOT_RECEIVED_ELIGIBLE,UNAUTHORIZED_PAYMENT_ELIGIBLE","parent_payment":"PAY-86C81811X5228590KKVNARQQ","transaction_fee":{"value":"0.88","currency":"USD"},"links":[{"href":"https://api.sandbox.paypal.com/v1/payments/sale/4EU7004268015634R","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/payments/sale/4EU7004268015634R/refund","rel":"refund","method":"POST"},{"href":"https://api.sandbox.paypal.com/v1/payments/payment/PAY-86C81811X5228590KKVNARQQ","rel":"parent_payment","method":"GET"}]},"links":[{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-0G2756385H040842W-5Y612302CV158622M","rel":"self","method":"GET"},{"href":"https://api.sandbox.paypal.com/v1/notifications/webhooks-events/WH-0G2756385H040842W-5Y612302CV158622M/resend","rel":"resend","method":"POST"}]}'
    # Paypal-Transmission-Id in webhook payload header
    transmission_id = "dfb3be50-fd74-11e4-8bf3-77339302725b"
    # Paypal-Transmission-Time in webhook payload header
    timestamp = "2015-05-18T15:45:13Z"
    # Webhook id created
    webhook_id = "4JH86294D6297924G"
    # Paypal-Transmission-Sig in webhook payload header
    actual_signature = "thy4/U002quzxFavHPwbfJGcc46E8rc5jzgyeafWm5mICTBdY/8rl7WJpn8JA0GKA+oDTPsSruqusw+XXg5RLAP7ip53Euh9Xu3UbUhQFX7UgwzE2FeYoY6lyRMiiiQLzy9BvHfIzNIVhPad4KnC339dr6y2l+mN8ALgI4GCdIh3/SoJO5wE64Bh/ueWtt8EVuvsvXfda2Le5a2TrOI9vLEzsm9GS79hAR/5oLexNz8UiZr045Mr5ObroH4w4oNfmkTaDk9Rj0G19uvISs5QzgmBpauKr7Nw++JI0pr/v5mFctQkoWJSGfBGzPRXawrvIIVHQ9Wer48GR2g9ZiApWg=="
    # Paypal-Cert-Url in webhook payload header
    cert_url = 'https://api.sandbox.paypal.com/v1/notifications/certs/CERT-360caa42-fca2a594-a5cafa77'
    # PayPal-Auth-Algo in webhook payload header
    auth_algo = 'sha256'

    response = WebhookEvent.verify(transmission_id, timestamp, webhook_id, event_body, cert_url, actual_signature,
                                   auth_algo)
    print(response)


def invoice_cancel():
    invoice = Invoice.find("INV2-CJL7-PF4G-BLQF-5FWG")
    options = {
        "subject": "Past due",
        "note": "Canceling invoice",
        "send_to_merchant": True,
        "send_to_payer": True
    }

    if invoice.cancel(options):  # return True or False
        print("Invoice[%s] cancel successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_create():
    invoice = Invoice({
        "merchant_info": {
            "email": "PPX.DevNet-facilitator@gmail.com",
            "first_name": "Dennis",
            "last_name": "Doctor",
            "business_name": "Medical Professionals, LLC",
            "phone": {
                "country_code": "001",
                "national_number": "5032141716"
            },
            "address": {
                "line1": "1234 Main St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97217",
                "country_code": "US"
            }
        },
        "billing_info": [{"email": "example@example.com"}],
        "items": [
            {
                "name": "Sutures",
                "quantity": 100,
                "unit_price": {
                    "currency": "USD",
                    "value": 5
                }
            }
        ],
        "note": "Medical Invoice 16 Jul, 2013 PST",
        "payment_term": {
            "term_type": "NET_45"
        },
        "shipping_info": {
            "first_name": "Sally",
            "last_name": "Patient",
            "business_name": "Not applicable",
            "phone": {
                "country_code": "001",
                "national_number": "5039871234"
            },
            "address": {
                "line1": "1234 Broad St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97216",
                "country_code": "US"
            }
        }
    })

    if invoice.create():
        print("Invoice[%s] created successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_get():
    try:
        invoice = Invoice.find("INV2-9DRB-YTHU-2V9Q-7Q24")
        print("Got Invoice Details for Invoice[%s]" % invoice.id)

    except ResourceNotFound as error:
        print("Invoice Not Found")


def invoice_get_all():
    history = Invoice.all({"page_size": 2})

    print("List Invoice:")
    for invoice in history.invoices:
        print("  -> Invoice[%s]" % invoice.id)


def invoice_get_qr_code():
    invoice_id = "INV2-EM7V-GTSP-7UTG-Y2MK"

    try:
        invoice = Invoice.find(invoice_id)
        height = "400"
        width = "400"

        rv = invoice.get_qr_code(height, width)
        print(rv)

    except ResourceNotFound as error:
        print("Invoice Not Found")


def invoice_record_payment():
    # Example demonstrating creating an invoice, sending it
    # (which changes its status from draft to sent)
    # and recording a payment on the send invoice
    invoice = Invoice({
        "merchant_info": {
            "email": "PPX.DevNet-facilitator@gmail.com",
            "first_name": "Dennis",
            "last_name": "Doctor",
            "business_name": "Medical Professionals, LLC",
            "phone": {
                "country_code": "001",
                "national_number": "5032141716"
            },
            "address": {
                "line1": "1234 Main St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97217",
                "country_code": "US"
            }
        },
        "billing_info": [{"email": "example@example.com"}],
        "items": [
            {
                "name": "Sutures",
                "quantity": 100,
                "unit_price": {
                    "currency": "USD",
                    "value": 5
                }
            }
        ],
        "note": "Medical Invoice 16 Jul, 2013 PST",
        "payment_term": {
            "term_type": "NET_45"
        },
        "shipping_info": {
            "first_name": "Sally",
            "last_name": "Patient",
            "business_name": "Not applicable",
            "phone": {
                "country_code": "001",
                "national_number": "5039871234"
            },
            "address": {
                "line1": "1234 Broad St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97216",
                "country_code": "US"
            }
        }
    })

    if invoice.create():
        print("Invoice[%s] created successfully" % invoice.id)
    else:
        print(invoice.error)

    if invoice.send():  # return True or False
        print("Invoice[%s] send successfully" % invoice.id)
    else:
        print(invoice.error)

    payment_attr = {
        "method": "CASH",
        "date": "2014-07-10 03:30:00 PST",
        "note": "Cash received."
    }

    if invoice.record_payment(payment_attr):  # return True or False
        print("Payment record on Invoice[%s] successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_record_refund():
    invoice = Invoice.find("<INVOICE_ID>")
    refund_attr = {
        "date": "2014-07-06 03:30:00 PST",
        "note": "Refund provided by cash."
    }

    if invoice.record_refund(refund_attr):  # return True or False
        print("Payment record on Invoice[%s] successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_remind():
    invoice = Invoice.find("INV2-9CAH-K5G7-2JPL-G4B4")
    options = {
        "subject": "Past due",
        "note": "Please pay soon",
        "send_to_merchant": True
    }

    if invoice.remind(options):  # return True or False
        print("Invoice[%s] remind successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_send():
    invoice = Invoice.find("INV2-9DRB-YTHU-2V9Q-7Q24")

    if invoice.send():  # return True or False
        print("Invoice[%s] send successfully" % invoice.id)
    else:
        print(invoice.error)


def invoice_third_party_invoicing():
    # Using Log In with PayPal, third party merchants can authorize your application
    # to create and submit invoices on their behalf.
    # See https://developer.paypal.com/docs/integration/direct/identity/log-in-with-paypal/
    # for more details about Log In with PayPal.


    # Step 1. Generate a Log In with PayPal authorization URL. The third party merchant will need to visit this URL
    # and authorize your request. You should use the `openid`, `https://uri.paypal.com/services/invoicing`,
    # and `email` scopes at the minimum.
    # paypalrestsdk.configure({'openid_client_id': 'CLIENT_ID', 'openid_client_secret': 'CLIENT_SECRET', 'openid_redirect_uri': 'http://example.com'})
    # login_url = Tokeninfo.authorize_url({'scope': 'openid profile'})

    # For example, the URL to redirect the third party merchant may be like:
    # https://www.sandbox.paypal.com/webapps/auth/protocol/openidconnect/v1/authorize?client_id=AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS&scope=openid%20https%3A%2F%2Furi.paypal.com%2Fservices%2Finvoicing%20email&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fpaypal%2FPayPal-PHP-SDK%2Fsample%2Flipp%2FUserConsentRedirect.php%3Fsuccess%3Dtrue

    # Step 2. After the third party merchant authorizes your request, Log In with PayPal will redirect the browser
    # to your configured openid_redirect_uri with an authorization code query parameter value.

    # For example, after the merchant successfully authorizes your request, they will be redirected
    # to the merchant's website configured via `openid_redirect_uri` like:
    # http://localhost/paypal/PayPal-Python-SDK/sample/UserConsentRedirect.php?success=true&scope=https%3A%2F%2Furi.paypal.com%2Fservices%2Finvoicing+openid+email&code=q6DV3YTMUDquwMQ88I7VyE_Ou1ksKcKVo1b_a8zqUQ7fGCxLoLajHIBROQ5yxcM0dNWVWPQaovH35dJmdmunNYrNKao8UlpY5LVMuTgnxZ6ce6UL3XuEd3JXuuPtdg-4feXyMA25oh2aM5o5HTtSeSG1Ag596q9tk90dfJQ8gxhFTw1bV

    # Step 3. Your web app should parse the query parameters for the authorization `code` value.
    # You should use the `code` value to get a refresh token and securely save it for later use.
    # begin
    # authorization_code = "q6DV3YTMUDquwMQ88I7VyE_Ou1ksKcKVo1b_a8zqUQ7fGCxLoLajHIBROQ5yxcM0dNWVWPQaovH35dJmdmunNYrNKao8UlpY5LVMuTgnxZ6ce6UL3XuEd3JXuuPtdg-4feXyMA25oh2aM5o5HTtSeSG1Ag596q9tk90dfJQ8gxhFTw1bV"
    # tokeninfo = Tokeninfo.create(code)
    # print(tokeninfo)
    # # Response 200
    # tokeninfo = {
    # token_type: 'Bearer',
    # expires_in: '28800',
    # refresh_token: 'J5yFACP3Y5dqdWCdN3o9lNYz0XyR01IHNMQn-E4r6Ss38rqbQ1C4rC6PSBhJvB_tte4WZsWe8ealMl-U_GMSz30dIkKaovgN41Xf8Sz0EGU55da6tST5I6sg3Rw',
    # id_token: '<some value>',
    # access_token: '<some value>'
    # }
    # end

    # Step 4. You can use the refresh token to get the third party merchant's email address to use in the invoice,
    # and then you can create an invoice on behalf of the third party merchant.
    refresh_token = "J5yFACP3Y5dqdWCdN3o9lNYz0XyR01IHNMQn-E4r6Ss38rqbQ1C4rC6PSBhJvB_tte4WZsWe8ealMl-U_GMSz30dIkKaovgN41Xf8Sz0EGU55da6tST5I6sg3Rw"

    token_info = Tokeninfo.create_with_refresh_token(refresh_token)
    print(token_info)

    user_info = token_info.userinfo()
    print(user_info)

    invoice = Invoice({
        "merchant_info": {
            # The email address used here would be of a third party.
            "email": user_info.email,
            "first_name": "Dennis",
            "last_name": "Doctor",
            "business_name": "Medical Professionals, LLC",
            "phone": {
                "country_code": "001",
                "national_number": "5032141716"
            },
            "address": {
                "line1": "1234 Main St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97217",
                "country_code": "US"
            }
        },
        "billing_info": [{"email": "example@example.com"}],
        "items": [
            {
                "name": "Sutures",
                "quantity": 50,
                "unit_price": {
                    "currency": "USD",
                    "value": 5
                }
            }
        ],
        "note": "Medical Invoice 16 Jul, 2013 PST",
        "payment_term": {
            "term_type": "NET_45"
        },
        "shipping_info": {
            "first_name": "Sally",
            "last_name": "Patient",
            "business_name": "Not applicable",
            "phone": {
                "country_code": "001",
                "national_number": "5039871234"
            },
            "address": {
                "line1": "1234 Broad St.",
                "city": "Portland",
                "state": "OR",
                "postal_code": "97216",
                "country_code": "US"
            }
        }
    })

    if invoice.create(refresh_token):
        print("Third Party Invoice[%s] created successfully" % invoice.id)

        # Fetch the resource similarly as shown below:
        result = Invoice.find(invoice.id, refresh_token=refresh_token)
        print("Invoice Detail: %s" % result)
    else:
        print(invoice.error)


def credit_card_create():
    # CreateCreditCard Sample
    # Using the 'vault' API, you can store a
    # Credit Card securely on PayPal. You can
    # use a saved Credit Card to process
    # a payment in the future.
    # The following code demonstrates how
    # can save a Credit Card on PayPal using
    # the Vault API.
    # API used: POST /v1/vault/credit-card
    credit_card = CreditCard({
        # CreditCard
        # A resource representing a credit card that can be
        # used to fund a payment.
        "type": "visa",
        "number": "4417119669820331",
        "expire_month": "11",
        "expire_year": "2018",
        "cvv2": "874",
        "first_name": "Joe",
        "last_name": "Shopper",

        # Address
        # Base Address object used as shipping or billing
        # address in a payment. [Optional]
        "billing_address": {
            "line1": "52 N Main ST",
            "city": "Johnstown",
            "state": "OH",
            "postal_code": "43210",
            "country_code": "US"}
    })

    # Make API call & get response status
    # Save
    # Creates the credit card as a resource
    # in the PayPal vault.
    if credit_card.create():
        print("CreditCard[%s] created successfully" % credit_card.id)
    else:
        print("Error while creating CreditCard:")
        print(credit_card.error)


def credit_card_delete():
    credit_card = CreditCard.find("CARD-7LT50814996943336KESEVWA")

    if credit_card.delete():
        print("CreditCard deleted")
    else:
        print(credit_card.error)


def credit_card_find():
    # GetCreditCard Sample
    # This sample code demonstrates how you
    # retrieve a previously saved
    # Credit Card using the 'vault' API.
    # API used: GET /v1/vault/credit-card/{id}
    try:
        # Retrieve the CreditCard  by calling the
        # static `find` method on the CreditCard class,
        # and pass CreditCard ID
        credit_card = CreditCard.find("CARD-5BT058015C739554AKE2GCEI")
        print("Got CreditCard[%s]" % credit_card.id)

    except ResourceNotFound as error:
        print("CreditCard Not Found")


def credit_card_replace():
    try:
        credit_card = CreditCard.find("CARD-5U686097RY597093SKPL7UNI")
        print("Got CreditCard[%s]" % credit_card.id)

        credit_card_update_attributes = [
            {
                "op": "replace",
                "path": "/first_name",
                "value": "Billy"
            }
        ]

        if credit_card.replace(credit_card_update_attributes):
            print("Card [%s] first name changed to %s" %
                  (credit_card.id, credit_card.first_name))
        else:
            print(credit_card.error)

    except ResourceNotFound as error:
        print("Billing Plan Not Found")


def capture_find():
    try:
        capture = Capture.find("8F148933LY9388354")
        print("Got Capture details for Capture[%s]" % capture.id)

    except ResourceNotFound as error:
        print("Capture Not Found")


def capture_refund():
    capture = Capture.find("8F148933LY9388354")
    refund = capture.refund({
        "amount": {
            "currency": "USD",
            "total": "110.54"}})

    if refund.success():
        print("Refund[%s] Success" % refund.id)
    else:
        print("Unable to Refund")
        print(refund.error)


def authorization_capture():
    authorization = Authorization.find("5RA45624N3531924N")
    capture = authorization.capture({
        "amount": {
            "currency": "USD",
            "total": "4.54"},
        "is_final_capture": True})

    if capture.success():
        print("Capture[%s] successfully" % capture.id)
    else:
        print(capture.error)


def authorization_find():
    try:
        authorization = Authorization.find("99M58264FG144833V")
        print("Got Authorization details for Authorization[%s]" % authorization.id)

    except ResourceNotFound as error:
        print("Authorization Not Found")


def authorization_reauthorize():
    authorization = Authorization.find("7GH53639GA425732B")

    authorization.amount = {
        "currency": "USD",
        "total": "7.00"}

    if authorization.reauthorize():
        print("Reauthorized[%s] successfully" % authorization.id)
    else:
        print(authorization.error)


def authorization_void():
    authorization = Authorization.find("6CR34526N64144512")

    if authorization.void():
        print("Void authorization successfully")
    else:
        print(authorization.error)

