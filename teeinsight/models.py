from datetime import timedelta
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.text import slugify
from teeinsight.fields import *
from teeinsight.utils import add_months


class Site(models.Model):
    name = models.CharField(db_index=True, max_length=1024)
    slug = models.SlugField(db_index=True, max_length=256, default='')

    url = models.URLField(max_length=4096, default='')
    icon = models.URLField(max_length=4096, default='')

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    is_deleted = models.BooleanField(db_index=True, default=False)

    class Meta:
        ordering = ['name']

    def __str__(self):
        if self.is_deleted:
            return '-' + self.name
        return self.name

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(Site, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            # make sure we have a slug first
            if not len(self.slug.strip()):
                self.slug = slugify(self.name)

            self.slug = self.get_unique_slug(self.slug)
            return True

        return False

    def get_unique_slug(self, slug):
        """
        Iterates until a unique slug is found
        """
        orig_slug = slug
        counter = 1

        while True:
            site = Site.objects.filter(slug=slug)
            if not site.exists():
                return slug

            slug = '%s-%s' % (orig_slug, counter)
            counter += 1


class Campaign(models.Model):
    title = models.CharField(max_length=2048, default='')
    slug = models.SlugField(db_index=True, max_length=1024, default='')
    link = models.URLField(db_index=True, max_length=2048, null=True)
    campaign_link = models.URLField(db_index=True, max_length=4096, null=True)
    description = models.TextField(null=True)

    seller = models.CharField(max_length=1024, null=True)

    site = models.ForeignKey(Site, related_name='campaigns_on_site', db_index=True)
    language = LanguageField(db_index=True, null=True)
    status = models.IntegerField(db_index=True, choices=CAMPAIGN_STATUSES, default=CAMPAIGN_UNDEFINED)
    front_price = models.FloatField(default=0.0)
    front_style = models.CharField(max_length=1024, null=True)
    front_color = models.CharField(max_length=1024, null=True)
    currency = models.CharField(max_length=16, null=True)
    total_sales = models.IntegerField(db_index=True, default=None, null=True)
    goal = models.IntegerField(default=None, null=True)
    ratio = models.FloatField(default=None, null=True)
    times_seen = models.IntegerField(default=None, null=True)
    ending_at = models.DateTimeField(db_index=True, default=timezone.now() + timedelta(days=30), null=True)
    added_at = models.DateTimeField(db_index=True, default=timezone.now)

    times_ran = models.IntegerField(default=None, null=True)
    tipped = models.NullBooleanField(default=None)

    front_image = models.URLField(max_length=2048, null=True)
    back_image = models.URLField(max_length=2048, null=True)
    styles = models.TextField(null=True)
    categories = models.TextField(null=True)

    facebook = models.URLField(max_length=2048, null=True)
    twitter = models.URLField(max_length=2048, null=True)
    google = models.URLField(max_length=2048, null=True)
    pinterest = models.URLField(max_length=2048, null=True)

    campaign_id = models.CharField(db_index=True, max_length=1024, null=True)
    referral_id = models.CharField(db_index=True, max_length=1024, null=True)
    referral_platform = models.CharField(db_index=True, max_length=512, null=True)
    reference = models.TextField(null=True)

    updated_at = models.DateTimeField(db_index=True, default=timezone.now)
    created_at = models.DateTimeField(db_index=True, default=timezone.now)

    is_deleted = models.BooleanField(db_index=True, default=False)

    class Meta:
        ordering = ['-added_at']

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.do_unique_slug()
        super(Campaign, self).save(*args, **kwargs)

    def do_unique_slug(self):
        """
        Ensures that the slug is always unique for this post
        """
        if not self.id:
            # make sure we have a slug first
            if not len(self.slug.strip()):
                self.slug = slugify(self.title)

            self.slug = self.get_unique_slug(self.slug)
            return True

        return False

    def get_unique_slug(self, slug):
        """
        Iterates until a unique slug is found
        """
        orig_slug = slug
        counter = 1

        while True:
            campaign = Campaign.objects.filter(slug=slug)
            if not campaign.exists():
                return slug

            slug = '%s-%s' % (orig_slug, counter)
            counter += 1

    def sales_total(self):
        sales_records_not_none = self.sales_records.filter(~Q(sales=None))
        if sales_records_not_none:
            sales_records_newest = sales_records_not_none.order_by('-recorded_at').first()
            return sales_records_newest.sales
        return -1

    def sales_hist(self):
        sales_records_not_none = self.sales_records.filter(~Q(sales=None))
        if sales_records_not_none:
            sales_records_newest = sales_records_not_none.order_by('recorded_at')[:10]
            return sales_records_newest.values_list('sales', flat=True)
        return []

    def sales_over_period(self, start_date=None, end_date=None):
        if start_date and end_date and self.sales_records.exists():
            sales_records_not_none = self.sales_records.filter(~Q(sales=None))
            sales_records_newest = sales_records_not_none.order_by('-recorded_at').first()
            sales_records_over_period = sales_records_not_none.filter(Q(recorded_at__range=(start_date, end_date)))
            if sales_records_over_period:
                sales_records_over_period_oldest = sales_records_over_period.order_by('recorded_at').first()
                return sales_records_newest.sales - sales_records_over_period_oldest.sales
        return -1

    def sales_over_hours(self, hours=0):
        start_date = timezone.now() - timedelta(hours=hours)
        end_date = timezone.now()
        return self.sales_over_period(start_date, end_date)

    def sales_over_days(self, days=0):
        start_date = timezone.now() - timedelta(days=days - 1)
        start_date = start_date.replace(hour=0, minute=0, second=0, microsecond=0)
        end_date = timezone.now()
        return self.sales_over_period(start_date, end_date)

    def sales_now(self):
        return self.sales_over_days(1)

    def sales_6_hours(self):
        return self.sales_over_hours(6)

    def sales_12_hours(self):
        return self.sales_over_hours(12)

    def sales_24_hours(self):
        return self.sales_over_hours(24)

    def sales_3_days(self):
        return self.sales_over_days(3)

    def sales_7_days(self):
        return self.sales_over_days(7)

    def sales_14_days(self):
        return self.sales_over_days(14)

    def sales_30_days(self):
        return self.sales_over_days(30)

    def engagement_total(self):
        engagement_records_not_none = self.engagement_metrics.filter(~Q(engagement=None))
        if engagement_records_not_none:
            engagement_records_newest = engagement_records_not_none.order_by('-created_at').first()
            return engagement_records_newest.engagement
        return -1

    def engagement_hist(self):
        engagement_records_not_none = self.engagement_metrics.filter(~Q(engagement=None))
        if engagement_records_not_none:
            engagement_records_newest = engagement_records_not_none.order_by('created_at')[:10]
            return engagement_records_newest.values_list('engagement', flat=True)
        return []

    def engagement_over_period(self, start_date=None, end_date=None):
        if start_date and end_date and self.engagement_metrics.exists():
            engagement_records_not_none = self.engagement_metrics.filter(~Q(engagement=None))
            engagement_records_newest = engagement_records_not_none.order_by('-created_at').first()
            engagement_records_over_period = engagement_records_not_none.filter(
                Q(created_at__range=(start_date, end_date)))
            if engagement_records_over_period:
                engagement_records_over_period_oldest = engagement_records_over_period.order_by('created_at').first()
                return engagement_records_newest.engagement - engagement_records_over_period_oldest.engagement
        return -1

    def engagement_over_hours(self, hours=0):
        start_date = timezone.now() - timedelta(hours=hours)
        end_date = timezone.now()
        return self.engagement_over_period(start_date, end_date)

    def engagement_over_days(self, days=0):
        start_date = timezone.now() - timedelta(days=days)
        start_date = start_date.date()
        end_date = timezone.now()
        return self.engagement_over_period(start_date, end_date)

    def engagement_now(self):
        return self.engagement_over_days(1)

    def engagement_6_hours(self):
        return self.engagement_over_hours(6)

    def engagement_12_hours(self):
        return self.engagement_over_hours(12)

    def engagement_24_hours(self):
        return self.engagement_over_hours(24)

    def engagement_3_days(self):
        return self.engagement_over_days(3)

    def engagement_7_days(self):
        return self.engagement_over_days(7)

    def engagement_14_days(self):
        return self.engagement_over_days(14)

    def engagement_30_days(self):
        return self.engagement_over_days(30)

    def fb_likes(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_likes
        return -1

    def fb_likes_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('fb_likes', flat=True)
        return []

    def fb_total_per_sale(self):
        if self.social_metrics.exists() and self.sales_total() and self.fb_likes():
            try:
                return self.fb_likes() / self.sales_total()
            except EnvironmentError:
                return -1
        return -1

    def fb_shares(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_shares
        return -1

    def fb_shares_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('fb_shares', flat=True)
        return []

    def fb_comments(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_comments
        return -1

    def fb_comments_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('fb_comments', flat=True)
        return []

    def twitter_tweets(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().twitter_tweets
        return -1

    def twitter_tweets_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('twitter_tweets', flat=True)
        return []

    def google_pluses(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().google_pluses
        return -1

    def google_pluses_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('google_pluses', flat=True)
        return []

    def pinterest_pins(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().pinterest_pins
        return -1

    def pinterest_pins_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('pinterest_pins', flat=True)
        return []

    def linkedin_shares(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().linkedin_shares
        return -1

    def linkedin_shares_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('linkedin_shares', flat=True)
        return []

    def stumbleupon_stumbles(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().stumbleupon_stumbles
        return -1

    def stumbleupon_stumbles_hist(self):
        if self.social_metrics.exists():
            return self.social_metrics.order_by('created_at')[:10].values_list('stumbleupon_stumbles', flat=True)
        return []

    def fb_likes_trend(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_likes_trend
        return -1

    def fb_comments_trend(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_comments_trend
        return -1

    def fb_shares_trend(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().fb_shares_trend
        return -1

    def tweets_trend(self):
        if self.social_metrics.exists():
            return self.social_metrics.first().tweets_trend
        return -1

    def metrics(self):
        if self.social_metrics.exists():
            return self.social_metrics.first()
        return None


class Sales(models.Model):
    campaign = models.ForeignKey(Campaign, related_name='sales_records', db_index=True)
    sales = models.IntegerField(db_index=True, null=True)
    recorded_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(db_index=True, default=timezone.now)

    def __str__(self):
        return self.campaign.title + ' - ' + str(self.sales)

    class Meta:
        ordering = ['-recorded_at']


class Engagement(models.Model):
    campaign = models.ForeignKey(Campaign, related_name='engagement_metrics', db_index=True)
    engagement = models.IntegerField(db_index=True, default=0)
    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(db_index=True, default=timezone.now)

    def __str__(self):
        return str(self.engagement)

    class Meta:
        ordering = ['-created_at']


class SocialMetrics(models.Model):
    campaign = models.ForeignKey(Campaign, related_name='social_metrics', db_index=True)
    fb_likes = models.IntegerField(db_index=True, null=True)
    fb_shares = models.IntegerField(db_index=True, null=True)
    fb_comments = models.IntegerField(db_index=True, null=True)
    twitter_tweets = models.IntegerField(db_index=True, null=True)
    google_pluses = models.IntegerField(db_index=True, null=True)
    pinterest_pins = models.IntegerField(db_index=True, null=True)
    linkedin_shares = models.IntegerField(db_index=True, null=True)
    stumbleupon_stumbles = models.IntegerField(db_index=True, null=True)
    fb_likes_trend = models.IntegerField(db_index=True, null=True)
    fb_shares_trend = models.IntegerField(db_index=True, null=True)
    fb_comments_trend = models.IntegerField(db_index=True, null=True)
    tweets_trend = models.IntegerField(db_index=True, null=True)
    created_at = models.DateTimeField(db_index=True, default=timezone.now)
    updated_at = models.DateTimeField(db_index=True, default=timezone.now)

    def __str__(self):
        return self.campaign.title

    class Meta:
        ordering = ['-created_at']


class SubscribedPackages(models.Model):
    name = models.CharField(max_length=1024)
    price = models.FloatField(default=0.0)
    description = models.TextField(null=True)
    features = models.CharField(max_length=2048, null=True)
    updated_at = models.DateTimeField(default=timezone.now)
    created_at = models.DateTimeField(default=timezone.now)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['-price']

    def __str__(self):
        return self.name


class Subscription(models.Model):
    user = models.ForeignKey(User, related_name='user_subscription')
    package = models.ForeignKey(SubscribedPackages, related_name='package_subscription')
    payment_opt = models.CharField(max_length=1024)
    started_at = models.DateTimeField(default=timezone.now)
    ending_at = models.DateTimeField(default=timezone.now() + timedelta(days=3))
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    tx = models.CharField(max_length=1024, null=True)
    tx_response = models.TextField(null=True)
    subscr_id = models.CharField(max_length=1024, null=True)
    is_verified = models.BooleanField(default=False)
    payer_email = models.EmailField(null=True)

    class Meta:
        ordering = ['-updated_at']

    def __str__(self):
        return "{} - {} [{:%Y-%m-%d %H:%M} - {:%Y-%m-%d %H:%M}]" \
            .format(self.user.username, self.package.name, self.started_at, self.ending_at)


# class Statistics(models.Model):
#     name = models.CharField(max_length=1024)
#     value = models.IntegerField(default=0)
#     value_float = models.FloatField(default=0.0)
#     created_at = models.DateTimeField(default=timezone.now)
#     updated_at = models.DateTimeField(default=timezone.now)
#
#     class Meta:
#         ordering = ['-updated_at']
#
#     def __str__(self):
#         return self.value_float


class SubscriptionAUp(models.Model):
    name = models.CharField(max_length=1024)
    phone = models.CharField(max_length=32, null=True)
    email = models.EmailField(null=True)
    emails_file = models.URLField(max_length=2048, null=True)
    pack_name = models.CharField(max_length=64)
    platforms = models.CharField(max_length=1024)
    licenses = models.IntegerField(default=1)
    discount = models.BooleanField(default=False)
    total = models.FloatField(default=0.0)
    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)

    message = models.CharField(max_length=1024, null=True)
    emails = models.TextField(null=True)
    license_keys = models.TextField(null=True)
    keys_file = models.URLField(max_length=2048, null=True)
    is_deleted = models.BooleanField(default=False)
    facebook_id = models.CharField(max_length=64, null=True)

    class Meta:
        ordering = ['-updated_at']

    def __str__(self):
        return "{} - {} - {}".format(self.pack_name, self.pack_name, self.total)


