import os
from django.http import QueryDict
from django.views.decorators.http import require_POST
import requests
import urllib.parse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q, Count, Sum
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
import paypalrestsdk
from paypalrestsdk import BillingPlan, BillingAgreement, ResourceNotFound
from rest_framework.response import Response
from rest_framework.views import APIView
from teeinsight.decorators import superuser_login_required
from teeinsight.serializers import *
from teeinsight.utils import *
from datetime import datetime

# Initialize PayPal sdk
paypalrestsdk.configure({
    'mode': 'sandbox',  # sandbox or live
    'client_id': settings.PAYPAL_CLIENT_ID,
    'client_secret': settings.PAYPAL_CLIENT_SECRET,
    # 'openid_redirect_uri': 'http://teeinsight.com/packages/'
})


# handler err-page
def teeinsight_page_not_found(request):
    data_instance = {
        'title': pre_title + '404' + suf_title,
        'active': ['404'],
        'request_path': request.path
    }
    response = render(request, '404.html', data_instance)
    response.status_code = 404
    return response


def teeinsight_error(request):
    data_instance = {
        'title': pre_title + '500' + suf_title,
        'active': ['500']
    }
    response = render(request, '500.html', data_instance)
    response.status_code = 500
    return response


def teeinsight_permission_denied(request):
    data_instance = {
        'title': pre_title + '403' + suf_title,
        'active': ['500']
    }
    response = render(request, '403.html', data_instance)
    response.status_code = 403
    return response


def teeinsight_bad_request(request):
    data_instance = {
        'title': pre_title + '400' + suf_title,
        'active': ['400']
    }
    response = render(request, '400.html', data_instance)
    response.status_code = 400
    return response


# ##
# PAGEs
# ##
# campaign page
@superuser_login_required
def campaign(request, slug=None):
    if not slug:
        slug = ''
    data_instance = {
        'title': pre_title + 'Campaign ' + slug + suf_title,
        'active': ['campaign'],
        'next_url': '/campaign/',
        'campaign': {}
    }
    if slug:
        campaign_instance = get_object_or_404(Campaign, slug=slug)
        data_instance['next_url'] = '/campaign/' + slug + '/'
    else:
        return redirect('teeinsight_campaigns')
    data_instance["campaign"] = campaign_instance
    return render(request, 'teeinsight/campaign.html', data_instance)


# campaigns v2 (data table) page
@csrf_exempt
@superuser_login_required
def campaigns(request):
    params = request.GET
    table_title = 'Campaigns'

    data_instance = {
        'title': pre_title + table_title + suf_title,
        'table_title': table_title,
        'active': ['campaigns'],
        'next_url': '/campaigns/',
        'filter': {
            'search': params.get('search', None),
            'min_price': params.get('min_price', None),
            'min_sales': params.get('min_sales', None),
            'time_added': params.get('time_added', None),
            'time_end': params.get('time_end', None),
            'company': params.get('company', None),
            'status': params.get('status', None),
        },
        'order': {
            'order_by': params.get('order_by', 'added_at'),
            'asc_desc': params.get('asc_desc', 'desc')
        },
        'paginator': {
            'page': params.get('page', 1),
            'items_per_page': params.get('items_per_page', 25)
        },
        'sites': Site.objects.filter(Q(is_deleted=False))
    }

    return render(request, 'teeinsight/campaigns.html', data_instance)


# dashboard v2 page
@superuser_login_required
def dashboard(request):
    campaigns_count_added_today = 0

    campaigns_count_active = 0

    campaigns_count_total_campaigns = 0

    campaigns_count_total_sales = [0]

    data_instance = {
        'title': pre_title + 'Dashboard' + suf_title,
        'active': ['dashboard'],
        'next_url': '/dashboard/',
        'campaigns': {
            'added_today': campaigns_count_added_today,
            'active': campaigns_count_active,
            'total': campaigns_count_total_campaigns,
            'top_selling': sum(campaigns_count_total_sales)
        },
        'recently': [],
        'hot': [],
        'highest': []
    }
    return render(request, 'teeinsight/dashboard.html', data_instance)


# packages page
@superuser_login_required
def packages(request):
    params = request.GET

    packages_instance = SubscribedPackages.objects.filter(is_deleted=False)
    data_instance = {
        'title': pre_title + 'Packages' + suf_title,
        'active': ['packages'],
        'next_url': '/packages/',
        'packages': packages_instance,
    }

    if request.user.is_authenticated():
        data_instance['current_subscription'] = packages_instance.get(Q(price=0)).id
        user_subscriptions = request.user.user_subscription
        if user_subscriptions or user_subscriptions.count():
            today = timezone.now()
            user_ok_subscriptions = user_subscriptions.filter(
                Q(is_verified=True),
                Q(ending_at__gte=today),
                Q(package_id__in=packages_instance.values_list('id', flat=True)))
            user_now_subscriptions = user_ok_subscriptions.exclude(Q(started_at__gt=today)).order_by('updated_at', 'id')
            if user_now_subscriptions.count():
                current_subscription = user_now_subscriptions.first()
                data_instance['current_subscription'] = current_subscription.package_id
                data_instance['ending_at'] = current_subscription.ending_at
            user_next_subscriptions = user_ok_subscriptions.exclude(
                Q(id__in=user_now_subscriptions.values_list('id', flat=True))).order_by('updated_at', 'id')
            if user_next_subscriptions.count():
                next_subscription = user_next_subscriptions.first()
                data_instance['next_subscription'] = next_subscription.package_id
    return render(request, 'teeinsight/packages.html', data_instance)


@login_required
def packages_pdt_listener(request):
    params = request.GET

    data_instance = {
        'title': 'Payment Data Transfer Listener',
        'active': ['packages', 'pdt', 'listener'],
        'transaction': {},
        'tx_response': {}
    }

    tx = params.get('tx', None)  # Paypal transaction ID
    if tx:
        # st = params.get('st', None)  # Paypal product status
        # amt = params.get('amt', None)  # Paypal received amount value
        # cc = params.get('cc', None)  # Paypal received currency type
        # cm = params.get('cm', None)
        # item_number = params.get('item_number', None)
        # sig = params.get('sig', None)
        # item_number = params.get('item_number', None)  # Product ID

        r = requests.post(
            "https://www.sandbox.paypal.com/cgi-bin/webscr",
            data={
                'cmd': '_notify-synch',
                'tx': tx,
                'at': 'SXGfPQnu53ZnpbPYWl25T9dSvTPq3ZMHOxvJc8IgdfGdLGAdAFsNoaVPoEG'
            }
        )
        tx_array = urllib.parse.unquote_plus('status=' + r.text).split('\n')
        tx_response = dict([m.groups()
                            for l in tx_array
                            for m in [re.search('([\w_]+)=(.*)', l), re.search('(Error):(.*)', l)] if m])
        # tx_response_exp = {
        #     "status": "SUCCESS",
        #     "protection_eligibility": "Ineligible",
        #     "residence_country": "US",
        #     "charset": "windows-1252",
        #
        #     "receiver_id": "PPX2ACUVHGE9S",
        #     "receiver_email": "itracker.family-facilitator@gmail.com",
        #     "business": "itracker.family-facilitator@gmail.com",
        #
        #     "payer_id": "2JPZMVEECZZWY",
        #     "first_name": "test",
        #     "last_name": "buyer",
        #     "payer_status": "verified",
        #     "payer_email": "itracker.family-buyer@gmail.com",
        #
        #     "subscr_id": "I-D3VFD6554VM7",
        #     "txn_id": "72L51759Y8908131V",
        #     "txn_type": "subscr_payment",
        #     "transaction_subject": "TeeInsight-Basic",
        #     "item_name": "TeeInsight-Basic",
        #     "option_name1": "Payment Options",
        #     "option_selection1": "1 Month",
        #
        #     "payment_date": "01:25:06 Sep 01, 2016 PDT",
        #     "payment_status": "Completed",
        #     "payment_type": "instant",
        #     "payment_gross": "50.00",
        #     "payment_fee": "1.75",
        #     "mc_gross": "50.00",
        #     "mc_fee": "1.75",
        #     "mc_currency": "USD",
        # }
        data_instance['tx_response'] = tx_response

        status = tx_response.get('status')
        if 'SUCCESS' == status and tx == tx_response.get('txn_id'):
            pack_name = tx_response.get('item_name').split('-')[1]
            pack = SubscribedPackages.objects.filter(Q(name=pack_name)).first()

            payer_email = tx_response.get('payer_email')

            payment_opt = tx_response.get('option_selection1')
            period_val, period_key = re.search('([\d]+) (month|year|week|day)', payment_opt.lower()).groups()
            period_val = int(period_val)

            started_at = tx_response.get('payment_date')
            started_at = datetime.strptime(started_at, '%H:%M:%S %b %d, %Y PDT')
            user_subscriptions = request.user.user_subscription
            packages_instance = SubscribedPackages.objects.filter(is_deleted=False)
            if user_subscriptions or user_subscriptions.count():
                today = timezone.now()
                user_ok_subscriptions = user_subscriptions.filter(
                    Q(is_verified=True),
                    Q(ending_at__gte=today),
                    Q(package_id__in=packages_instance.values_list('id', flat=True)))
                last_ending_at = max(user_ok_subscriptions.values_list('ending_at', flat=True))
                started_at = max([last_ending_at, started_at])

            if period_key == 'week':
                ending_at = started_at + timedelta(weeks=period_val)
            elif period_key == 'day':
                ending_at = started_at + timedelta(days=period_val)
            elif period_key == 'year':
                ending_at = add_months(started_at, period_val * 12)
            elif period_key == 'month':
                ending_at = add_months(started_at, period_val)
            else:
                ending_at = started_at + timedelta(days=3)
            tx_response['ending_at'] = ending_at.strftime('%H:%M:%S %b %d, %Y PDT')
            data_instance['tx_response']['ending_at'] = ending_at.strftime('%H:%M:%S %b %d, %Y PDT')

            if request.user and pack:
                if Subscription.objects.filter(Q(tx=tx)).exists():
                    Subscription.objects.filter(Q(tx=tx)).update(
                        updated_at=timezone.now()
                    )
                else:
                    subscr_id = tx_response.get('subscr_id')
                    Subscription.objects.create(
                        user_id=request.user.id,
                        package=pack,
                        payment_opt=payment_opt,
                        started_at=started_at,
                        tx=tx,
                        tx_response=tx_response,
                        subscr_id=subscr_id,
                        ending_at=ending_at,
                        payer_email=payer_email
                    )
        elif 'FAIL' == status:
            if Subscription.objects.filter(Q(tx=tx)).exists():
                data_instance['tx_response'] = tx_response
        else:
            pass
    return render(request, 'teeinsight/packages_pdt_listener.html', data_instance)


CONTENT_TYPE_ERROR = ("Invalid Content-Type - PayPal is only expected to use "
                      "application/x-www-form-urlencoded. If using django's "
                      "test Client, set `content_type` explicitly")

DEFAULT_ENCODING = 'windows-1252'  # PayPal seems to normally use this.


@require_POST
@csrf_exempt
def packages_ipn_listener(request):
    verify_url = "https://www.sandbox.paypal.com/cgi-bin/webscr"

    # listener_file_name = 'ipn_listener.txt'
    # path = "/".join(['text'])
    # local_path = "/".join([settings.MEDIA_ROOT, path])
    # if not os.path.exists(local_path):
    #     os.makedirs(local_path)
    # licenses_file_path = "/".join([local_path, listener_file_name])
    # f_sites = open(licenses_file_path, 'a')
    # print('[' + timezone.now().strftime('%H:%M:%S %b %d, %Y PDT') + ']', file=f_sites)
    if not request.META.get('CONTENT_TYPE', '').startswith(
            'application/x-www-form-urlencoded'):
        raise AssertionError(CONTENT_TYPE_ERROR)

    encoding = request.POST.get('charset', None)

    encoding_missing = encoding is None
    if encoding_missing:
        encoding = DEFAULT_ENCODING

    try:
        data = QueryDict(request.body, encoding=encoding).copy()
    except LookupError:
        data = None
        # print('=====================================================', file=f_sites)
        # print('=====================================================', file=f_sites)
        # f_sites.close()
        return redirect('s_plans')

    # print('data', file=f_sites)
    # print(data, file=f_sites)

    # Post back to PayPal for validation
    data['cmd'] = '_notify-validate'
    headers = {'content-type': 'application/x-www-form-urlencoded', 'host': 'www.paypal.com'}
    r = requests.post(verify_url, params=data, headers=headers, verify=True)
    r.raise_for_status()

    # print('r.text', file=f_sites)
    # print(r.text, file=f_sites)

    # print('=====================================================', file=f_sites)
    # print('=====================================================', file=f_sites)
    # f_sites.close()

    # process data
    if r.text == 'VERIFIED':
        # rep = {
        #     'ipn_track_id': '7c5a86a669bf',
        #     'notify_version': '3.8',
        #     'test_ipn': '1',
        #     'resend': 'true',
        #     'verify_sign': 'ACUe-E7Hjxmeel8FjYAtjnx-yjHAANDjFHDkswhSk8gSl.8Ahh7wBhX6',
        #     'protection_eligibility': 'Ineligible',
        #     'residence_country': 'US',
        #     'charset': 'windows-1252',
        #
        #     'receiver_id': 'PPX2ACUVHGE9S',
        #     'receiver_email': 'itracker.family-facilitator@gmail.com',
        #     'business': 'itracker.family-facilitator@gmail.com',
        #
        #     'payer_id': 'M7LK9W2SRKWG8',
        #     'first_name': 'test',
        #     'last_name': 'buyer',
        #     'payer_status': 'verified',
        #     'payer_email': 'ntphat691-buyer@gmail.com',
        #
        #     'subscr_id': 'I-2HF8CSW1T21G',
        #     'txn_id': '02T18536ND031523W',
        #     'txn_type': 'subscr_payment',
        #     'transaction_subject': 'TeeInsight-Basic',
        #     'item_name': 'TeeInsight-Basic',
        #     'option_name1': 'Payment Options',
        #     'option_selection1': '1 Month',
        #
        #     'payment_date': '02:13:22 Oct 13, 2016 PDT',
        #     'payment_status': 'Completed',
        #     'payment_type': 'instant',
        #     'payment_gross': '50.00',
        #     'payment_fee': '1.75',
        #     'mc_gross': '50.00',
        #     'mc_fee': '1.75',
        #     'mc_currency': 'USD',
        # }
        is_verified = True
    elif r.text == 'INVALID':
        is_verified = False
    else:
        is_verified = False

    if data.get('txn_type', None) == 'subscr_payment':
        txn_id = data.get('txn_id', None)
        subscr_id = data.get('subscr_id', None)
        subscription = Subscription.objects.filter(Q(tx=txn_id), Q(subscr_id=subscr_id))
        if subscription.exists():
            subscription.update(
                is_verified=is_verified,
                updated_at=timezone.now()
            )

    data_instance = {
        'title': 'Instant Payment Notification (IPN) Listener',
        'active': ['packages', 'ipn', 'listener'],
        'data': dict(data)
    }

    # return render(request, 'teeinsight/packages_ipn_listener.html', data_instance)
    return redirect('s_plans')


# duplicate page
@superuser_login_required
def duplicate(request):
    data_instance = {
        'title': pre_title + 'Show Duplicate Campaigns' + suf_title,
        'active': ['duplicate'],
        'next_url': '/duplicate/',
    }
    return render(request, 'teeinsight/duplicate.html', data_instance)


# plans page
def get_billing_plans(status='ACTIVE', sort_order='DESC', page_size=10, page=0, total_required='yes'):
    """
    status: enum. Possible values: CREATED, ACTIVE, INACTIVE.
    """
    plans_query = BillingPlan.all({
        "status": status,
        "sort_order": sort_order,
        "page_size": page_size,
        "page": page,
        "total_required": total_required
    })
    plans_query_dict = plans_query.to_dict()
    plans_dict = plans_query_dict.get('plans', [])
    for plan in plans_dict:
        plan['full'] = BillingPlan.find(plan['id']).to_dict()

    return plans_query_dict, plans_dict


@superuser_login_required
def plans(request):
    data_instance = {
        'title': pre_title + 'S-Plans' + suf_title,
        'active': ['plans', 'sample'],
        'next_url': '/plans/',
    }

    plans_active_query_dict, plans_active = get_billing_plans(page_size=8)
    data_instance['plans_active_query_dict'] = plans_active_query_dict
    data_instance['plans_active'] = plans_active

    if request.user.is_authenticated() and request.user.is_superuser:
        plans_inactive_query_dict, plans_inactive = get_billing_plans(status='INACTIVE', page_size=8)
        data_instance['plans_inactive_query_dict'] = plans_inactive_query_dict
        data_instance['plans_inactive'] = plans_inactive

        plans_created_query_dict, plans_created = get_billing_plans(status='CREATED', page_size=8)
        data_instance['plans_created_query_dict'] = plans_created_query_dict
        data_instance['plans_created'] = plans_created

    return render(request, 'teeinsight/plans.html', data_instance)


def build_billing_plan_attributes(params):
    params_dict = dict(params)
    name = params.get('name', None)
    description = params.get('description', None)
    type = params.get('type', None)

    currency = params.get('currency', None)
    setup_fee = params.get('setup_fee', None)
    return_url = params.get('return_url', None)
    cancel_url = params.get('cancel_url', None)
    max_fail_attempts = params.get('max_fail_attempts', None)
    auto_bill_amount = params.get('auto_bill_amount', None)
    initial_fail_amount_action = params.get('initial_fail_amount_action', None)
    merchant_preferences = {
        'setup_fee': {
            'currency': currency,
            'value': setup_fee
        },
        'return_url': return_url,
        'cancel_url': cancel_url,
        'max_fail_attempts': max_fail_attempts,
        'auto_bill_amount': auto_bill_amount,
        'initial_fail_amount_action': initial_fail_amount_action
    }

    payment_definitions = []
    frequency_arr = params_dict.get('frequency', [])
    for frequency in frequency_arr:
        frequency = frequency.lower()
        payment_type_frequency = 'payment_type_' + frequency
        payment_type_frequency_arr = params_dict.get(payment_type_frequency, [])
        for frequency_type in payment_type_frequency_arr:
            frequency_type = frequency_type.lower()
            prefix = frequency + '_' + frequency_type + '_'
            payment_name = params.get(prefix + 'payment_name', None)
            frequency_interval = params.get(prefix + 'frequency_interval', None)
            cycles = params.get(prefix + 'cycles', None)
            amount = params.get(prefix + 'amount', None)
            shipping = params.get(prefix + 'shipping', None)
            tax = params.get(prefix + 'tax', None)
            payment_definition = {
                'name': payment_name,
                'type': frequency_type,
                'frequency': frequency,
                'frequency_interval': frequency_interval,
                'cycles': cycles,
                'amount': {
                    'currency': currency,
                    'value': amount
                },
                'charge_models': [
                    {
                        'type': 'SHIPPING',
                        'amount': {
                            'currency': currency,
                            'value': shipping
                        }
                    },
                    {
                        'type': 'TAX',
                        'amount': {
                            'currency': currency,
                            'value': tax
                        }
                    }
                ]
            }
            payment_definitions.append(payment_definition)
    billing_plan_attributes = {
        'name': name,
        'description': description,
        'type': type,
        'merchant_preferences': merchant_preferences,
        'payment_definitions': payment_definitions
    }

    return billing_plan_attributes


@superuser_login_required
@csrf_exempt
def create_plan(request):
    """
    Merchant creates new billing plan from input values in the form
    """
    data_instance = {
        'title': pre_title + 'S-Create Billing Plan' + suf_title,
        'active': ['plans', 'sample', 'create'],
        'next_url': '/plan/create/',
        'plan': {
            "name": '',
            "description": '',
            "type": "FIXED",
            "merchant_preferences": {
                "setup_fee": {
                    "value": "1",
                    "currency": "USD"
                },
                "return_url": '',
                "cancel_url": '',
                "auto_bill_amount": "YES",
                "initial_fail_amount_action": "CONTINUE",
                "max_fail_attempts": "0"
            },
            "payment_definitions": [
                {
                    "name": '',
                    "type": "REGULAR",
                    "frequency": "MONTH",
                    "frequency_interval": "2",
                    "amount": {
                        "value": "100",
                        "currency": "USD"
                    },
                    "cycles": "12",
                    "charge_models": [
                        {
                            "type": "SHIPPING",
                            "amount": {
                                "value": "10",
                                "currency": "USD"
                            }
                        },
                        {
                            "type": "TAX",
                            "amount": {
                                "value": "12",
                                "currency": "USD"
                            }
                        }
                    ]
                }
            ]
        },
        'payment_list': ['Month']
    }

    if request.method == 'POST':
        data_instance['pst'] = dict(request.POST)
        billing_plan_attributes = build_billing_plan_attributes(request.POST)
        data_instance['billing_plan_attributes'] = billing_plan_attributes
        billing_plan = BillingPlan(billing_plan_attributes)

        if billing_plan.create():
            msg = "Billing Plan [%s] created successfully" % billing_plan.id
            print(msg)
            return redirect('s_plans')
        else:
            msg = billing_plan.error
            print(msg)
            data_instance['message'] = msg

    return render(request, 'teeinsight/plans/create_plan.html', data_instance)


@superuser_login_required
@csrf_exempt
def update_plan(request, id=None):
    """
    Merchant creates new billing plan from input values in the form
    """
    data_instance = {
        'title': pre_title + 'S-Update Billing Plan' + suf_title,
        'active': ['plans', 'sample', 'update']
    }

    plan = BillingPlan.find(id)
    plan_dict = plan.to_dict()
    state = plan_dict.get('state', None)
    if state is not None and state != 'ACTIVE':
        data_instance['plan'] = plan_dict
        data_instance['payment_list'] = set_items_value_list(plan['payment_definitions'], 'frequency')
    else:
        return redirect('s_plans')

    if request.method == 'POST':
        data_instance['pst'] = dict(request.POST)
        billing_plan_attributes = build_billing_plan_attributes(request.POST)
        billing_plan_update_attributes = [
            {
                "op": 'replace',
                "path": "/",
                "value": billing_plan_attributes
            }
        ]
        data_instance['billing_plan_update_attributes'] = billing_plan_update_attributes
        if plan.replace(billing_plan_update_attributes):
            msg = "Billing Plan [%s] update successfully" % plan.id
            print(msg)
            return redirect('s_plans')
        else:
            msg = plan.error
            print(msg)
            data_instance['message'] = msg

    return render(request, 'teeinsight/plans/update_plan.html', data_instance)


def change_state(id=None, op='replace', state='INACTIVE'):
    """
    op enum. Possible values: add, remove, replace, move, copy, test.
    state: enum. Possible values: CREATED, ACTIVE, INACTIVE, DELETED.
    """
    if 'remove' == op:
        billing_plan_update_attributes = [
            {
                "op": 'remove',
                "path": "/",
            }
        ]
    else:
        billing_plan_update_attributes = [
            {
                "op": op,
                "path": "/",
                "value": {
                    "state": state
                }
            }
        ]

    try:
        billing_plan = BillingPlan.find(id)

        if 'ACTIVE' == state:
            if billing_plan.activate():
                return 0
            else:
                return billing_plan.error
        else:
            if billing_plan.replace(billing_plan_update_attributes):
                return 0
            else:
                return billing_plan.error

    except ResourceNotFound as error:
        return "Billing Plan Not Found"


@superuser_login_required
@csrf_exempt
def activate_plan(request, id=None):
    """
    Merchant activates plan after creation
    """
    if request.method == 'POST' and id:
        change_state(id=id, state='ACTIVE')
    return redirect('s_plans')


@superuser_login_required
@csrf_exempt
def inactivate_plan(request, id=None):
    """
    Merchant activates plan
    """
    if request.method == 'POST' and id:
        change_state(id=id, state='INACTIVE')
    return redirect('s_plans')


@superuser_login_required
@csrf_exempt
def delete_plan(request, id=None):
    """
    Merchant delete plan unused
    """
    if request.method == 'POST' and id:
        change_state(id=id, state='DELETED')
    return redirect('s_plans')


@csrf_exempt
def subscribe_plan(request, id=None):
    """
    Customer subscribes to a billing plan to form a billing agreement
    """
    billing_agreement = BillingAgreement({
        "name": "Teeinsight Campaign - " + request.GET.get('payment', ''),
        "description": "Agreement for " + request.GET.get('name', '') + "(" + request.GET.get('payment', '') + ")",
        "start_date": (datetime.now() + timedelta(hours=1)).strftime('%Y-%m-%dT%H:%M:%SZ'),
        "plan": {
            "id": id
        },
        "payer": {
            "payment_method": "paypal"
        }
    })
    if billing_agreement.create():
        for link in billing_agreement.links:
            if link.rel == "approval_url":
                approval_url = link.href
                return redirect(approval_url)
    else:
        print(billing_agreement.error)
    return redirect('s_plans')


def execute_agreement(request):
    """
    Customer redirected to this endpoint by PayPal after payment approval
    """
    payment_token = request.GET.get('token', '')
    billing_agreement_response = BillingAgreement.execute(payment_token)
    return redirect('s_agreement_details', id=billing_agreement_response.id)


def cancel_agreement(request):
    payment_token = request.GET.get('token', '')
    # billing_agreement_response = BillingAgreement.execute(payment_token)
    return redirect('s_plans')


@superuser_login_required
def agreement_details(request, id=None):
    data_instance = {
        'title': pre_title + 'S-Subscription Details' + suf_title,
        'active': ['plans', 'sample', 'agreement', 'details'],
    }
    billing_agreement = BillingAgreement.find(id)
    data_instance['agreement'] = billing_agreement
    data = {
        'id': 'I-SC0G3G60MKLB',
        'description': 'Agreement for Basic(Bas-Monthly)',
        'state': 'Active',
        'start_date': '2016-10-10T07:00:00Z',
        'agreement_details': {
            'outstanding_balance': {
                'currency': 'USD',
                'value': '0.00'
            },
            'cycles_remaining': '0',
            'last_payment_date': '2016-10-10T08:12:52Z',
            'last_payment_amount': {
                'currency': 'USD',
                'value': '10.00'
            },
            'cycles_completed': '0',
            'failed_payment_count': '0',
            'next_billing_date': '2016-10-10T10:00:00Z',
            'final_payment_date': '1970-01-01T00:00:00Z'
        },
        'plan': {
            'payment_definitions':
                [
                    {
                        'frequency': 'Month',
                        'type': 'REGULAR',
                        'frequency_interval': '1',
                        'amount': {
                            'currency': 'USD',
                            'value': '10.00'
                        },
                        'cycles': '0',
                        'charge_models':
                            [
                                {
                                    'amount': {
                                        'currency': 'USD',
                                        'value': '0.00'
                                    },
                                    'type': 'TAX'},
                                {
                                    'amount': {
                                        'currency': 'USD',
                                        'value': '0.00'
                                    },
                                    'type': 'SHIPPING'
                                }
                            ]
                    }
                ],
            'merchant_preferences': {
                'auto_bill_amount': 'NO',
                'setup_fee': {
                    'currency': 'USD',
                    'value': '10.00'
                },
                'max_fail_attempts': '0'
            }
        },
        'payer': {
            'payment_method': 'paypal',
            'status': 'verified',
            'payer_info': {
                'first_name': 'test',
                'email': 'ntphat691-buyer@gmail.com',
                'payer_id': 'M7LK9W2SRKWG8',
                'last_name': 'buyer',
                'shipping_address': {
                    'state': 'CA',
                    'recipient_name': 'test buyer',
                    'line1': '1 Main St',
                    'country_code': 'US',
                    'city': 'San Jose',
                    'postal_code': '95131'
                }
            }
        },
        'links': [
            {
                'method': 'POST',
                'rel': 'suspend',
                'href': 'https://api.sandbox.paypal.com/v1/payments/billing-agreements/I-SC0G3G60MKLB/suspend'
            },
            {
                'method': 'POST',
                'rel': 're_activate',
                'href': 'https://api.sandbox.paypal.com/v1/payments/billing-agreements/I-SC0G3G60MKLB/re-activate'
            },
            {
                'method': 'POST',
                'rel': 'cancel',
                'href': 'https://api.sandbox.paypal.com/v1/payments/billing-agreements/I-SC0G3G60MKLB/cancel'
            },
            {
                'method': 'POST',
                'rel': 'self',
                'href': 'https://api.sandbox.paypal.com/v1/payments/billing-agreements/I-SC0G3G60MKLB/bill-balance'
            },
            {
                'method': 'POST',
                'rel': 'self',
                'href': 'https://api.sandbox.paypal.com/v1/payments/billing-agreements/I-SC0G3G60MKLB/set-balance'
            }
        ],
        'shipping_address': {
            'state': 'CA',
            'recipient_name': 'test buyer',
            'line1': '1 Main St',
            'country_code': 'US',
            'city': 'San Jose',
            'postal_code': '95131'
        }
    }
    return render(request, 'teeinsight/plans/subscription_details.html', data_instance)


@superuser_login_required
def payment_history(request, id=None, description=''):
    """
    Display transactions that happened over a billing agreement
    """
    end_date = (datetime.now() + timedelta(hours=1)).strftime('%Y-%m-%d')
    start_date = (datetime.now() + timedelta(days=-30)).strftime('%Y-%m-%d')
    billing_agreement = BillingAgreement.find(id)
    transactions = billing_agreement.search_transactions(start_date, end_date)
    data_instance = {
        'title': pre_title + 'S-payment history' + suf_title,
        'active': ['plans', 'sample'],
        'transactions': transactions.agreement_transaction_list,
        'description': description
    }
    data = {
        'agreement_transaction_list': [
            {
                'transaction_id': 'I-SC0G3G60MKLB',
                'status': 'Created',
                'payer_email': '',
                'payer_name': 'test buyer',
                'time_stamp': '2016-10-10T08:12:47Z',
                'time_zone': 'GMT',
                'transaction_type': 'Recurring Payment'
            },
            {
                'transaction_id': '44L369574S331831D',
                'status': 'Completed',
                'amount': {
                    'currency': 'USD',
                    'value': '10.00'
                },
                'net_amount': {
                    'currency': 'USD',
                    'value': '9.41'
                },
                'fee_amount': {
                    'currency': 'USD',
                    'value': '-0.59'
                },
                'payer_email': 'ntphat691-buyer@gmail.com',
                'payer_name': 'test buyer',
                'time_stamp': '2016-10-10T08:12:52Z',
                'time_zone': 'GMT',
                'transaction_type': 'Recurring Payment'
            }
        ]
    }
    return render(request, 'teeinsight/plans/payment_history.html', data_instance)


# ##
# PARTs
# ##
def campaign_metrics_chart(request, slug=None):
    if not slug:
        slug = ''
    data_instance = {
        'campaign': {}
    }
    if slug:
        campaign_instance = get_object_or_404(Campaign, slug=slug)
    else:
        return redirect('teeinsight_campaigns')
    data_instance["campaign"] = campaign_instance
    return render(request, 'teeinsight/campaign/campaign_metrics_need_wrapper.html', data_instance)


def dashboard_recently(request, status=None):
    data_instance = {
        'recently': []
    }
    campaigns_recently_instance = Campaign.objects.all()
    if status:
        if status == 'active':
            campaigns_recently_instance = campaigns_recently_instance.filter(Q(status=CAMPAIGN_ACTIVE)
                                                                             | Q(status=CAMPAIGN_UNDEFINED))
        else:
            campaigns_recently_instance = campaigns_recently_instance.filter(Q(status=campaign_status_reverse[status]))
    campaigns_recently_instance = campaigns_recently_instance.order_by('-added_at')[:5]
    data_instance["recently"] = campaigns_recently_instance
    return render(request, 'teeinsight/dashboard/dashboard_recently_wrapper.html', data_instance)


def dashboard_hot(request, metric='sales', period='last_3_days'):
    data_instance = {
        'hot': []
    }
    campaigns_hot_instance = Campaign.objects.all()
    metric_reverse = campaign_metrics_reverse[metric]
    period_reverse = metric_periods_reverse[period]
    if metric_reverse is METRIC_SALES:
        if period_reverse is PERIODS_6_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_6_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_6_hours()
        elif period_reverse is PERIODS_12_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_12_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_12_hours()
        elif period_reverse is PERIODS_24_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_24_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_24_hours()
        elif period_reverse is PERIODS_7_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_7_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_7_days()
        elif period_reverse is PERIODS_14_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_14_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_14_days()
        elif period_reverse is PERIODS_30_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_30_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_30_days()
        else:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.sales_3_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.sales_3_days()
    elif metric_reverse is METRIC_ENGAGEMENT:
        if period_reverse is PERIODS_6_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_6_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_6_hours()
        elif period_reverse is PERIODS_12_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_12_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_12_hours()
        elif period_reverse is PERIODS_24_HOURS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_24_hours(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_24_hours()
        elif period_reverse is PERIODS_7_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_7_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_7_days()
        elif period_reverse is PERIODS_14_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_14_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_14_days()
        elif period_reverse is PERIODS_30_DAYS:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_30_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_30_days()
        else:
            campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.engagement_3_days(), reverse=True)
            campaigns_hot_instance = campaigns_hot_instance[:5]
            for campaign_hot in campaigns_hot_instance:
                campaign_hot.value_over_period = campaign_hot.engagement_3_days()
    elif metric_reverse is METRIC_SOCIAL:
        campaigns_hot_instance = sorted(campaigns_hot_instance, key=lambda c: c.fb_likes(), reverse=True)
        campaigns_hot_instance = campaigns_hot_instance[:5]
        for campaign_hot in campaigns_hot_instance:
            campaign_hot.value_over_period = campaign_hot.fb_likes()
    data_instance["hot"] = campaigns_hot_instance
    return render(request, 'teeinsight/dashboard/dashboard_hot_wrapper.html', data_instance)


def dashboard_highest(request, is_today=None):
    data_instance = {
        'is_today': True if is_today else False,
        'highest': []
    }
    campaigns_highest_instance = Campaign.objects.all()
    if is_today:
        campaigns_highest_instance = sorted(campaigns_highest_instance, key=lambda c: c.sales_now(), reverse=True)
        campaigns_highest_instance = campaigns_highest_instance[:5]
    else:
        campaigns_highest_instance = campaigns_highest_instance.order_by('-total_sales')[:5]
    data_instance["highest"] = campaigns_highest_instance
    return render(request, 'teeinsight/dashboard/dashboard_highest_selling_wrapper.html', data_instance)


# total_metric.html
def dashboard_count_today(request):
    start_date = timezone.now().date()
    end_date = timezone.now()
    campaigns_count_added_today = Campaign.objects.filter(Q(added_at__year=end_date.year),
                                                          Q(added_at__month=end_date.month),
                                                          Q(added_at__day=end_date.day)).count()
    return render(request, 'teeinsight/dashboard/dashboard_total_metric.html', {'count': campaigns_count_added_today})


def dashboard_count_active(request):
    end_date = timezone.now()
    campaigns_active = Campaign.objects.filter(Q(status__in=(CAMPAIGN_UNDEFINED, CAMPAIGN_ACTIVE)))
    campaigns_actually_active = campaigns_active.filter(Q(ending_at=None) | Q(ending_at__lte=end_date))
    campaigns_count_active = campaigns_actually_active.count()
    return render(request, 'teeinsight/dashboard/dashboard_total_metric.html', {'count': campaigns_count_active})


def dashboard_count_campaigns(request):
    # campaigns_count_total_campaigns = Campaign.objects.count()
    campaigns_count_total_campaigns = Campaign.objects.filter(Q(is_deleted=False)).aggregate(Count('id'))
    campaigns_count_total_campaigns = campaigns_count_total_campaigns["id__count"]
    return render(request, 'teeinsight/dashboard/dashboard_total_metric.html',
                  {'count': campaigns_count_total_campaigns})


def dashboard_total_sales(request):
    campaigns_filter_total_sales = Campaign.objects.filter(~Q(total_sales=None), Q(total_sales__gt=0))
    campaigns_count_total_sales = campaigns_filter_total_sales.aggregate(Sum('total_sales'))
    campaigns_count_total_sales = campaigns_count_total_sales["total_sales__sum"]
    if campaigns_count_total_sales is None:
        campaigns_count_total_sales = 0
    return render(request, 'teeinsight/dashboard/dashboard_total_metric.html', {'count': campaigns_count_total_sales})


# ##
# APIs
# ##
class CampaignHist(APIView):
    @csrf_exempt
    def get(self, request, slug=None, attr='sales_hist'):
        values_instance = []
        label_instance = 'Sales'
        if slug:
            campaign_instance = Campaign.objects.filter(slug=slug).first()
        else:
            return Response(values_instance)
        if attr == 'sales_hist':
            values_instance = campaign_instance.sales_records.values('updated_at', 'sales')
            values_instance = values_instance.order_by('updated_at')
            values_instance = SalesTimeValues(values_instance, many=True).data
        elif attr == 'fb_likes_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'fb_likes')
            values_instance = values_instance.order_by('updated_at')
            values_instance = FBLikeTimeValues(values_instance, many=True).data
            label_instance = 'Facebook - likes'
        elif attr == 'fb_shares_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'fb_shares')
            values_instance = values_instance.order_by('updated_at')
            values_instance = FBShareTimeValues(values_instance, many=True).data
            label_instance = 'Facebook - shares'
        elif attr == 'fb_comments_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'fb_comments')
            values_instance = values_instance.order_by('updated_at')
            values_instance = FBCmtTimeValues(values_instance, many=True).data
            label_instance = 'Facebook - comments'
        elif attr == 'twitter_tweets_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'twitter_tweets')
            values_instance = values_instance.order_by('updated_at')
            values_instance = TWTimeValues(values_instance, many=True).data
            label_instance = 'Twitter - tweets'
        elif attr == 'google_pluses_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'google_pluses')
            values_instance = values_instance.order_by('updated_at')
            values_instance = GPTimeValues(values_instance, many=True).data
            label_instance = 'Google pluses - +1'
        elif attr == 'pinterest_pins_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'pinterest_pins')
            values_instance = values_instance.order_by('updated_at')
            values_instance = PinTimeValues(values_instance, many=True).data
            label_instance = 'Pinterest - pins'
        elif attr == 'linkedin_shares_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'linkedin_shares')
            values_instance = values_instance.order_by('updated_at')
            values_instance = LinTimeValues(values_instance, many=True).data
            label_instance = 'Linkedin - shares'
        elif attr == 'stumbleupon_stumbles_hist':
            values_instance = campaign_instance.social_metrics.values('updated_at', 'stumbleupon_stumbles')
            values_instance = values_instance.order_by('updated_at')
            values_instance = SBTimeValues(values_instance, many=True).data
            label_instance = 'Stumbleupon stumbles'
        return Response({
            'label': label_instance,
            'data': values_instance
        })


class APISites(APIView):
    @csrf_exempt
    def get(self, request):
        return Response({
            'sites': SiteSerializer(Site.objects.filter(is_deleted=False).order_by('name'), many=True).data
        })


class APICampaigns(APIView):
    @csrf_exempt
    def get(self, request):
        return self.post(request)

    @csrf_exempt
    def post(self, request):
        params = request.data

        key_word = params.get('search', None)

        filter_min_price = params.get('min_price', None)
        filter_min_sales = params.get('min_sales', None)
        filter_time_added = params.get('time_added', None)
        filter_time_end = params.get('time_end', None)
        filter_company = params.get('company', None)
        filter_status = params.get('status', None)
        order_by = params.get('order_by', 'added_at')
        order_asc_desc = params.get('asc_desc', 'desc')
        paging_page = params.get('page', 1)
        paging_items = params.get('items_per_page', 25)

        order_column = params.get('order[0][column]', 10)
        order_dir = params.get('order[0][dir]', 'desc')
        paging_start = params.get('start', 0)
        paging_length = params.get('length', 25)

        campaigns_instance = Campaign.objects.filter(Q(is_deleted=False))
        # raw_campaigns_instance = "select teeinsight_campaign.id from teeinsight_campaign"
        # raw_where = " where"
        # raw_where_list = []

        if key_word:
            campaigns_instance = campaigns_instance.filter(
                Q(title__search=key_word)
                | Q(description__search=key_word)
                | Q(campaign_link__search=key_word)
                | Q(categories__search=key_word))
            # expression = " ((lower(title) like lower('%" + key_word + "%'))"
            # expression += " or (lower(description) like lower('%" + key_word + "%'))"
            # expression += " or (lower(campaign_link) like lower('%" + key_word + "%'))"
            # expression += " or (lower(categories) like lower('%" + key_word + "%'))"
            # expression += ")"
            # raw_where_list.append(expression)

        if filter_time_added:
            filter_time_added = filter_time_added.split(' - ')
            start_date_added = datetime.strptime(filter_time_added[0], '%Y/%m/%d').date()
            end_date_added = datetime.strptime(filter_time_added[1], '%Y/%m/%d').date()
            # print('filter_time_end: ', start_date_added, end_date_added)
            campaigns_instance = campaigns_instance.filter(Q(added_at__range=(start_date_added, end_date_added)))
            # expression = " (teeinsight_campaign.added_at between '"
            # expression += str(start_date_added)
            # expression += "' and '"
            # expression += str(end_date_added)
            # expression += "')"
            # raw_where_list.append(expression)
        if filter_time_end:
            filter_time_end = filter_time_end.split(' - ')
            start_date_end = datetime.strptime(filter_time_end[0], '%Y/%m/%d').date()
            end_date_end = datetime.strptime(filter_time_end[1], '%Y/%m/%d').date()
            # print('filter_time_end: ', start_date_end, end_date_end)
            campaigns_instance = campaigns_instance.filter(Q(ending_at__range=(start_date_end, end_date_end)))
            # expression = " (teeinsight_campaign.ending_at between '"
            # expression += str(start_date_end)
            # expression += "' and '"
            # expression += str(end_date_end)
            # expression += "')"
            # raw_where_list.append(expression)

        if filter_min_price:
            filter_min_price = float(filter_min_price)
            # print('filter_min_price: ', filter_min_price)
            campaigns_instance = campaigns_instance.filter(Q(front_price__gte=filter_min_price))
            # expression = " (teeinsight_campaign.front_price >= "
            # expression += str(filter_min_price)
            # expression += ")"
            # raw_where_list.append(expression)
        if filter_min_sales:
            filter_min_sales = int(filter_min_sales)
            # print('filter_min_sales: ', filter_min_sales)
            campaigns_instance = campaigns_instance.filter(Q(total_sales__gte=filter_min_sales), ~Q(total_sales=None))
            # expression = " (teeinsight_campaign.total_sales >= "
            # expression += str(filter_min_sales)
            # expression += " and teeinsight_campaign.total_sales is not NULL)"
            # raw_where_list.append(expression)

        if filter_company and filter_company != 'None':
            filter_company = filter_company.split(', ')
            sites = Site.objects.filter(Q(slug__in=filter_company)).values_list('id', flat=True)
            # print('filter_company: ', filter_company)
            campaigns_instance = campaigns_instance.filter(Q(site_id__in=sites))
            # if sites:
            # expression = " (teeinsight_campaign.site_id in ("
            # expression += ', '.join(str(x) for x in list(sites))
            # expression += "))"
            # raw_where_list.append(expression)

        if filter_status:
            filter_status = campaign_status_reverse[filter_status]
            # print('filter_status: ', filter_status)
            if filter_status == CAMPAIGN_UNDEFINED or filter_status == CAMPAIGN_ACTIVE:
                # print('filter_only_active: ', filter_only_active)
                campaigns_instance = campaigns_instance.filter(Q(status=CAMPAIGN_ACTIVE) | Q(status=CAMPAIGN_UNDEFINED))
                # expression = " (teeinsight_campaign.status = "
                # expression += str(CAMPAIGN_ACTIVE)
                # expression += " or teeinsight_campaign.status = "
                # expression += str(CAMPAIGN_UNDEFINED)
                # expression += ")"
            else:
                campaigns_instance = campaigns_instance.filter(Q(status=filter_status))
                # expression = " (teeinsight_campaign.status = "
                # expression += str(filter_status)
                # expression += ")"
                # raw_where_list.append(expression)

        # if len(raw_where_list):
        # raw_where += ' and'.join(raw_where_list)
        # raw_campaigns_instance += raw_where

        if order_by:
            # raw_campaigns_instance += " order by "
            # raw_campaigns_instance += "teeinsight_campaign."
            # raw_campaigns_instance += order_by
            # if order_asc_desc:
            # raw_campaigns_instance += " "
            # raw_campaigns_instance += order_asc_desc
            desc = (order_asc_desc == 'desc')
            if order_by == 'sales_now':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.sales_now(), reverse=desc)
            elif order_by == 'fb_likes':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_likes(), reverse=desc)
            elif order_by == 'fb_shares':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_shares(), reverse=desc)
            elif order_by == 'fb_comments':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_comments(), reverse=desc)
            elif order_by == 'twitter_tweets':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.twitter_tweets(), reverse=desc)
            elif order_by == 'google_pluses':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.google_pluses(), reverse=desc)
            elif order_by == 'pinterest_pins':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.pinterest_pins(), reverse=desc)
            elif order_by == 'linkedin_shares':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.linkedin_shares(), reverse=desc)
            elif order_by == 'stumbleupon_stumbles':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.stumbleupon_stumbles(), reverse=desc)
            else:
                if desc:
                    order_by = '-' + order_by
                campaigns_instance = campaigns_instance.order_by(order_by)
        else:
            campaigns_instance = campaigns_instance.order_by('-added_at')
            # raw_campaigns_instance += " order by teeinsight_campaign.added_at desc"

        campaigns_page_instance, paginator = get_paginator(campaigns_instance, paging_items, paging_page)
        # list_ids, paginator = get_camp_id_paginator(raw_campaigns_instance, paging_items, paging_page)

        _to = int(paging_page) * int(paging_items)
        if _to > paginator.count:
            _to = paginator.count
        _from = _to - int(paging_items) + 1
        if _from < 0:
            _from = 0
        _page = int(paging_page)
        if paginator.num_pages < _page:
            _page = 1
        display_str = 'Showing ' + str(_from) + ' to ' + str(_to)
        display_str = display_str + ' of ' + str(paginator.count)
        display_str += ' campaigns.'

        # campaigns_instance = Campaign.objects.filter(Q(id__in=list_ids))

        # print('[', timezone.now(), ']', 'params', params)
        # print('[', timezone.now(), ']', 'raw_sql', str(campaigns_instance.query))
        return Response({
            'params': params,
            'paginator': {
                'display': display_str,
                'totalItems': paginator.count,
                'itemsPerPage': int(paging_items),
                'numPages': paginator.num_pages,
                'currentPage': _page
            },
            'recordsTotal': paginator.count,
            'recordsFiltered': paginator.count,
            'data': CampaignsSerializer(campaigns_page_instance, many=True).data
        })


class APICountToday(APIView):
    @csrf_exempt
    def get(self, request):
        end_date = timezone.now()
        campaigns_count_added_today = Campaign.objects.filter(Q(added_at__year=end_date.year),
                                                              Q(added_at__month=end_date.month),
                                                              Q(added_at__day=end_date.day)).count()
        return Response(campaigns_count_added_today)


class APICountActive(APIView):
    @csrf_exempt
    def get(self, request):
        end_date = timezone.now()
        campaigns_active = Campaign.objects.filter(Q(status__in=(CAMPAIGN_UNDEFINED, CAMPAIGN_ACTIVE)))
        campaigns_actually_active = campaigns_active.filter(Q(ending_at=None) | Q(ending_at__lte=end_date))
        campaigns_count_active = campaigns_actually_active.count()
        return Response(campaigns_count_active)


class APICountCampaigns(APIView):
    @csrf_exempt
    def get(self, request):
        # campaigns_count_total_campaigns = Campaign.objects.count()
        campaigns_count_total_campaigns = Campaign.objects.filter(Q(is_deleted=False)).aggregate(Count('id'))
        campaigns_count_total_campaigns = campaigns_count_total_campaigns["id__count"]
        return Response(campaigns_count_total_campaigns)


class APITotalSales(APIView):
    @csrf_exempt
    def get(self, request):
        campaigns_filter_total_sales = Campaign.objects.filter(~Q(total_sales=None), Q(total_sales__gt=0))
        campaigns_count_total_sales = campaigns_filter_total_sales.aggregate(Sum('total_sales'))
        campaigns_count_total_sales = campaigns_count_total_sales["total_sales__sum"]
        if campaigns_count_total_sales is None:
            campaigns_count_total_sales = 0
        return Response(campaigns_count_total_sales)


class APISearchSlug(APIView):
    @csrf_exempt
    def get(self, request, slug='test', items=25, page=1):
        campaigns_search = Campaign.objects.filter(Q(slug=slug))
        campaigns_instance, paginator = get_paginator(campaigns_search, items, page)
        return Response({
            'campaigns_search': CampaignsDefaultSerializer(campaigns_instance, many=True).data
        })


class APISearchStatus(APIView):
    @csrf_exempt
    def get(self, request, status=0, items=25, page=1):
        campaigns_search = Campaign.objects.filter(Q(status=status))
        campaigns_instance, paginator = get_paginator(campaigns_search, items, page)
        return Response({
            'campaigns_search': CampaignsDefaultSerializer(campaigns_instance, many=True).data
        })


class APISearchCampaigns(APIView):
    @csrf_exempt
    def get(self, request):
        return self.post(request)

    @csrf_exempt
    def post(self, request):
        params = request.data

        key_word = params.get('search', None)

        filter_min_price = params.get('min_price', None)
        filter_min_sales = params.get('min_sales', None)
        filter_time_added = params.get('time_added', None)
        filter_time_end = params.get('time_end', None)
        filter_company = params.get('company', None)
        filter_status = params.get('status', None)
        order_by = params.get('order_by', 'added_at')
        order_asc_desc = params.get('asc_desc', 'desc')
        paging_page = params.get('page', 1)
        paging_items = params.get('items_per_page', 25)

        order_column = params.get('order[0][column]', 10)
        order_dir = params.get('order[0][dir]', 'desc')
        paging_start = params.get('start', 0)
        paging_length = params.get('length', 25)

        campaigns_instance = Campaign.objects.filter(Q(is_deleted=False))
        # raw_campaigns_instance = "select teeinsight_campaign.id from teeinsight_campaign"
        # raw_where = " where"
        # raw_where_list = []

        if key_word:
            campaigns_instance = campaigns_instance.filter(
                Q(title__icontains=key_word)
                | Q(description__icontains=key_word)
                | Q(campaign_link__icontains=key_word)
                | Q(categories__icontains=key_word))
            # expression = " ((lower(title) like lower('%" + key_word + "%'))"
            # expression += " or (lower(description) like lower('%" + key_word + "%'))"
            # expression += " or (lower(campaign_link) like lower('%" + key_word + "%'))"
            # expression += " or (lower(categories) like lower('%" + key_word + "%'))"
            # expression += ")"
            # raw_where_list.append(expression)

        if filter_time_added:
            filter_time_added = filter_time_added.split(' - ')
            start_date_added = datetime.strptime(filter_time_added[0], '%Y/%m/%d').date()
            end_date_added = datetime.strptime(filter_time_added[1], '%Y/%m/%d').date()
            # print('filter_time_end: ', start_date_added, end_date_added)
            campaigns_instance = campaigns_instance.filter(Q(added_at__range=(start_date_added, end_date_added)))
            # expression = " (teeinsight_campaign.added_at between '"
            # expression += str(start_date_added)
            # expression += "' and '"
            # expression += str(end_date_added)
            # expression += "')"
            # raw_where_list.append(expression)
        if filter_time_end:
            filter_time_end = filter_time_end.split(' - ')
            start_date_end = datetime.strptime(filter_time_end[0], '%Y/%m/%d').date()
            end_date_end = datetime.strptime(filter_time_end[1], '%Y/%m/%d').date()
            # print('filter_time_end: ', start_date_end, end_date_end)
            campaigns_instance = campaigns_instance.filter(Q(ending_at__range=(start_date_end, end_date_end)))
            # expression = " (teeinsight_campaign.ending_at between '"
            # expression += str(start_date_end)
            # expression += "' and '"
            # expression += str(end_date_end)
            # expression += "')"
            # raw_where_list.append(expression)

        if filter_min_price:
            filter_min_price = float(filter_min_price)
            # print('filter_min_price: ', filter_min_price)
            campaigns_instance = campaigns_instance.filter(Q(front_price__gte=filter_min_price))
            # expression = " (teeinsight_campaign.front_price >= "
            # expression += str(filter_min_price)
            # expression += ")"
            # raw_where_list.append(expression)
        if filter_min_sales:
            filter_min_sales = int(filter_min_sales)
            # print('filter_min_sales: ', filter_min_sales)
            campaigns_instance = campaigns_instance.filter(Q(total_sales__gte=filter_min_sales), ~Q(total_sales=None))
            # expression = " (teeinsight_campaign.total_sales >= "
            # expression += str(filter_min_sales)
            # expression += " and teeinsight_campaign.total_sales is not NULL)"
            # raw_where_list.append(expression)

        if filter_company and filter_company != 'None':
            filter_company = filter_company.split(', ')
            sites = Site.objects.filter(Q(slug__in=filter_company)).values_list('id', flat=True)
            # print('filter_company: ', filter_company)
            campaigns_instance = campaigns_instance.filter(Q(site_id__in=sites))
            # if sites:
            # expression = " (teeinsight_campaign.site_id in ("
            # expression += ', '.join(str(x) for x in list(sites))
            # expression += "))"
            # raw_where_list.append(expression)

        if filter_status:
            filter_status = campaign_status_reverse[filter_status]
            # print('filter_status: ', filter_status)
            if filter_status == CAMPAIGN_UNDEFINED or filter_status == CAMPAIGN_ACTIVE:
                # print('filter_only_active: ', filter_only_active)
                campaigns_instance = campaigns_instance.filter(Q(status=CAMPAIGN_ACTIVE) | Q(status=CAMPAIGN_UNDEFINED))
                # expression = " (teeinsight_campaign.status = "
                # expression += str(CAMPAIGN_ACTIVE)
                # expression += " or teeinsight_campaign.status = "
                # expression += str(CAMPAIGN_UNDEFINED)
                # expression += ")"
            else:
                campaigns_instance = campaigns_instance.filter(Q(status=filter_status))
                # expression = " (teeinsight_campaign.status = "
                # expression += str(filter_status)
                # expression += ")"
                # raw_where_list.append(expression)

        # if len(raw_where_list):
        # raw_where += ' and'.join(raw_where_list)
        # raw_campaigns_instance += raw_where

        if order_by:
            # raw_campaigns_instance += " order by "
            # raw_campaigns_instance += "teeinsight_campaign."
            # raw_campaigns_instance += order_by
            # if order_asc_desc:
            # raw_campaigns_instance += " "
            # raw_campaigns_instance += order_asc_desc
            desc = (order_asc_desc == 'desc')
            if order_by == 'sales_now':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.sales_now(), reverse=desc)
            elif order_by == 'fb_likes':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_likes(), reverse=desc)
            elif order_by == 'fb_shares':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_shares(), reverse=desc)
            elif order_by == 'fb_comments':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.fb_comments(), reverse=desc)
            elif order_by == 'twitter_tweets':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.twitter_tweets(), reverse=desc)
            elif order_by == 'google_pluses':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.google_pluses(), reverse=desc)
            elif order_by == 'pinterest_pins':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.pinterest_pins(), reverse=desc)
            elif order_by == 'linkedin_shares':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.linkedin_shares(), reverse=desc)
            elif order_by == 'stumbleupon_stumbles':
                campaigns_instance = sorted(campaigns_instance, key=lambda c: c.stumbleupon_stumbles(), reverse=desc)
            else:
                if desc:
                    order_by = '-' + order_by
                campaigns_instance = campaigns_instance.order_by(order_by)
        else:
            campaigns_instance = campaigns_instance.order_by('-added_at')
            # raw_campaigns_instance += " order by teeinsight_campaign.added_at desc"

        campaigns_instance, paginator = get_paginator(campaigns_instance, paging_items, paging_page)
        # list_ids, paginator = get_camp_id_paginator(raw_campaigns_instance, paging_items, paging_page)

        _to = int(paging_page) * int(paging_items)
        if _to > paginator.count:
            _to = paginator.count
        _from = _to - int(paging_items) + 1
        if _from < 0:
            _from = 0
        _page = int(paging_page)
        if paginator.num_pages < _page:
            _page = 1
        display_str = 'Showing ' + str(_from) + ' to ' + str(_to)
        display_str = display_str + ' of ' + str(paginator.count)
        display_str += ' campaigns.'

        # campaigns_instance = Campaign.objects.filter(Q(id__in=list_ids))

        return Response({
            'params': params,
            'raw_sql': str(campaigns_instance.object_list.query),
            'paginator': {
                'display': display_str,
                'totalItems': paginator.count,
                'itemsPerPage': int(paging_items),
                'numPages': paginator.num_pages,
                'currentPage': _page
            },
            'recordsTotal': paginator.count,
            'recordsFiltered': paginator.count,
            'data': CampaignsSerializer(campaigns_instance, many=True).data
        })


