# cffi==1.8.3                   # paypalrestsdk
# cryptography==1.5.3             # paypalrestsdk
dj-static==0.0.6
Django==1.9.8
# django-appconf==1.0.2         # django-compressor
django-bower==5.2.0
django-compressor==2.1
django-debug-toolbar==1.6
djangorestframework==3.4.6
# et-xmlfile==1.0.1             # openpyxl
# idna==2.1                     # paypalrestsdk
# jdcal==1.3                    # openpyxl
mysql-connector-python==2.1.3
openpyxl==2.4.0
paypalrestsdk==1.12.0
# pkg-resources==0.0.0
# pyasn1==0.1.9                 # paypalrestsdk
# pycparser==2.17               # paypalrestsdk
# pyOpenSSL==16.2.0             # paypalrestsdk
# rcssmin==1.0.6                # django-compressor
# requests==2.11.1              # paypalrestsdk
# rjsmin==1.0.12                # django-compressor
# six==1.10.0                   # django-bower
# sqlparse==0.2.2               # django-debug-toolbar
# static3==0.7.0                # dj-static
